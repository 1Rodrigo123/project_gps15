# IPC4us
![Documentos/logo.png](Documentos/logo.png)

## Contents

- [Team](#team)
- [Vision and Scope](#vision-and-scope)
- [Requirements](#requirements)
    - [Use case diagram](#use-case-diagram)
    - [Mockups](#mockups)
    - [User stories](#user-stories)
- [Definition of Done](#definition-of-done)
- [Architecture and Design](#architecture-and-design)
    - [Domain Model](#domain-model)
- [Risk Plan](#risk-plan)
- [Pre-Game](#pre-game)
- [Release Plan](#release-plan)
  - [Release 1](#release-1)
  - [Release 2](#release-2)
- [Increments](#increments)
  - [Sprint 1](#sprint-1)
  - [Sprint 2](#sprint-2)
  - [Sprint 3](#sprint-3)

## Team

- Chelsea Carina Pais da Cunha Duarte (Nº de estudante: 2021100010 | Email: a2021100010@isec.pt)
- Denise do Carmo Sigaúque Taúla (Nº de estudante: 2021150940 | Email: a2021150940@isec.pt)
- Diogo Alexandre Pereira Coelho (Nº de estudante: 2019143273 | Email: a2019143273@isec.pt)
- Guilherme Nuno Sobral Coelho Pina (Nº de estudante: 2021122169 | Email: a2021122169@isec.pt)
- Rodrigo Martins Duarte (Nº de estudante: 2021153914 | Email: a2021153914@isec.pt)

***

## Vision and Scope

#### Problem Statement

##### Project background

Atualmente, no Instituto Superior de Engenharia de Coimbra (ISEC), as ferramentas utilizadas para avaliar os cursos e as unidades curriculares (UC), nomeadamente os inquéritos pedagógicos, não permitem que as avaliações se realizem a qualquer momento, ao longo dos semestres.

##### Stakeholders

**Estudantes**
  - Avaliar e fazer ouvir a sua opinião sobre o funcionamento do curso, das UCs e da própria instituição, com o intuito de melhorar os processos de ensino e de avaliação.

**Docentes**
  - Obter feedback dos estudantes e identificar aspectos com impacto no processo de aprendizagem, com vista a melhorar o ensino e o plano curricular da UC;
  - Responder e/ou clarificar as avaliações realizadas pelos estudantes.

##### Users

**Estudantes**
  - Necessidade de uma plataforma onde possam avaliar continuamente o seu curso, as suas UCs e a instituição;
  - Necessidade de um fórum que agrupe informação sobre as suas UCs e que permita interagir com colegas.

**Docentes**
  - Necessidade de uma plataforma onde possam consultar, ao longo do semestre, as avaliações e opiniões dos estudantes, sobre as suas UCs.

**Moderador**
  - Necessidade de assegurar que todos os utilizadores cumprem as regras de utilização da aplicação, analisando e mediando conflitos, punindo ainda os utilizadores que infrinjam as regras.

***

#### Vision & Scope of the Solution

##### Vision statement
O nosso projeto visa desenvolver uma aplicação que agregue uma hierarquia de fóruns, com sistema de moderação, e que se ramifique em 3 secções principais: 
  - A primeira secção, permitirá reunir e discutir vários aspetos inerentes ao funcionamento do próprio IPC / ISEC e seus demais serviços.
  - A segunda secção, partilhada por estudantes e docentes, irá centrar-se na avaliação dos respectivos cursos e UCs da instituição.
  - Por último, existirá uma hierarquia de fóruns, dirigidos a estudantes, que permitirá discutir, de um modo mais informal, sobre todos os tópicos já mencionados.

##### List of features

**1. Áreas de avaliação das Unidade Curriculares:**
   - Permitirá aos estudantes avaliarem e classificarem as suas UCs em termos de gerais, de organização e conteúdos;
   - No final de cada semestre esta secção seria revista de modo a eliminar comentários com más ou nenhuma avaliação, mantendo para a posteridade apenas aquelas que a comunidade estudantil identificou como positivas.

**2. Áreas de avaliação dos Docentes:**
  - Permitirá aos estudantes avaliarem e classificarem os seus docentes em relação a algumas das suas competências, nomeadamente: capacidade pedagógica, capacidade científica e capacidade organizacional.

**3. Áreas de avaliação do IPC/ISEC e respetivos cursos:**
  - Permitirá aos estudantes avaliarem e classificarem os seus cursos e os próprios serviços do IPC / ISEC.

**4. Perfil do docente:**
  - Cada docente terá um perfil que incluirá informações como o endereço eletrónico, o horário de gabinete e as UCs que lecciona.

**5. Fórum:**
  - Associado a cada UC, existirá um fórum através do qual os estudantes poderão interagir e ajudar outros colegas.
  - Os estudantes terão ainda a possibilidade de avaliar a pertinência e interesse de cada publicação, através da atribuição de upvotes e downvotes, o que irá influenciar a notoriedade e visibilidade de cada publicação.

**6. Ferramenta de moderação:**
  - Antes de cada publicação ser submetida, o seu conteúdo será filtrado por uma ferramenta de moderação para detetar e eliminar publicações que violem as regras da aplicação.
  - Os próprios utilizadores poderão reportar publicações que considerem desadequadas, sendo estas posteriormente analisadas por um moderador.

**7. Estratégias de gamificação:**
  - Serão utilizadas estratégias de gamificação, que permitirão aos estudantes ganhar distintivos (badges) pela sua participação ativa e construtiva nos fóruns, aumentando assim o nível de confiança do seu perfil.

**8. Avaliação anónima opcional:**
  - No momento de publicar uma avaliação, o estudante pode optar por realizar uma partilha anónima, que não ficará associada ao seu perfil de estudante.

**9. Ordenação, filtragem e pesquisa avançada:**
  - Permitirá aos estudantes refinar as suas pesquisas, com base em vários critérios, como por exemplo, mais comentados, melhor avaliação, ou pesquisando por palavras-chave.

**10. Secção de Classificação e Estatísticas:**
  - Serão apresentadas as médias (de 0 a 5) resultantes das avaliações de cada UC, discriminadas pelos tópicos avaliados;
  - Através destes dados, o docente poderá obter estatísticas e representações gráficas das participações e avaliações.

**11. Secção de avaliação de outros espaços e serviços do ISEC:**
  - Será possível, para além de avaliar as UC, avaliar também outros espaços e serviços do ISEC, como por exemplo, a cantina e o bar.


##### Features that will not be developed

**1. Expansão da aplicação aos demais cursos do IPC:**
  - Nesta fase de desenvolvimento, a aplicação irá conter apenas informação dos cursos do ISEC, pelo que não iremos expandir aos restantes cursos e docentes do IPC.

**2. Histórico de avaliação:**
  - Seria disponibilizado o acesso ao histórico de avaliações de cada UC, apresentando a evolução dos resultados das avaliações ao longo dos anos letivos, sendo esta informação representada graficamente.

**3. Notificações, atualizações e sugestões:**
  - Sistema de notificações que iria informar os estudantes e docentes sobre novas avaliações, discussões ou atualizações nas UCs subscritas.

**4. Chat ou mensagens privadas:**
  - Permitiria aos utilizadores da aplicação trocar mensagens privadas, para discutir tópicos de forma privada ou mesmo tirar dúvidas.

**5. Acesso em qualquer dispositivo:**
  - Idealmente a aplicação estaria disponível em qualquer dispositivo, desenvolvendo uma aplicação adaptada para sistemas móveis, ou eventualmente migrando o projeto para o formato de uma aplicação web, garantindo assim o máximo de compatibilidade entre dispositivos e tornando-a facilmente acessível.

**6. Restrição de acesso ao público externo:**
  - Permitiria acesso restrito para o público em geral que não possui uma conta associada ao IPC, mas que pretendesse ingressar futuramente ou que quisesse simplesmente obter mais informações sobre os cursos e a instituição. Este público conseguiria aceder a uma secção de “Dúvidas” onde poderia colocar questões e receber feedback sobre os diferentes cursos do IPC, interagindo assim com os estudantes da instituição.

**7. Sistema de Login restrito a estudantes e docentes do IPC**
  - Uma vez que a aplicação iria conter informação institucional, como dados pessoais de docentes e estudantes, o acesso teria de se cingir a utilizadores da própria instituição. Deste modo, para assegurar o cumprimento do Regulamento Geral sobre a Proteção de Dados (RGPD), no momento de login, o utilizador seria redirecionado para a página do IPC, onde validaria as suas credenciais de acesso.

##### Assumptions

1. Os estudantes, ao longo de cada semestre, irão utilizar a aplicação para realizar avaliações e partilhar opiniões e sugestões, em detrimento das plataformas atuais;
2. Os docentes terão em conta as avaliações realizadas através da aplicação e não apenas as que advêm dos inquéritos pedagógicos;
3. As denúncias dos utilizadores e as estratégias de moderação a implementar (automática e através de moderadores) serão suficientes para assegurar o cumprimento das regras da aplicação;
4. Na fase de desenvolvimento, para testar e demonstrar as funcionalidades da aplicação, será criada e utilizada uma base de dados com dados fictícios (que irá simular as validações e os dados dos utilizadores fornecidos pelo próprio IPC).

***

## Requirements

### Use Case Diagram

![Recursos/UML//DCU.png](Documentos/UML/DCU.png)

***

### Mockups

Login

![Login](Documentos/Mockups-Imagens/m1.png)

Homepage

![Homepage](Documentos/Mockups-Imagens/m2.png)

-------------------------------------------------------------------------
Lista das Unidades Curriculares

![Recursos/mockups//m8.png](Documentos/Mockups-Imagens/m8.png)

Perfil da Unidade Curricular

![PerfilUC](Documentos/Mockups-Imagens/m3.png)

Secção de Avaliação da Unidade Curricular

![SeccaoAvaliacaoUC](Documentos/Mockups-Imagens/m4.png)

Fórum de Estudantes

![ForumEstudantes](Documentos/Mockups-Imagens/m5.png)

Perfil do Curso

![PerfilCurso](Documentos/Mockups-Imagens/m9.png)

Perfil de Avaliação de Docente

![PerfilAvaliacaoDocente](Documentos/Mockups-Imagens/m11.png)


-------------------------------------------------------------------------

Perfil de Docente

![PerfilDocente-SobreMim](Documentos/Mockups-Imagens/m6.png)
![PerfilDocente-MinhasAvaliacoes](Documentos/Mockups-Imagens/m10.png)

Perfil de Estudante

![PerfilEstudante](Documentos/Mockups-Imagens/m12.png)
![PerfilEstudante-Badges](Documentos/Mockups-Imagens/m13.png)
![EditarPerfil-Perfil](Documentos/Mockups-Imagens/m14.png)
![EditarPerfil-Visibilidade](Documentos/Mockups-Imagens/m15.png)
![EditarPerfil-Notificacoes](Documentos/Mockups-Imagens/m16.png)
![EditarPerfil-Seguranca](Documentos/Mockups-Imagens/m17.png)

***


### User Stories

- User Story 1 - Efetuar login (1Rodrigo123/project_gps15#1)
- User Story 2 - Visualizar a média das avaliações de uma UC (1Rodrigo123/project_gps15#3)
- User Story 3 - Consultar a secção de avaliação de uma UC (1Rodrigo123/project_gps15#4)
- User Story 4 - Aceder às estatísticas de avaliação da UC (1Rodrigo123/project_gps15#5)
- User Story 5 - Avaliar as UCs do meu curso (1Rodrigo123/project_gps15#6)
- User Story 6 - Escolher a opção de avaliar anonimamente (1Rodrigo123/project_gps15#7)
- User Story 7 - Atribuir upvote / downvote a uma avaliação (1Rodrigo123/project_gps15#8)
- User Story 8 - Responder a avaliações (1Rodrigo123/project_gps15#9)
- User Story 9 - Consultar o meu perfil (1Rodrigo123/project_gps15#10)
- User Story 10 - Editar o meu perfil (1Rodrigo123/project_gps15#11)
- User Story 11 - Criar uma publicação num fórum (1Rodrigo123/project_gps15#12)
- User Story 12 - Atribuir upvote / downvote  às publicações de um fórum (1Rodrigo123/project_gps15#13)
- User Story 13 - Responder às publicações de um fórum (1Rodrigo123/project_gps15#14)
- User Story 14 - Ser recompensado por fazer contribuições positivas (1Rodrigo123/project_gps15#15)
- User Story 15 - Organizar publicações (1Rodrigo123/project_gps15#17)
- User Story 16 - Efetuar pesquisa avançada (1Rodrigo123/project_gps15#18)
- User Story 17 - Denunciar uma publicação num fórum (1Rodrigo123/project_gps15#19)
- User Story 18 - Denunciar um comentário (1Rodrigo123/project_gps15#32)
- User Story 19 - Analisar publicações reportadas (1Rodrigo123/project_gps15#34)
- User Story 20 - Apagar publicações (1Rodrigo123/project_gps15#33)
- User Story 21 - Punir utilizadores (1Rodrigo123/project_gps15#35)
- User Story 22 - Consultar a secção de avaliação de um docente (1Rodrigo123/project_gps15#69)
- User Story 23 - Avaliar os docentes que lecionam as minhas turmas (1Rodrigo123/project_gps15#70)
- User Story 24 - Mostrar/ocultar classificações de um docente (1Rodrigo123/project_gps15#71)

***

## Definition of done

(This section is already written, do not edit)
It is a collection of criteria that must be completed for a User Story to be considered “done.”

1. All tasks done:
   - CI – built, tested (Junit), reviewed (SonarCloud)
   - Merge request to qa (code review)
2. Acceptance tests passed
3. Accepted by the client
4. Code merged to main

***

##### User Story 1 - Efetuar login
Como utilizador, necessito de efetuar login na aplicação, de modo a poder aceder às suas funcionalidades, utilizando para isso as minhas credenciais do IPC.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have

***

##### User Story 2 - Visualizar a média das avaliações de uma UC
Como estudante, pretendo visualizar a média das avaliações de cada UC, de forma a compreender qual a visão geral que os estudantes têm da UC.

###### Story Points
Story Points - Small

###### MoSCoW
Must Have

***

##### User Story 3 - Consultar a secção de avaliação de uma UC
Enquanto utilizador, pretendo consultar as avaliações das UCs realizadas pelos estudantes, para poder refletir e compreender a visão de cada um em relação à UC.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 4 - Aceder às estatísticas de avaliação da UC
Enquanto docente, tenciono aceder a uma aba que exiba informação estatística sucinta sobre as avaliações da UC que leciono, para melhor analisar o desempenho da UC.

###### Story Points
Story Points - Medium

###### MoSCoW
Could Have
 
***

##### User Story 5 - Avaliar as UCs do meu curso
Enquanto estudante, pretendo avaliar as minhas UCs ao longo do semestre, de modo a que a minha opinião sirva para melhorar o funcionamento da UC.

###### Story Points
Story Points - Large

###### MoSCoW
Must Have
 
***

##### User Story 6 - Escolher a opção de avaliar anonimamente
Enquanto estudante, pretendo poder submeter as minhas avaliações de forma anónima, para que não tenha receio em expor a minha opinião. 

###### Story Points
Story Points - Small

###### MoSCoW
Should Have
 
***

##### User Story 7 - Atribuir upvote / downvote a uma avaliação
Enquanto utilizador, pretendo atribuir upvote / downvote, consoante concorde / discorde de uma avaliação, de modo a demonstrar a minha opinião.

###### Story Points
Story Points - Small

###### MoSCoW
Must Have
 
***

##### User Story 8 - Responder a avaliações
Como utilizador, pretendo responder às avaliações dos estudantes, com o intuito de promover um diálogo construtivo.

###### Story Points
Story Points - Large

###### MoSCoW
Must Have
 
***

##### User Story 9 - Consultar o meu perfil
Enquanto utilizador, desejo consultar o meu perfil na aplicação, para visualizar as minhas informações pessoais e outras configurações relativas ao meu perfil.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 10 - Editar o meu perfil
Enquanto utilizador, pretendo editar as informações disponíveis no meu perfil, de modo a mantê-lo atualizado.

###### Story Points
Story Points - Medium

###### MoSCoW
Could Have
 
***

##### User Story 11 - Criar uma publicação num fórum
Enquanto estudante, pretendo esclarecer as minhas dúvidas, podendo publicá-las e obter respostas através de um fórum, promovendo assim a interajuda entre estudantes.

###### Story Points
Story Points - Large

###### MoSCoW
Must Have
 
***

##### User Story 12 - Atribuir upvote / downvote  às publicações de um fórum 
Como estudante, quando leio uma publicação com a qual concordo / discordo, pretendo atribuir um upvote / downvote, de modo a avaliar a sua pertinência.

###### Story Points
Story Points - Small

###### MoSCoW
Must Have
 
***

##### User Story 13 - Responder às publicações de um fórum
Enquanto estudante, pretendo responder às publicações de outros colegas, de modo a contribuir com a minha opinião para os tópicos em discussão.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 14 - Ser recompensado por fazer contribuições positivas
Enquanto estudante, pretendo ser recompensado pelo contributo positivo que presto aos meus colegas, de modo a aumentar o nível de confiança das minhas publicações.

###### Story Points
Story Points - Large

###### MoSCoW
Could Have
 
***

##### User Story 15 - Organizar os comentários
Enquanto utilizador, pretendo organizar as publicações de acordo com certos critérios, de modo a facilmente localizar o que pretendo.

###### Story Points
Story Points - Small

###### MoSCoW
Could Have
 
***

##### User Story 16 - Efetuar pesquisa avançada
Enquanto utilizador, pretendo efetuar pesquisas através de palavras-chave, de modo a localizar mais facilmente os conteúdos que pretendo.

###### Story Points
Story Points - Medium

###### MoSCoW
Should Have
 
***

##### User Story 17 - Denunciar uma publicação num fórum
Enquanto utilizador, pretendo denunciar publicações ofensivas, de modo a que sejam removidas, garantindo assim um bom ambiente nos fóruns.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 18 - Denunciar um comentário de avaliação
Enquanto utilizador, pretendo denunciar avaliações ofensivas, de modo a que sejam removidas, garantindo assim um bom ambiente na plataforma.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 19 - Analisar publicações reportadas
Como moderador, pretendo analisar as publicações que tenham sido reportadas, de modo a poder agir em conformidade com o cumprimento das regras da aplicação.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have
 
***

##### User Story 20 - Apagar publicações
Como moderador, necessito de apagar publicações que violem as regras da aplicação, de forma a garantir o seu cumprimento.

###### Story Points
Story Points - Small

###### MoSCoW
Must Have
 
***

##### User Story 21 - Punir utilizadores
Enquanto moderador, pretendo aplicar medidas punitivas a utilizadores que incumpram as regras da aplicação, a fim de garantir o seu cumprimento.

###### Story Points
Story Points - Large

###### MoSCoW
Could Have
 
***

##### User Story 22 - Consultar a secção de avaliação de um docente
Enquanto estudante, pretendo consultar as avaliações dos meus docentes, para poder refletir e compreender a visão de cada um em relação à UC.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have

***

##### User Story 23 - Avaliar os docentes que lecionam as minhas turmas
Enquanto estudante, pretendo avaliar docentes com os quais tenho aulas ao longo do semestre, de modo a que a minha opinião sirva para melhorar o funcionamento da UC.

###### Story Points
Story Points - Medium

###### MoSCoW
Must Have

***

##### User Story 24 - Mostrar/ocultar classificações de um docente
Enquanto docente, pretendo tornar visível, ou não, as classificações atribuídas pelos estudantes às minhas avaliações, de modo a gerir a privacidade das minhas avaliações.

###### Story Points
Story Points - Small

###### MoSCoW
Could Have

***

## Architecture and Design

#### Domain Model

![Recursos/UML//MD.png](Documentos/UML/MD.png)

***

## Risk Plan

##### Threshold of Success
- Até à data da 2ª release:
  - Ter pelo menos 50% das user stories concluídas e aceites pelo cliente. 
  - Todas as tarefas implementadas devem passar nos testes de qualidade;
- Integrar uma base de dados à aplicação com dados fictícios necessários ao login e gestão de informação dos utilizadores.
- Garantir que o acesso à informação na aplicação será restringido de acordo com o papel de cada utilizador.

##### Risk List

- **RSK1** – [PxI: 1x4 = 4]; Dado que a equipa em geral tem pouca experiência com desenvolvimento de código em simultâneo, o que pode levar a ocorrer problemas nomeadamente aos testes e bugs que eventualmente possam aparecer.
- **RSK2** – [PxI: 3x5 = 15]; Dado que a aplicação conterá dados sensíveis dos estudantes e docentes e está sujeita a falhas técnicas, pode ocorrer problemas de segurança deixando estes dados expostos.
- **RSK3** – [PxI: 4x5 = 20]; Dado que o número de estudantes e docentes associados ao instituto, a aplicação poderá acomodar um grande fluxo de utilizadores em simultâneo, o que pode trazer dificuldades em escalar o sistema para acomodar um elevado número de utilizadores, comentários e/ou avaliações.
- **RSK4** – [PxI: 2x4 = 8]; Sendo que não é possível prever de que forma cada estudante irá utilizar a aplicação, pode haver a possibilidade dos estudantes realizarem comentários ofensivos, insultuosos ou que desrespeitem a instituição, os seus docentes e alunos. 

##### Mitigation Actions (threats>=20)
- **RSK3 - (AS)** Planear adequadamente a infraestrutura do servidor para lidar com aumentos no tráfego da aplicação. Efetuar monitorizações periódicas para verificar o desempenho da aplicação e fazer ajustes conforme necessário.

<br>
Nota: Contingency Plan (CP) / Plano de Contingência, Avoidance Strategy (AS) / Plano de Prevenção or Minimization Strategy (MS) / Plano de Minimização.

***

## Pre-Game
### Sprint 0 Plan

- Goal: Aprovar todas as funcionalidades da aplicação junto do cliente.
- Dates: De 13 de outubro a 27 de outubro | 2 semanas.
- Sprint 0 Backlog (don't edit this list):
  - Task1 – Write Team
  - Task2 – Write V&S
  - Task3 – Write Requirements
  - Task4 – Write DoD
  - Task5 – Write Architecture&Design
  - Task6 – Write Risk Plan
  - Task7 – Write Pre-Gane
  - Task8 – Write Release Plan
  - Task9 – Write Product Increments
  - Task10 – Create Product Board
  - Task11 – Create Sprint 0 Board
  - Task12 – Write US in PB, estimate (SML), prioritize (MoSCoW), sort
  - Task13 – Create repository with “GPS Git” Workflow

***

## Release Plan

### Release 1

- Goal: MVP - Ter o Sistema de login a funcionar e a Secção de Avaliação da UC a permitir acrescentar avaliações, upvotes e downvotes, e com o sistema de moderação automática a funcionar.
- Date: 30 de novembro
- Release: V1.0

***

### Release 2

- Goal: Implementar os fóruns de estudantes, permitir reportar publicações e permitir ao moderador analisar e remover publicações.
- Date: 15 de dezembro
- Release: V2.0

***

## Increments

### Sprint 1
##### Sprint Plan

- Goal: Ter o **Sistema de login** a funcionar e a **Secção de Avaliação da UC** a permitir inserir avaliações.

- Dates: De 27 de outubro a 17 de novembro | 3 semanas

- Roles:
  - Product Owner: Denise Taúla
  - Scrum Master: Rodrigo Duarte

- To do:
  - User Story 1 - Efetuar login 
  - User Story 3 - Consultar a secção de avaliação das UCs
  - User Story 5 - Avaliar as UCs do meu curso
  - User Story 9 - Consultar o meu perfil
  - Task: Estruturação do projeto
  - Task: Desenvolver o modelo de dados

- Story Points: 3M+1L

- Analysis: O utilizador já consegue efetuar o login na aplicação e inserir novas avaliações nas Secções de Avaliação das UCs.

##### Sprint Review

- Analysis: what was not done or what was added 
  - O login está a funcionar corretamente e interage com a base de dados para validar as credenciais indicadas;
  - É necessário alterar a interface dos fóruns de avaliação quando o utilizador é um docente, por questões de UX;
  - Quando é inserida uma nova publicação no fórum esta já é gravada na base de dados, porém o fórum não é atualizado automaticamente;
  - Deviamos adicionar mais testes.

- Story Points: 2S+1M+2X+2H

- Version: 0.1 

- Client analysis: client feedback
  - O cliente ficou satisfeito e aprovou a sprint 1. 
  - Foram evidenciados alguns bugs, maioritariamente relacionados com a interface, que foram entretanto corrigidos.
  - O sistema utilizado para selecionar o número de estrelas numa avaliação foi alterado para corresponder às expectativas do cliente e aos AC.

- Conclusions: what to add/review
  - Os objetivos propostos para esta sprint foram todos alcançados, estando congruentes com o goal definido para a Release 1 (MVP). 
  - Apesar de no global o tempo estimado ter correspondido ao tempo de desenvolvimento, evidenciámos que tendencialmente: 
    - Subestimarmos tasks relacionadas com funcionalidades;
    - Sobrestimarmos tasks relacionadas a interface.

##### Sprint Retrospective

- What we did well:
  - Conseguimos manter o tempo de desenvolvimento dentro do budget definido;
  - Conseguimos modificar e adaptar o trabalho à visão do cliente
  - Relativamente ao Gitlab, fora algumas especificidades, conseguimos integrar esta ferramenta no nosso processo de desenvolvimento;
  - Expandimos a utilização da base de dados, não apenas para o processo de login, mas também para gerir os restantes dados referentes ao uso da aplicação.

- What we did less well:
  - Demorámos muito tempo a conseguir clarificar, com conjunto com o cliente, qual a visão geral do projeto;
  - Subestimarmos tasks relacionadas com funcionalidades;
  - Sobrestimarmos tasks relacionadas a interface.

- How to improve to the next sprint:
  - Sermos mais rigorosos no registo do tempo efetivo de desenvolvimento;
  - Estimarmos as futuras tasks tendo por base a experiência com tasks semelhantes da Sprint 1
  - No Gitlab, atualizarmos mais frequentemente o trabalho desenvolvido / em desenvolvimento, para facilitar a gestão das tarefas realmente em falta.

***

### Sprint 2
##### Sprint Plan

- Goal: Ter o **Sistema de login** a funcionar e a **Secção de Avaliação da UC e Docente** a permitir acrescentar avaliações, upvotes e downvotes, e com o sistema de moderação automática a funcionar.

- Dates: De 17 de novembro a 30 de novembro | 2 semanas

- Roles:
  - Product Owner: Guilherme Pina
  - Scrum Master: Diogo Coelho

- To do: 
  - User Story 7 - Atribuir upvote / downvote a uma avaliação
  - User Story 8 - Responder a avaliações 
  - User Story 22 - Consultar a secção de avaliação de um docente
  - User Story 23 - Avaliar os docentes que lecionam as minhas turmas
  - Task: Desenvolver um sistema de moderação automática

- Story Points: 1S+2M+1L

- Analysis: O utilizador já consegue efetuar o login na aplicação, inserir novas avaliações nas Secções de Avaliação das UCs e dos docentes, atribuir upvotes / downvotes e responder a avaliações.

##### Sprint Review

- Analysis: what was not done or what was added 
  - A secção de avaliação dos docentes foi adicionada;
  - A funcionalidade de atribuir upvote / downvote a uma avaliação funciona corretamente;
  - A funcionalidade de responder a avaliações apresentava alguns problemas quando o utilizador era um docente;
  - Apesar dos critérios de aceitação terem sido bem redigidos, deveríamos tê-los revisto no final da sprint para garantir que a adição de novas funcionalidades não tivesse comprometido outras tasks.

- Story Points: 1S+2M+1L

- Version: 1.0

- Client analysis: client feedback
  - O cliente ficou satisfeito e aprovou a sprint 2;
  - Foram identificados alguns bugs, maioritariamente relacionados com a US8, que foram entretanto corrigidos;
  - A estrutura da base de dados será mantida.

- Conclusions: what to add/review
  - Com excepção do bug identificado, os objetivos propostos para esta sprint foram alcançados, estando congruentes com o goal definido para a Release 1 (MVP).
  - Comparativamente à Sprint 1 as nossas estimativas estiverem mais aproximadas do tempo efetivo de desenvolvimento.

##### Sprint Retrospective

- What we did well:
  - Conseguimos manter o tempo de desenvolvimento dentro do budget definido;
  - Fizemos melhor uso do Gitlab e conseguimos integrá-lo mais facilmente no nosso processo de desenvolvimento;

- What we did less well:
  - Tivemos algumas dúvidas sobre se a estrutura atual da base de dados estava congruente com a visão do cliente.

- How to improve to the next sprint:
  - Termos mais atenção aos critérios de aceitação e realizar uma revisão dos mesmos quando todas as tasks estiverem concluídas.

***

### Sprint 3
##### Sprint Plan

- Goal: Implementar os fóruns de estudantes, permitir reportar publicações e permitir ao moderador analisar e remover publicações.

- Dates: De 30 de novembro a 15 de Dezembro | 2 semanas

- Roles:
  - Product Owner: Chelsea Duarte
  - Scrum Master: Denise Taúla

- To do:
  - User Story 2 - Visualizar a média das avaliações de uma UC
  - User Story 11 - Criar uma publicação num fórum
  - User Story 12 - Atribuir upvote / downvote às publicações de um fórum
  - User Story 13 - Responder às Publicações de um Forúm
  - User Story 17 - Denunciar uma publicação num fórum
  - User Story 18 - Denunciar uma avaliação
  - User Story 19 - Analisar Publicações reportadas
  - User Story 20 - Apagar Publicações

- Story Points: 3S+4M+1L

- Analysis: ~

##### Sprint Review

- Analysis: what was not done or what was added (Link to US or Task from the PB)  

- Story Points: 1S+2M+1L

- Version: 2.0

- Client analysis: client feedback

- Conclusions: what to add/review

##### Sprint Retrospective

- What we did well:
  - A
- What we did less well:
  - B
- How to improve to the next sprint:
  - C

***

***
