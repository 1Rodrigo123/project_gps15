package project_gps15.ui.Controlers;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import project_gps15.Application;
import project_gps15.data.Topicos;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Post;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.data.post.Resposta;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.ImageLoader;
import project_gps15.ui.SceneManager;
import javafx.fxml.FXML;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

public class ForumEstudantesControler extends BaseController {
    @FXML private SceneManager sceneManager;

    @FXML  TextField TituloPub;
    @FXML  TextArea ConteudoPub;
    @FXML private Button PublicButton;
    @FXML private VBox VboxPub;
    @FXML private Label LabelTop;
    @FXML private Label Caminho;
    @FXML private ScrollPane scrool;
    @FXML public AnchorPane CriarPublicacao;
    @FXML private VBox espaco_publi;

    Topicos topicoatual;

    private String ucatual;
    private List<PublicacaoForum> publicacaoForumList;

    private boolean respostaVisivel = false;
    private VBox respostaBox;
    private boolean clean=false;
    private TextArea respostaTextArea;
    private Button responderButton;
    private UserInfo userInfo; // Guarda o utilizador que está com a sessão iniciada
    private static ForumEstudantesControler instance;

    public static ForumEstudantesControler getInstance(){
        if(instance==null)
            new ForumEstudantesControler();
        return instance;
    }

    public void setUcatual(String ucatual) {
        this.ucatual = ucatual;
    }

    public String getUcatual() {
        return ucatual;
    }

    @Override
    protected void atualiza() {
        EnumFXML enu = EnumFXML.getByName(Application.Scene.get());
        if (enu == EnumFXML.FORUM_ESTUDANTES) {
            userInfo = programManager.getUserInfo();
            publicacaoForumList = programManager.ConsultaPublicacaoForum(topicoatual,ucatual);
            scrool.setVvalue(0);
            if(topicoatual==Topicos.UC){
                LabelTop.setText(ucatual);
                Caminho.setText("Fórum > UCs > " + ucatual);
            }else{
                LabelTop.setText(topicoatual.toString());
                Caminho.setText("Fórum > "+topicoatual);
                ucatual=" ";
            }

            CriarPublicacao.setVisible(userInfo.tipo() != Utilizadores.MODERADOR);

            atualizapublicacoes();

        }
    }

    private void atualizapublicacoes() {
        TituloPub.clear();
        ConteudoPub.clear();
        VboxPub.getChildren().clear();
        VboxPub.setSpacing(10);

        for (PublicacaoForum pi:publicacaoForumList) {
            VBox aux = new VBox();
            HBox hBoxcima = new HBox();
            aux.setSpacing(5);

            HBox baixo = new HBox();
            baixo.setOpaqueInsets(new Insets(0, 0, 0, 20));
            baixo.setSpacing(10);
            baixo.setAlignment(Pos.BASELINE_LEFT);

            TextArea txt = new TextArea();
            txt.setEditable(false);
            txt.setText(pi.getComentario());
            txt.setStyle("-fx-text-fill: black;");
            txt.setMaxHeight(60);

            Label title = new Label(pi.getTitulo());
            title.setStyle("-fx-background-color: steelblue;-fx-border-radius:10");
            title.setPadding(new Insets(5, 10, 5, 10));

            Label texto = new Label(pi.getEmailAutor());
            texto.setPadding(new Insets(5, 10, 5, 10));

            Label data = new Label(pi.getDataPublicacao());
            data.setPadding(new Insets(5, 10, 5, 10));

            hBoxcima.getChildren().addAll(title, new Separator(Orientation.VERTICAL), texto, new Separator(Orientation.VERTICAL), data);
            aux.getChildren().addAll(hBoxcima, txt, baixo);

            Label upvoteLabel = new Label(Integer.toString(pi.getnUpVotes()));
            Label downvoteLabel = new Label(Integer.toString(pi.getnDownVotes()));

            Button upvote = new Button("");
            Button downvote = new Button("");
            downvote.setStyle("-fx-start-margin: 10");


            upvote.setOnAction(e -> onUpvoteButtonClick(pi, upvoteLabel, downvoteLabel, upvote, downvote));
            downvote.setOnAction(e -> onDownvoteButtonClick(pi, upvoteLabel, downvoteLabel, upvote, downvote));


            //Atualiza o Contador de respostas
            Button respostaButton = new Button("");
            respostaButton.setStyle("-fx-start-margin: 10");
            respostaButton.setOnAction(e -> onRespostaButtonClick(pi, aux, respostaButton));

            //Atualiza o Contador de respostas
            atualizaContadorRespostas(respostaButton, pi.getIdPost());

            Button Reportar = new Button("Reportar");

            if (programManager.utilizadorJaReportou(userInfo.email(), pi.getIdPost(), "Post")) {
                Reportar.setStyle("-fx-background-color: #7e3a3a;");
                Reportar.setText("Publicação reportada");
                Reportar.setOnAction(null);
            } else {
                Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
                Reportar.setOnAction(e -> mostraPopupConfirmacaoPost(pi));
            }

            //Botão apagar apenas para o moderador
            Button Apagar = new Button("Apagar");
            Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
            Apagar.setOnAction(e -> mostraPopupApagarPost(pi));

            //Botão cumpre apenas para o moderador
            Button Cumpre = new Button("Cumpre");
            Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white; ");
            Cumpre.setOnAction(e -> {
                //Altera a flag de Reportada de true para false
                programManager.atualizarReportPost(userInfo.email(), pi.getIdPost(), false);
                atualizapublicacoes();
                mostraConfirmacao("Publicação desmarcada", " Esta publicação foi marcada como não reportada com sucesso.");
            });

            Hyperlink verRespostasLink = new Hyperlink("Ver respostas");
            verRespostasLink.setOnAction(e -> {
                // Exibe as respostas
                exibirOuOcultarRespostas(pi, aux);
            });

            if (userInfo.getTipo() != Utilizadores.MODERADOR) {
                baixo.getChildren().addAll(upvote, upvoteLabel,
                        new Separator(Orientation.VERTICAL), downvote, downvoteLabel,
                        new Separator(Orientation.VERTICAL), respostaButton,
                        new Separator(Orientation.VERTICAL), Reportar,
                        new Separator(Orientation.VERTICAL), verRespostasLink);
            }
            //Se for moderador
            else {
                Separator reportSeparator = new Separator(Orientation.VERTICAL);

                if (programManager.postfoiReportado(pi.getIdPost())) {
                    txt.setStyle("-fx-background-color: #f38888;");
                    Reportar.setVisible(false);
                    reportSeparator.setVisible(false);
                } else {
                    Cumpre.setDisable(true);
                }
                baixo.getChildren().addAll(verRespostasLink, new Separator(Orientation.VERTICAL),
                        Apagar, new Separator(Orientation.VERTICAL),
                        Cumpre, reportSeparator, Reportar);
            }

            ImageView upvoteImageView = new ImageView(ImageLoader.getImage("upvote_icon2.png"));
            upvoteImageView.setFitWidth(20);
            upvoteImageView.setFitHeight(20);
            upvote.setGraphic(upvoteImageView);

            ImageView downvoteImageView = new ImageView(ImageLoader.getImage("downvote_icon.png"));
            downvoteImageView.setFitWidth(20);
            downvoteImageView.setFitHeight(20);
            downvote.setGraphic(downvoteImageView);

            ImageView replyImageView = new ImageView(ImageLoader.getImage("reply_icon.png"));
            replyImageView.setFitWidth(20);
            replyImageView.setFitHeight(20);
            respostaButton.setGraphic(replyImageView);
            //aux.set
            VboxPub.getChildren().add(aux);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        ConteudoPub.setOnMouseClicked(mouseEvent -> {
            if(clean)
                ConteudoPub.clear();
            ConteudoPub.setStyle("-fx-text-fill: black;");
            clean=false;
        });
    }

    public ForumEstudantesControler() {
        topicoatual=Topicos.GERAL;
        if(instance==null)
            instance=this;
    }

    public void setTopicoatual(Topicos topicoatual) {
        this.topicoatual = topicoatual;
    }

    @FXML
    public void onPublicar() {
        System.out.println("Registou clique");
        String titulo = TituloPub.getText();
        String conteudo = ConteudoPub.getText();
        if (titulo.isBlank() || conteudo.isBlank()){
            clean=true;
            ConteudoPub.setStyle("-fx-text-fill: red;");
            ConteudoPub.setText("Tem de ter todos os campos preenchidos");
            return;
        }
        String[] palavrasOfensivas = {
                "Cabrão", "Caralho", "Foda", "Filho da mãe", "Desgraçado",
                "Merda", "Puta", "Porra", "Foder", "Estúpido",
                "Imbecil", "Idiota", "Ignorante", "Retardado", "Babaca", "Tosco",
                "Insuportável", "Chato", "Besta", "Estúpido", "Asno", "Patife",
                "Tonto", "Maldito", "Maldita", "Asqueroso", "Vergonha", "Ordinário",
                "Patético", "Nojento", "Asquerosa", "Chata", "Ordinária", "Retardada", "Estúpida", "Nojenta", "Tonta", "Porcaria"
        };

        //boolean contemPalavraOfensiva = false;
        for (String palavra : palavrasOfensivas) {
            if (conteudo.toLowerCase().contains(palavra.toLowerCase())) {
                clean=true;
                ConteudoPub.setStyle("-fx-text-fill: red;");
                ConteudoPub.setText("Não podes publicar conteúdo ofensivo");
                return;
            }
        }

        if (!programManager.fazPublicacao(topicoatual,titulo,conteudo,ucatual))
            System.out.println("Erro a fazer a publicacao");

        atualiza();
    }

    //------------------ UPVOTES E DOWNVOTES --------------

    void onUpvoteButtonClick(PublicacaoForum pf, Label upLabel, Label downLabel, Button upvote, Button downvote) {
        System.out.println("FUNCIONOU O EVENT (Upvote)");

        if (pf != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),pf.getIdPost(),"Post", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),pf.getIdPost(),"Post", "Downvote");

            if (!alreadyUpvoted) {
                // Se ainda não upvotou, incrementa o upvote e define como upvotado
                pf.incrementUpVotes();
                System.out.println(pf.getnUpVotes());
                pf.setUpvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),pf.getIdPost(),"Post", "Upvote");
                upvote.setStyle("-fx-background-color: #4169E1;");
                downvote.setStyle("");

                if (alreadyDownvoted) {
                    // Se já downvotou, decrementa o downvote e define como não downvotado
                    pf.decrementDownVotes();
                    System.out.println(pf.getnDownVotes());
                    pf.setDownvoted(false);
                    downvote.setStyle("");
                    programManager.removeVoto(userInfo.email(), pf.getIdPost(), "Post","Downvote");
                }
            } else {
                // Se já upvotou uma vez, remove o upvote
                pf.decrementUpVotes();
                System.out.println(pf.getnUpVotes());
                pf.setUpvoted(false);
                upvote.setStyle("");
                programManager.removeVoto(userInfo.email(), pf.getIdPost(),"Post", "Upvote");
            }

            programManager.atualizaVotesForum(pf.getIdPost(), pf.getnUpVotes(), pf.getnDownVotes());
            upLabel.setText(Integer.toString(pf.getnUpVotes()));
            downLabel.setText(Integer.toString(pf.getnDownVotes()));
            upvote.setStyle("-fx-background-color: #4169E1;");
            atualizapublicacoes();
        }
    }
    void onDownvoteButtonClick(PublicacaoForum pf, Label upLabel, Label downLabel,Button upvote, Button downvote) {
        if (pf != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),pf.getIdPost(),"Post", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),pf.getIdPost(),"Post", "Downvote");

            if (!alreadyDownvoted) {
                // Se ainda não downvotou, incrementa o downvote e define como downvotado
                pf.incrementDownVotes();
                System.out.println(pf.getnDownVotes());
                pf.setDownvoted(true);
                downvote.setStyle("-fx-background-color: #4169E1;");

                upvote.setStyle("");
                programManager.registarAcaoUtilizador(userInfo.email(), pf.getIdPost(),"Post", "Downvote");

                if (alreadyUpvoted) {
                    // Se já upvotou, decrementa o upvote e define como não upvotado
                    pf.decrementUpVotes();
                    System.out.println(pf.getnUpVotes());
                    pf.setUpvoted(false);
                    upvote.setStyle("");
                    programManager.removeVoto(userInfo.email(), pf.getIdPost(),"Post", "Upvote");
                }
            } else {
                // Se já downvotou uma vez, remove o downvote
                pf.decrementDownVotes();
                System.out.println(pf.getnDownVotes());
                pf.setDownvoted(false);
                //downvote.setStyle("");
                programManager.removeVoto(userInfo.email(), pf.getIdPost(),"Post", "Downvote");
            }

            programManager.atualizaVotesForum(pf.getIdPost(), pf.getnUpVotes(), pf.getnDownVotes());
            upLabel.setText(Integer.toString(pf.getnUpVotes()));
            downLabel.setText(Integer.toString(pf.getnDownVotes()));
            atualizapublicacoes();
        }
    }


//------------------------- Upvotes e Downvotes Respostas

    void onUpvoteButtonClickResposta(Resposta r, Label upLabel, Label downLabel) {
        if (r != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");
            boolean alreadyDownvoted = programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

            if (!alreadyUpvoted) {
                // Se ainda não upvotou, incrementa o upvote e define como upvotado
                r.incrementUpVotes();
                System.out.println(r.getUpvotes());
                r.setUpvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");

                if (alreadyDownvoted) {
                    // Se já downvotou, decrementa o downvote e define como não downvotado
                    r.decrementDownVotes();
                    System.out.println(r.getDownvotes());
                    r.setDownvoted(false);
                    programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Downvote");
                }
            } else {
                // Se já upvotou uma vez, remove o upvote
                r.decrementUpVotes();
                System.out.println(r.getUpvotes());
                r.setUpvoted(false);
                programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Upvote");
            }

            programManager.atualizaVotesRespostas(r.getIdComentario(), r.getUpvotes(), r.getDownvotes());
            upLabel.setText(Integer.toString(r.getUpvotes()));
            downLabel.setText(Integer.toString(r.getDownvotes()));

        }
    }

    void onDownvoteButtonClickResposta(Resposta r, Label upLabel, Label downLabel) {
        System.out.println("FUNCIONOU O EVENT (Downvote)");

        if (r != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

            if (!alreadyDownvoted) {
                // Se ainda não downvotou, incrementa o downvote e define como downvotado
                r.incrementDownVotes();
                System.out.println(r.getDownvotes());
                r.setDownvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

                if (alreadyUpvoted) {
                    // Se já upvotou, decrementa o upvote e define como não upvotado
                    r.decrementUpVotes();
                    System.out.println(r.getUpvotes());
                    r.setUpvoted(false);
                    programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Upvote");
                }
            } else {
                // Se já downvotou uma vez, remove o downvote
                r.decrementDownVotes();
                System.out.println(r.getDownvotes());
                r.setDownvoted(false);
                programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Downvote");
            }

            programManager.atualizaVotesRespostas(r.getIdComentario(), r.getUpvotes(), r.getDownvotes());
            upLabel.setText(Integer.toString(r.getUpvotes()));
            downLabel.setText(Integer.toString(r.getDownvotes()));

        }
    }
    //------------------------------------------ RESPOSTAS ----------------------------------------
    private void onRespostaButtonClick(PublicacaoForum avaliacao, VBox publicacaoBox, Button respostaButton) {
        if (respostaVisivel) {
            publicacaoBox.getChildren().remove(respostaBox);
            respostaVisivel = false;

            publicacaoBox.getChildren().remove(respostaButton);
        } else {
            criarCampoResposta(avaliacao, publicacaoBox, respostaButton);
            publicacaoBox.getChildren().add(respostaBox);
            respostaVisivel = true;

        }
    }

    private void criarCampoResposta(PublicacaoForum avaliacao, VBox publicacaoBox, Button respostaButton) {

        VBox infoBox = new VBox();
        infoBox.setAlignment(Pos.CENTER_LEFT);
        infoBox.setPadding(new Insets(0, 10, 10, 10));
        infoBox.prefHeight(100);
        infoBox.prefWidth(400);

        respostaBox = new VBox();
        respostaBox.setAlignment(Pos.CENTER_LEFT);
        respostaBox.setPadding(new Insets(20, 10, 10, 10));
        respostaBox.prefHeight(250);
        respostaBox.prefWidth(400);
        respostaBox.setStyle("-fx-border-color: #191970; -fx-border-radius: 20;");

        // Adiciona o nome de quem vai responder e a data atual
        String infoResposta = userInfo.email() + " em " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        Label infoLabel = new Label(infoResposta);
        Label errorLabel = new Label("");

        respostaTextArea = new TextArea();
        respostaTextArea.setPromptText("Escreva aqui a sua resposta...");
        respostaTextArea.setPrefWidth(300);
        respostaTextArea.setMaxHeight(Double.MAX_VALUE);

        HBox botoesBox = new HBox(); // Cria uma nova HBox para os botões
        botoesBox.setAlignment(Pos.BASELINE_LEFT);
        botoesBox.setSpacing(10);

        responderButton = new Button("Responder");
        responderButton.setStyle("-fx-background-radius: 5px; -fx-background-color: #B0C4DE; ");

        responderButton.setOnAction(e -> {
            // Verifica se o campo de texto não está vazio
            if (!respostaTextArea.getText().isEmpty()) {

                // Se não estiver vazio, envia a resposta
                enviarResposta(avaliacao, publicacaoBox, respostaButton);
                publicacaoBox.getChildren().remove(respostaBox);
            } else {
                // Se estiver vazio, exibe uma mensagem de erro
                System.out.println("Vazio");
                errorLabel.setStyle("-fx-text-fill: red;");
                errorLabel.setText("Campo de preenchimento obrigatório");
            }
            errorLabel.setText("");
        });

        infoBox.getChildren().addAll(infoLabel,errorLabel);

        // Adiciona o botão de responder à HBox
        botoesBox.getChildren().add( responderButton);

        // Adiciona elementos à respostaBox
        respostaBox.getChildren().addAll(infoBox, respostaTextArea, botoesBox);
    }

    private void enviarResposta(PublicacaoForum avaliacao, VBox publicacaoBox, Button respostaButton) {
        String resposta = respostaTextArea.getText();
        avaliacao.getIdPost();

        programManager.inserirResposta("Post", avaliacao.getIdPost(), userInfo.email(), resposta,false);
        System.out.println("Resposta enviada");

        // Atualiza o contador de respostas
        atualizaContadorRespostas(respostaButton, avaliacao.getIdPost());

        // Mostra a resposta  imediatamente após enviar
        mostrarRespostaAtual(avaliacao, resposta, publicacaoBox);

        // Remove o campo de resposta depois enviar
        respostaTextArea.clear();
        //espaco_publi.getChildren().remove(respostaBox);
        respostaVisivel = false;
    }

    private void atualizaContadorRespostas(Button respostaButton, int idPost) {
        int novoNumeroRespostas = programManager.nRespostasPorPost(idPost);
        respostaButton.setText(String.valueOf(novoNumeroRespostas));
    }

    private void exibirOuOcultarRespostas(PublicacaoForum avaliacao, VBox publicacaoBox) {
        if (!respostaVisivel) {
            List<Resposta> todasRespostas = programManager.obterTodasRespostas(avaliacao.getIdPost(),"Post");

            for (Resposta resposta : todasRespostas) {
                mostrarRespostas(resposta, publicacaoBox);
            }

            respostaVisivel = true; // Define como verdadeiro, pois as respostas estão visíveis
        } else {
            // Caso contrário, remove as respostas
            publicacaoBox.getChildren().removeIf(node -> node instanceof VBox); // Remove todas as respostas (VBoxes)
            respostaVisivel = false; // Define como falso, pois as respostas foram ocultadas
        }
    }

    private void mostrarRespostas(Resposta resposta, VBox publicacaoBox) {

        boolean respostaJaExiste = publicacaoBox.getChildren().stream()
                .filter(node -> node instanceof VBox)
                .anyMatch(box -> ((VBox) box).getChildren().contains(createRespostaVBox(resposta)));

        if (!respostaJaExiste) {
            VBox respostaVBox = createRespostaVBox(resposta);
            publicacaoBox.getChildren().add(respostaVBox);
        }

    }

    private VBox createRespostaVBox(Resposta resposta){
        VBox respostaVBox = new VBox();
        respostaVBox.setAlignment(Pos.CENTER);
        respostaVBox.setPadding(new Insets(0, 10, 10, 0));
        respostaVBox.prefHeight(157);
        respostaVBox.prefWidth(300);
        respostaVBox.setStyle("-fx-border-color:  #ffffff; -fx-border-radius: 0;");
        respostaVBox.setStyle("-fx-background-color: #f1efef; -fx-background-radius: 10; -fx-border-radius: 10; ");

        String infoResposta = resposta.getUtilizador() + " em " + resposta.getDataComentario();

        Label infoLabel = new Label(infoResposta);

        TextArea txtResposta = new TextArea(resposta.getConteudoComentario());
        txtResposta.setEditable(false);
        txtResposta.setPrefWidth(200);
        txtResposta.prefHeight(100);

        HBox botoesBox = new HBox(); // Crie uma nova HBox para os botões
        botoesBox.setAlignment(Pos.BASELINE_LEFT);
        botoesBox.setSpacing(10);

        Label upvoteLabel = new Label(Integer.toString(resposta.getUpvotes()));
        Label downvoteLabel = new Label(Integer.toString(resposta.getDownvotes()));

        Button Reportar = new Button("Reportar");
        if(programManager.utilizadorJaReportou(userInfo.email(), resposta.getIdComentario(),"Comentario")){
            Reportar.setStyle("-fx-background-color: #7e3a3a;");
            Reportar.setText("Resposta reportada");
            Reportar.setOnAction(null);
        }
        else{
            Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
            Reportar.setOnAction(e -> mostraPopupConfirmacaoResposta(resposta));
        }

        //Botão apagar apenas para o moderador
        Button Apagar = new Button("Apagar");
        Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
        Apagar.setOnAction(e -> mostraPopupApagarResposta(resposta));


        //Botão cumpre apenas para o moderador
        Button Cumpre = new Button("Cumpre");
        Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white;");
        Cumpre.setOnAction(e -> {
            //Altera a flag de Reportada de true para false
            programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(), false);
            atualizapublicacoes();
            mostraConfirmacao("Comentário desmarcado", " Este comentário foi marcado como não reportado com sucesso.");
        });

        // Adiciona as imagens aos botões
        ImageView upvoteImageView = new ImageView(ImageLoader.getImage("upvote_icon2.png"));
        upvoteImageView.setFitWidth(20);
        upvoteImageView.setFitHeight(20);
        Button upvoteButton = new Button("", upvoteImageView);
        upvoteButton.setOnAction(e -> onUpvoteButtonClickResposta(resposta, upvoteLabel, downvoteLabel));

        ImageView downvoteImageView = new ImageView(ImageLoader.getImage("downvote_icon.png"));
        downvoteImageView.setFitWidth(20);
        downvoteImageView.setFitHeight(20);
        Button downvoteButton = new Button("", downvoteImageView);
        downvoteButton.setStyle("-fx-start-margin: 10");
        downvoteButton.setOnAction(e -> onDownvoteButtonClickResposta(resposta, upvoteLabel, downvoteLabel));

        if(programManager.utilizadorJaVotou(userInfo.email(),resposta.getIdComentario(),"Comentario", "Upvote")){
            upvoteButton.setOnAction(null);
            upvoteButton.setDisable(true);
        }
        else {
            upvoteButton.setOnAction(e -> onUpvoteButtonClickResposta(resposta, upvoteLabel, downvoteLabel));
        }

        // Adiciona os botões à nova HBox
        if(userInfo.getTipo() != Utilizadores.MODERADOR){
            botoesBox.getChildren().clear();
            botoesBox.getChildren().addAll(upvoteButton, upvoteLabel, new Separator(Orientation.VERTICAL), downvoteButton, downvoteLabel, new Separator(Orientation.VERTICAL), Reportar);
        }
        //Botões para o moderador
        else{
            Separator reportSeparator = new Separator(Orientation.VERTICAL);
            if(programManager.respostaFoiReportada(resposta.getIdComentario())){
                txtResposta.setStyle("-fx-background-color: #f38888;");
                Reportar.setVisible(false);
                reportSeparator.setVisible(false);
            }
            else{
                Cumpre.setDisable(true);
            }
            botoesBox.getChildren().addAll(Apagar , new Separator(Orientation.VERTICAL),Cumpre,reportSeparator,Reportar);
        }

        respostaVBox.getChildren().addAll(infoLabel, txtResposta, botoesBox);
        return respostaVBox;
    }
    private void mostrarRespostaAtual(PublicacaoForum publicacaoForum, String resposta, VBox publicacaoBox) {
        VBox respostaVBox = new VBox();
        respostaVBox.setAlignment(Pos.CENTER);
        respostaVBox.setPadding(new Insets(0, 10, 10, 0));
        respostaVBox.prefHeight(157);
        respostaVBox.prefWidth(300);
        respostaVBox.setStyle("-fx-border-color:  #000000; -fx-border-radius: 0;");


        String infoResposta = "Você em " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));

        Label infoLabel = new Label(infoResposta);

        TextArea txtResposta = new TextArea(resposta);
        txtResposta.setEditable(false);
        txtResposta.setPrefWidth(200);
        txtResposta.prefHeight(100);

        respostaVBox.getChildren().addAll(infoLabel, txtResposta);

        publicacaoBox.getChildren().add(respostaVBox);
    }
    //_________________________ Reportar Publicações _________________________
    private void mostraPopupConfirmacaoPost(Post post) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar publicação");
        alert.setHeaderText("Tem a certeza que deseja reportar esta publicação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Altera a flag de reportada para true
                programManager.atualizarReportPost(userInfo.email(),post.getIdPost(), true);
                atualizapublicacoes();
                mostraConfirmacao("Publicação reportada", " Esta publicação foi reportada com sucesso.");
            }
        });
    }

    private void mostraPopupConfirmacaoResposta(Resposta resposta) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar Comentário");
        alert.setHeaderText("Tem a certeza que deseja reportar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Alterar a flag de reportada para true
                programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(),true);
                atualizapublicacoes();
                mostraConfirmacao("Comentário reportado", " Este comentário foi reportado com sucesso.");
            }
        });
    }

    private void mostraConfirmacao(String titulo, String mensagem) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensagem);
        alert.showAndWait();
    }

    //------------------------------ Apagar Publicações --------------------
    private void mostraPopupApagarPost(Post post){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar publicação");
        alert.setHeaderText("Tem a certeza que deseja apagar esta publicação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarPostEComentarios(post.getIdPost());
                mostraConfirmacao("Publicação apagada", " Esta publicação foi apagada com sucesso.");
            }
        });
        atualiza();
    }
    private void mostraPopupApagarResposta(Resposta resposta){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar Comentário");
        alert.setHeaderText("Tem a certeza que deseja apagar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarResposta(resposta.getIdComentario());
                atualizapublicacoes();
                mostraConfirmacao("Comentário apagado", " Este comentário foi apagado com sucesso.");
            }
        });
    }

    public Topicos getTopicoatual() {
        return topicoatual;
    }

    public List<PublicacaoForum> getPublicacaoForum() {
        return  publicacaoForumList;
    }

    public TextArea getComentario() {
        return ConteudoPub;
    }

}
