package project_gps15.ui.Controlers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import project_gps15.Application;
import project_gps15.data.Utilizadores;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.ImageLoader;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ListaDocenteControler extends BaseController {
    private static ListaDocenteControler instance;
    public VBox semestre1;

    private final Map<Button, String> docentesMap;

    public static ListaDocenteControler getInstance() {
        if (instance == null)
            new ListaDocenteControler();
        return instance;
    }

    public ListaDocenteControler() {
        docentesMap = new HashMap<>();
        if (instance == null)
            instance = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        getInstance();
    }

    @Override
    protected void atualiza() {
        _createListUCs();
       // super.atualiza();

        //_replaceLabels();
    }

    private void _createListUCs() {
        String[] docentes;
        if(programManager.isModerador()){
           docentes = programManager.devolveTodosDocentes();
        }
        else{
          docentes = programManager.devolveDocentes();
        }

        semestre1.getChildren().remove(0, semestre1.getChildren().size());
        for (String docente : docentes) {
            semestre1.getChildren().add(_createBoxDocente(docente));
        }
    }

    private HBox _createBoxDocente(String nomeDocente) {
        // HBox
        HBox myHBox = new HBox();
        myHBox.setAlignment(Pos.CENTER_LEFT);
        myHBox.prefHeight(27.0);
        myHBox.prefWidth(588);
        myHBox.setStyle("-fx-background-color: gainsboro");

        // Separator
        Separator separator = new Separator();
        separator.setLayoutX(32.0);
        separator.setLayoutY(10.0);
        separator.setPrefHeight(13.0);
        separator.setPrefWidth(5.0);
        separator.setVisible(false);

        // Button
        Button button = new Button();
        button.setMnemonicParsing(false);
        button.setPrefHeight(20.0);
        button.setPrefWidth(25.0);
        button.setOnAction(this::gotDocente);   // evento

        // ImageView » button
        ImageView buttonImageView = new ImageView(ImageLoader.getImage("link_icon.png"));
        buttonImageView.setFitHeight(16.0);
        buttonImageView.setFitWidth(17.0);
        buttonImageView.setPickOnBounds(true);
        buttonImageView.setPreserveRatio(true);

        button.setGraphic(buttonImageView);

        // Label
        Label label = new Label(nomeDocente);
        label.setPrefHeight(15.0);
        label.setPrefWidth(350.0);
        label.setText(nomeDocente);
        label.setFont(new Font("Arial Bold", 14.0));

        // Margins...
        HBox.setMargin(separator, new Insets(0.0, 0.0, 0.0, 32.0));
        HBox.setMargin(button, new Insets(0.0, 0.0, 0.0, 0.0));
        HBox.setMargin(label, new Insets(0.0, 0.0, 0.0, 0.0));

        myHBox.getChildren().addAll(separator, button, label);

        docentesMap.put(button, nomeDocente);

        return myHBox;
    }

    @FXML
    public void gotDocente(ActionEvent actionEvent) {
        String nomeDocente = docentesMap.get((Button) actionEvent.getSource());

        PerfilAvaliacaoDocenteControler docenteControler = PerfilAvaliacaoDocenteControler.getInstance();
        docenteControler.setNomeDocente(nomeDocente);
        docenteControler.setBreadCrumbUC(nomeDocente);
        Application.Scene.set(EnumFXML.PERFIL_AVALIACAO_DOCENTES.toString());
    }
}