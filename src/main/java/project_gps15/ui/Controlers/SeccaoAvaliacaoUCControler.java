package project_gps15.ui.Controlers;


import javafx.event.ActionEvent;
import project_gps15.Application;
import project_gps15.data.*;

import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.Resposta;
import project_gps15.ui.EnumFXML;
import javafx.fxml.FXML;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.controlsfx.control.Rating;
import project_gps15.ui.ImageLoader;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;




public class SeccaoAvaliacaoUCControler extends BaseController {
    @FXML public Label breadcrumbs; //  »   @FXML private Label caminho;
    @FXML private Label LabelTopico;
    @FXML private Rating Rating;
    @FXML private AnchorPane campoAvaliacao;
    @FXML private TextArea comentario;
    @FXML private Label Assunto;
    @FXML private Button downVotedButton;
    @FXML private Label errorLabel;
    @FXML private VBox espaco_publi;
    @FXML private Text nDownvoteLabel;
    @FXML private Text nUpvoteLabel;
    @FXML private Button publicar;
    @FXML private CheckBox publicar_ano;
    @FXML private Button upVotedButton;
    @FXML private ScrollPane scrool;
    @FXML public Label labelFiltro;
    @FXML public ComboBox filtroComboBox;

    private VBox respostaBox;
    private TextArea respostaTextArea;
    private Button responderButton;
    private TEMA temaatual;

    private UserInfo userInfo; // Guarda o utilizador que está com a sessão iniciada
    private Avaliacao[] avaliacoes;
    private Avaliacao avaliacaoAtual;

    private double valueRating;
    private int nRespostas;
    private boolean anonimo, respostaVisivel = false;

    // Estas 3 variáveis são o bastante para apresentar as coisas dinamicamente
    private String nome_uc_OR_docente;
    Topicos topicoAtual;
    TipoAvaliacao tipoAvaliacao;

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    private static SeccaoAvaliacaoUCControler sec;// tenho esta variavel para guardar a instance do controller;

    public static SeccaoAvaliacaoUCControler getInstance() {
        if (sec == null)
            new SeccaoAvaliacaoUCControler();
        return sec;
    }

    public SeccaoAvaliacaoUCControler() {
        if (sec == null)
            sec = this;
    }



    public void setTemaatual(TEMA temaatual) {
        this.temaatual = temaatual;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        getInstance();

    }

    @Override
    protected void atualiza() {
        System.out.println("UC: ");
        System.out.println("topico: " + getTopicoAtual());

        EnumFXML enu = EnumFXML.getByName(Application.Scene.get());
        if (enu == EnumFXML.SECCAO_AVALIACAO) {
            errorLabel.setText("");
            scrool.setVvalue(0);
            userInfo = programManager.getUserInfo();
            System.out.println(userInfo.tipo());

            if (userInfo.tipo() != Utilizadores.ESTUDANTE) { //Se for docente ou moderador, não mostra opção de avaliar a UC
                campoAvaliacao.setVisible(false);
                labelFiltro.setVisible(true);
                filtroComboBox.setVisible(true);
            }else {
                campoAvaliacao.setVisible(true);
                labelFiltro.setVisible(false);
                filtroComboBox.setVisible(false);
            }

            avaliacoes = programManager.consultaAvaliacoes(topicoAtual, tipoAvaliacao, nome_uc_OR_docente);

            atualizapublicacoes();
            updateBreadcrumbs();

            if (avaliacoes.length > 1) {
                System.out.println(avaliacoes[1].getComentario());
            }
        }
    }

    @FXML
    private void atualizapublicacoes() {
        espaco_publi.getChildren().clear();
        LabelTopico.setText(topicoAtual.getNome());

        if (avaliacoes.length == 0)
            espaco_publi.getChildren().add(new Label("Ainda não há publicações"));

        else {

            for (Avaliacao a : avaliacoes) {
                VBox aux = new VBox();
                espaco_publi.getChildren().add(aux);
                aux.setAlignment(Pos.CENTER);
                VBox.setMargin(aux, new Insets(5, 0, 0, 3));

                aux.setPadding(new Insets(10, 10, 10, 10));
                aux.prefHeight(157);
                aux.prefWidth(300);
                aux.setStyle("-fx-border-color:  #191970; -fx-border-radius: 20;");

                HBox cima = new HBox();
                aux.getChildren().add(cima);
                cima.setAlignment(Pos.CENTER_LEFT);
                cima.setStyle("-fx-background-color: #ffffff; -fx-background-radius: 10;");
                cima.prefWidth(300);
                cima.prefHeight(30);

                String eval = a.getNota() != 0 ? a.getNota() + "/5" : "";
                Label title = new Label(a.getTopico() + " " + eval);

                cima.getChildren().add(title);
                cima.getChildren().add(new Separator(Orientation.HORIZONTAL));
                String email= a.getIsAnonima()?"Anonimo":a.getEmailAutor();
                cima.getChildren().add(new Hyperlink(email));
                cima.getChildren().add(new Text(a.getDataPublicacao()));

                title.setPadding(new Insets(0, 5, 0, 5));
                title.setStyle("-fx-background-color:  #F5DEB3; -fx-background-radius: 10; -fx-border-radius: 10; ");
                TextArea txt = new TextArea(a.getComentario());
                txt.setEditable(false);
                txt.setPrefWidth(200);
                aux.getChildren().add(txt);
                txt.prefHeight(100);

                HBox baixo = new HBox();

                baixo.setOpaqueInsets(new Insets(0, 0, 0, 20));
                baixo.setSpacing(10);
                baixo.setAlignment(Pos.BASELINE_LEFT);


                Label upvoteLabel = new Label(Integer.toString(a.getnUpVotes()));
                Label downvoteLabel = new Label(Integer.toString(a.getnDownVotes()));

                Button upvote = new Button("");
                Button downvote = new Button("");
                downvote.setStyle("-fx-start-margin: 10");
                upvote.setOnAction(e -> onUpvoteButtonClick(a, upvoteLabel, downvoteLabel));
                downvote.setOnAction(e -> onDownvoteButtonClick(a, upvoteLabel, downvoteLabel));

                //Atualiza o Contador de respostas
                Button respostaButton = new Button("" + nRespostas);
                respostaButton.setStyle("-fx-start-margin: 10");
                respostaButton.setOnAction(e -> onRespostaButtonClick(a, aux, respostaButton));

                //Atualiza o Contador de respostas
                atualizaContadorRespostas(respostaButton, a.getIdPost());

                //Botão reportar
                Button Reportar = new Button("Reportar");

                if(programManager.utilizadorJaReportou(userInfo.email(), a.getIdPost(),"Avaliacao")){
                    Reportar.setStyle("-fx-background-color: #7e3a3a;");
                    Reportar.setText("Avaliação reportada");
                    Reportar.setOnAction(null);
                }
                else{
                    Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
                    Reportar.setOnAction(e -> mostraPopupConfirmacaoAvaliacao(a));
                }

                Hyperlink verRespostasLink = new Hyperlink("Ver respostas");
                verRespostasLink.setOnAction(e -> {
                    // Exibe as respostas
                    exibirOuOcultarRespostas(a, aux);
                });

                //Botão apagar apenas para o moderador
                Button Apagar = new Button("Apagar");
                Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
                Apagar.setOnAction(e -> mostraPopupApagarAvaliacao(a));

                //Botão cumpre apenas para o moderador
                Button Cumpre = new Button("Cumpre");
                Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white; ");
                Cumpre.setOnAction(e -> {
                    //Altera a flag de Reportada de true para false
                    programManager.atualizarReportAvaliacao(userInfo.email(), a.getIdPost(), false);
                    atualizapublicacoes();
                    mostraConfirmacao("Avaliação desmarcada", " Esta avaliação foi marcada como não reportada com sucesso.");
                });

                //Adicionar as imagens aos botoes
                ImageView upvoteImageView = new ImageView(ImageLoader.getImage("upvote_icon2.png"));
                upvoteImageView.setFitWidth(20);
                upvoteImageView.setFitHeight(20);
                upvote.setGraphic(upvoteImageView);

                ImageView downvoteImageView = new ImageView(ImageLoader.getImage("downvote_icon.png"));
                downvoteImageView.setFitWidth(20);
                downvoteImageView.setFitHeight(20);
                downvote.setGraphic(downvoteImageView);

                ImageView replyImageView = new ImageView(ImageLoader.getImage("reply_icon.png"));
                replyImageView.setFitWidth(20);
                replyImageView.setFitHeight(20);
                respostaButton.setGraphic(replyImageView);

                if (userInfo.getTipo() != Utilizadores.MODERADOR) {
                    baixo.getChildren().addAll(upvote, upvoteLabel,
                            new Separator(Orientation.VERTICAL), downvote, downvoteLabel,
                            new Separator(Orientation.VERTICAL), respostaButton,
                            new Separator(Orientation.VERTICAL), Reportar,
                            new Separator(Orientation.VERTICAL), verRespostasLink);
                }

                //Se for moderador
                else{
                    Separator reportSeparator = new Separator(Orientation.VERTICAL);

                    if(programManager.avaliacaofoiReportada(a.getIdPost())){
                        txt.setStyle("-fx-background-color: #f38888;");
                        Reportar.setVisible(false);
                        reportSeparator.setVisible(false);
                    }
                    else{
                        Cumpre.setDisable(true);
                    }

                    baixo.getChildren().addAll(verRespostasLink,new Separator(Orientation.VERTICAL),Apagar,new Separator(Orientation.VERTICAL),Cumpre,reportSeparator,Reportar);
                }

                aux.getChildren().add(baixo);
            }
        }
    }

    @FXML
    private void onRatingchoosen() {
        if (valueRating == Rating.getRating()) {
            Rating.setRating(0);
            valueRating = 0;
            return;
        }
        valueRating = Rating.getRating();

        if (programManager != null)
            System.out.println(userInfo.email());
        else
            System.out.println("Ainda na mesma");
    }

    //----------- ANÓNIMO---------------------
    @FXML
    private void onAnonimo() {
        anonimo = publicar_ano.isSelected();
    }

    //------------------- PUBLICAR ------
    @FXML
    public void onPublicar() {
        errorLabel.setText("");
        System.out.println("FEZ");
        //  System.out.println(programManager.getEmail_user());
        String comment = comentario.getText();
        System.out.println(valueRating);

        // Array de palavras ofensivas
        String[] palavrasOfensivas = {
                "Cabrão", "Caralho", "Foda", "Filho da mãe", "Desgraçado",
                "Merda", "Puta", "Porra", "Foder", "Estúpido",
                "Imbecil", "Idiota", "Ignorante", "Retardado", "Babaca", "Tosco",
                "Insuportável", "Chato", "Besta", "Estúpido", "Asno", "Patife",
                "Tonto", "Maldito", "Maldita", "Asqueroso", "Vergonha", "Ordinário",
                "Patético", "Nojento", "Asquerosa", "Chata", "Ordinária", "Retardada", "Estúpida", "Nojenta", "Tonta", "Porcaria"
        };

        boolean contemPalavraOfensiva = false;

        // Verifica se o comentário contém palavras ofensivas (insensível a maiúsculas/minúsculas)
        for (String palavra : palavrasOfensivas) {
            if (comment.toLowerCase().contains(palavra.toLowerCase())) {
                contemPalavraOfensiva = true;
                break;
            }
        }

        if (topicoAtual != null && (Rating.getRating() != 0 || !comentario.getText().isEmpty())) {
            if (!contemPalavraOfensiva) { // Se não contém palavras ofensivas
                if (programManager.fazavaliacao(topicoAtual, valueRating, comment, publicar_ano.isSelected(), tipoAvaliacao, nome_uc_OR_docente)) {
                    System.out.println("Deu certo");

                    valueRating = 0;
                    Rating.setRating(0);
                    comentario.clear();
                    atualiza();
                } else {
                    System.out.println("Isto vai ser chato");
                }
            } else {
                System.out.println("Comentário contém palavras ofensivas");
                errorLabel.setText("O comentário contém palavras ofensivas. Por favor, reveja o seu comentário.");

            }
        } else {
            System.out.println("Não pode comentar");
            errorLabel.setText("Campo de preenchimento obrigatório");
        }

    }

//_________________________ Reportar Publicações _________________________
    private void mostraPopupConfirmacaoAvaliacao(Avaliacao avaliacao) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar Avaliação");
        alert.setHeaderText("Tem a certeza que deseja reportar esta avaliação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Altera a flag de reportada para true
                programManager.atualizarReportAvaliacao(userInfo.email(),avaliacao.getIdPost(), true);
                atualizapublicacoes();
                mostraConfirmacao("Avaliação reportada", " Esta avaliação foi reportada com sucesso.");
            }
        });
    }

    private void mostraPopupConfirmacaoResposta(Resposta resposta) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar Comentário");
        alert.setHeaderText("Tem a certeza que deseja reportar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Alterar a flag de reportada para true
                programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(),true);
                atualizapublicacoes();
                mostraConfirmacao("Comentário reportado", " Este comentário foi reportado com sucesso.");
            }
        });
}

    private void mostraConfirmacao(String titulo, String mensagem) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensagem);
        alert.showAndWait();
    }

    //------------------------------ Apagar Publicações --------------------
    private void mostraPopupApagarAvaliacao(Avaliacao avaliacao){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar Avaliação");
        alert.setHeaderText("Tem a certeza que deseja apagar esta avaliação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarAvaliacaoEComentarios(avaliacao.getIdPost());
                mostraConfirmacao("Avaliação apagada", " Esta avaliação foi apagada com sucesso.");
            }
        });
        atualiza();
    }
    private void mostraPopupApagarResposta(Resposta resposta){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar Comentário");
        alert.setHeaderText("Tem a certeza que deseja apagar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarResposta(resposta.getIdComentario());
                atualizapublicacoes();
                mostraConfirmacao("Comentário apagado", " Este comentário foi apagado com sucesso.");
            }
        });
    }


// --------------------------- UPVOTES E DOWNVOTES --------------

    void onUpvoteButtonClick(Avaliacao a, Label upLabel, Label downLabel) {
        System.out.println("FUNCIONOU O EVENT (Upvote)");

        if (a != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),a.getIdPost(),"Avaliacao", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),a.getIdPost(),"Avaliacao", "Downvote");

            if (!alreadyUpvoted) {
                // Se ainda não upvotou, incrementa o upvote e define como upvotado
                a.incrementUpVotes();
                System.out.println(a.getnUpVotes());
                a.setUpvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),a.getIdPost(),"Avaliacao", "Upvote");

                if (alreadyDownvoted) {
                    // Se já downvotou, decrementa o downvote e define como não downvotado
                    a.decrementDownVotes();
                    System.out.println(a.getnDownVotes());
                    a.setDownvoted(false);
                    programManager.removeVoto(userInfo.email(), a.getIdPost(), "Avaliacao","Downvote");
                }
            } else {
                // Se já upvotou uma vez, remove o upvote
                a.decrementUpVotes();
                System.out.println(a.getnUpVotes());
                a.setUpvoted(false);
                programManager.removeVoto(userInfo.email(), a.getIdPost(),"Avaliacao", "Upvote");
            }

            programManager.atualizaVotes(a.getIdPost(), a.getnUpVotes(), a.getnDownVotes());
            upLabel.setText(Integer.toString(a.getnUpVotes()));
            downLabel.setText(Integer.toString(a.getnDownVotes()));

        }
    }
    void onDownvoteButtonClick(Avaliacao a, Label upLabel, Label downLabel) {
        System.out.println("FUNCIONOU O EVENT (Downvote)");

        if (a != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),a.getIdPost(),"Avaliacao", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),a.getIdPost(),"Avaliacao", "Downvote");

            if (!alreadyDownvoted) {
                // Se ainda não downvotou, incrementa o downvote e define como downvotado
                a.incrementDownVotes();
                System.out.println(a.getnDownVotes());
                a.setDownvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),a.getIdPost(),"Avaliacao", "Downvote");


                if (alreadyUpvoted) {
                    // Se já upvotou, decrementa o upvote e define como não upvotado
                    a.decrementUpVotes();
                    System.out.println(a.getnUpVotes());
                    a.setUpvoted(false);
                    programManager.removeVoto(userInfo.email(), a.getIdPost(), "Avaliacao","Upvote");
                }
            } else {
                // Se já downvotou uma vez, remove o downvote
                a.decrementDownVotes();
                System.out.println(a.getnDownVotes());
                a.setDownvoted(false);
                programManager.removeVoto(userInfo.email(), a.getIdPost(),"Avaliacao", "Downvote");
            }

            programManager.atualizaVotes(a.getIdPost(), a.getnUpVotes(), a.getnDownVotes());
            upLabel.setText(Integer.toString(a.getnUpVotes()));
            downLabel.setText(Integer.toString(a.getnDownVotes()));

        }
    }

//------------------------------------------ RESPOSTAS ----------------------------------------
    private void onRespostaButtonClick(Avaliacao avaliacao, VBox publicacaoBox, Button respostaButton) {
        if (respostaVisivel) {
            publicacaoBox.getChildren().remove(respostaBox);
            respostaVisivel = false;
            avaliacaoAtual = null;

            publicacaoBox.getChildren().remove(respostaButton);
        } else {
            criarCampoResposta(avaliacao, publicacaoBox, respostaButton);
            publicacaoBox.getChildren().add(respostaBox);
            respostaVisivel = true;
            avaliacaoAtual = avaliacao;
        }
    }

    private void criarCampoResposta(Avaliacao avaliacao, VBox publicacaoBox, Button respostaButton) {

        VBox infoBox = new VBox();
        infoBox.setAlignment(Pos.CENTER_LEFT);
        infoBox.setPadding(new Insets(0, 10, 10, 10));
        infoBox.prefHeight(100);
        infoBox.prefWidth(400);

        respostaBox = new VBox();
        respostaBox.setAlignment(Pos.CENTER_LEFT);
        respostaBox.setPadding(new Insets(20, 10, 10, 10));
        respostaBox.prefHeight(250);
        respostaBox.prefWidth(400);
        respostaBox.setStyle("-fx-border-color: #191970; -fx-border-radius: 20;");

        // Adiciona o nome de quem vai responder e a data atual
        String infoResposta = userInfo.email() + " em " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        Label infoLabel = new Label(infoResposta);
        Label errorLabel = new Label("");

        respostaTextArea = new TextArea();
        respostaTextArea.setPromptText("Escreva aqui a sua resposta...");
        respostaTextArea.setPrefWidth(300);
        respostaTextArea.setMaxHeight(Double.MAX_VALUE);

        HBox botoesBox = new HBox(); // Cria uma nova HBox para os botões
        botoesBox.setAlignment(Pos.BASELINE_LEFT);
        botoesBox.setSpacing(10);
        CheckBox isAnonima=new CheckBox("Responder Anonimamente");

        responderButton = new Button("Responder");
        responderButton.setStyle("-fx-background-radius: 5px; -fx-background-color: #B0C4DE; ");

        responderButton.setOnAction(e -> {
            // Verifica se o campo de texto não está vazio
            if (!respostaTextArea.getText().isEmpty()) {
                // Se não estiver vazio, envia a resposta
                enviarResposta(avaliacao, publicacaoBox, respostaButton,isAnonima);
                publicacaoBox.getChildren().remove(respostaBox);
            } else {
                // Se estiver vazio, exibe uma mensagem de erro
                errorLabel.setStyle("-fx-text-fill: red;");
                errorLabel.setText("Campo de preenchimento obrigatório");
            }
            errorLabel.setText("");
        });

        infoBox.getChildren().addAll(infoLabel,errorLabel);

        // Adiciona o botão de responder à HBox
        botoesBox.getChildren().addAll( isAnonima,new Separator(Orientation.HORIZONTAL),responderButton);

        // Adiciona elementos à respostaBox
        respostaBox.getChildren().addAll(infoBox, respostaTextArea, botoesBox);
    }

    private void enviarResposta(Avaliacao avaliacao, VBox publicacaoBox, Button respostaButton,CheckBox isAnonimo) {
        String resposta = respostaTextArea.getText();
        avaliacao.getIdPost();

        programManager.inserirResposta("Avaliacao", avaliacao.getIdPost(), userInfo.email(), resposta,isAnonimo.isSelected());
        System.out.println("Resposta enviada");

        // Atualiza o contador de respostas
        atualizaContadorRespostas(respostaButton, avaliacao.getIdPost());

        // Mostra a resposta  imediatamente após enviar
        mostrarRespostaAtual(avaliacao, resposta, publicacaoBox);

        // Remove o campo de resposta depois enviar
        respostaTextArea.clear();
        espaco_publi.getChildren().remove(respostaBox);
        respostaVisivel = false;
    }

    private void atualizaContadorRespostas(Button respostaButton, int idAvaliacao) {
        int novoNumeroRespostas = programManager.nRespostasPorAvaliacao(idAvaliacao);
        respostaButton.setText(String.valueOf(novoNumeroRespostas));
    }

    private void exibirOuOcultarRespostas(Avaliacao avaliacao, VBox publicacaoBox) {
        if (!respostaVisivel) {
            List<Resposta> todasRespostas = programManager.obterTodasRespostas(avaliacao.getIdPost(),"Avaliacao");

            for (Resposta resposta : todasRespostas) {
                mostrarRespostas(resposta, publicacaoBox);
            }

            respostaVisivel = true; // Define como verdadeiro, pois as respostas estão visíveis
        } else {
            // Caso contrário, remove as respostas
            publicacaoBox.getChildren().removeIf(node -> node instanceof VBox); // Remove todas as respostas (VBoxes)
            respostaVisivel = false; // Define como falso, pois as respostas foram ocultadas
        }
    }

    private void mostrarRespostas(Resposta resposta, VBox publicacaoBox) {

        boolean respostaJaExiste = publicacaoBox.getChildren().stream()
                .filter(node -> node instanceof VBox)
                .anyMatch(box -> ((VBox) box).getChildren().contains(createRespostaVBox(resposta)));

        if (!respostaJaExiste) {
            VBox respostaVBox = createRespostaVBox(resposta);
            publicacaoBox.getChildren().add(respostaVBox);
        }
    }

    private VBox createRespostaVBox(Resposta resposta){
        VBox respostaVBox = new VBox();

        respostaVBox.setAlignment(Pos.CENTER);
        respostaVBox.setPadding(new Insets(0, 10, 10, 0));
        respostaVBox.prefHeight(157);
        respostaVBox.prefWidth(300);
        respostaVBox.setStyle("-fx-border-color:  #ffffff; -fx-border-radius: 0;");
        respostaVBox.setStyle("-fx-background-color: #f1efef; -fx-background-radius: 10; -fx-border-radius: 10; ");
        String utilizador=resposta.isAnonimo()?"Anonimo":resposta.getUtilizador();

        String infoResposta = utilizador + " em " + resposta.getDataComentario();

        Label infoLabel = new Label(infoResposta);

        TextArea txtResposta = new TextArea(resposta.getConteudoComentario());
        txtResposta.setEditable(false);
        txtResposta.setPrefWidth(200);
        txtResposta.prefHeight(100);

        HBox botoesBox = new HBox(); // Crie uma nova HBox para os botões
        botoesBox.setAlignment(Pos.BASELINE_LEFT);
        botoesBox.setSpacing(10);


        Label upvoteLabel = new Label(Integer.toString(resposta.getUpvotes()));
        Label downvoteLabel = new Label(Integer.toString(resposta.getDownvotes()));

        Button Reportar = new Button("Reportar");
        if(programManager.utilizadorJaReportou(userInfo.email(), resposta.getIdComentario(),"Comentario")){
            Reportar.setStyle("-fx-background-color: #7e3a3a;");
            Reportar.setText("Resposta reportada");
            Reportar.setOnAction(null);
        }
        else{
            Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
            Reportar.setOnAction(e -> mostraPopupConfirmacaoResposta(resposta));
        }

        //Botão apagar apenas para o moderador
        Button Apagar = new Button("Apagar");
        Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
        Apagar.setOnAction(e -> mostraPopupApagarResposta(resposta));


        //Botão cumpre apenas para o moderador
        Button Cumpre = new Button("Cumpre");
        Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white;");
        Cumpre.setOnAction(e -> {
            //Altera a flag de Reportada de true para false
            programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(), false);
            atualizapublicacoes();
            mostraConfirmacao("Comentário desmarcado", " Este comentário foi marcado como não reportado com sucesso.");
        });

        // Adiciona as imagens aos botões
        ImageView upvoteImageView = new ImageView(ImageLoader.getImage("upvote_icon2.png"));
        upvoteImageView.setFitWidth(20);
        upvoteImageView.setFitHeight(20);
        Button upvoteButton = new Button("", upvoteImageView);
        upvoteButton.setOnAction(e -> onUpvoteButtonClickResposta(resposta, upvoteLabel, downvoteLabel));

        ImageView downvoteImageView = new ImageView(ImageLoader.getImage("downvote_icon.png"));
        downvoteImageView.setFitWidth(20);
        downvoteImageView.setFitHeight(20);
        Button downvoteButton = new Button("", downvoteImageView);
        downvoteButton.setStyle("-fx-start-margin: 10");
        downvoteButton.setOnAction(e -> onDownvoteButtonClickResposta(resposta, upvoteLabel, downvoteLabel));


        // Adiciona os botões à nova HBox
        if(userInfo.getTipo() != Utilizadores.MODERADOR){
            botoesBox.getChildren().clear();
            botoesBox.getChildren().addAll(upvoteButton, upvoteLabel, new Separator(Orientation.VERTICAL), downvoteButton, downvoteLabel, new Separator(Orientation.VERTICAL), Reportar);
        }
        //Botões para o moderador
        else{
            Separator reportSeparator = new Separator(Orientation.VERTICAL);
            if(programManager.respostaFoiReportada(resposta.getIdComentario())){
                txtResposta.setStyle("-fx-background-color: #f38888;");
                Reportar.setVisible(false);
                reportSeparator.setVisible(false);
            }
            else{
                Cumpre.setDisable(true);
            }
            botoesBox.getChildren().addAll(Apagar , new Separator(Orientation.VERTICAL),Cumpre,reportSeparator,Reportar);
        }

        respostaVBox.getChildren().addAll(infoLabel, txtResposta, botoesBox);

        return respostaVBox;
    }

    //------------------------- Upvotes e Downvotes Respostas

    void onUpvoteButtonClickResposta(Resposta r, Label upLabel, Label downLabel) {
        System.out.println("FUNCIONOU O EVENT (Upvote)");

        if (r != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

            if (!alreadyUpvoted) {
                // Se ainda não upvotou, incrementa o upvote e define como upvotado
                r.incrementUpVotes();
                System.out.println(r.getUpvotes());
                r.setUpvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");

                if (alreadyDownvoted) {
                    // Se já downvotou, decrementa o downvote e define como não downvotado
                    r.decrementDownVotes();
                    System.out.println(r.getDownvotes());
                    r.setDownvoted(false);
                    programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Downvote");
                }
            } else {
                // Se já upvotou uma vez, remove o upvote
                r.decrementUpVotes();
                System.out.println(r.getUpvotes());
                r.setUpvoted(false);
                programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Upvote");
            }

            programManager.atualizaVotesRespostas(r.getIdComentario(), r.getUpvotes(), r.getDownvotes());
            upLabel.setText(Integer.toString(r.getUpvotes()));
            downLabel.setText(Integer.toString(r.getDownvotes()));
        }
    }

    void onDownvoteButtonClickResposta(Resposta r, Label upLabel, Label downLabel) {
        System.out.println("FUNCIONOU O EVENT (Downvote)");

        if (r != null) {
            boolean alreadyUpvoted = programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Upvote");
            boolean alreadyDownvoted =  programManager.utilizadorJaVotou(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

            if (!alreadyDownvoted) {
                // Se ainda não downvotou, incrementa o downvote e define como downvotado
                r.incrementDownVotes();
                System.out.println(r.getDownvotes());
                r.setDownvoted(true);
                programManager.registarAcaoUtilizador(userInfo.email(),r.getIdComentario(),"Comentario", "Downvote");

                if (alreadyUpvoted){
                    // Se já upvotou, decrementa o upvote e define como não upvotado
                    r.decrementUpVotes();
                    r.setUpvoted(false);
                    programManager.removeVoto(userInfo.email(), r.getIdComentario(),"Comentario", "Upvote");
                }
            } else {
                // Se já downvotou uma vez, remove o downvote
                r.decrementDownVotes();
                System.out.println(r.getDownvotes());
                r.setDownvoted(false);
                programManager.removeVoto(userInfo.email(), r.getIdComentario(), "Comentario","Downvote");
            }

            programManager.atualizaVotesRespostas(r.getIdComentario(), r.getUpvotes(), r.getDownvotes());
            upLabel.setText(Integer.toString(r.getUpvotes()));
            downLabel.setText(Integer.toString(r.getDownvotes()));
        }
    }

    private void mostrarRespostaAtual(Avaliacao avaliacao, String resposta, VBox publicacaoBox) {
        VBox respostaVBox = new VBox();
        respostaVBox.setAlignment(Pos.CENTER);
        respostaVBox.setPadding(new Insets(0, 10, 10, 0));
        respostaVBox.prefHeight(157);
        respostaVBox.prefWidth(300);
        respostaVBox.setStyle("-fx-border-color:  #000000; -fx-border-radius: 0;");


        String infoResposta = "Você em " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));

        Label infoLabel = new Label(infoResposta);

        TextArea txtResposta = new TextArea(resposta);
        txtResposta.setEditable(false);
        txtResposta.setPrefWidth(200);
        txtResposta.prefHeight(100);

        respostaVBox.getChildren().addAll(infoLabel, txtResposta);

        publicacaoBox.getChildren().add(respostaVBox);
    }

    /* ---------- GETS & SETS ------------- */
    public void updateBreadcrumbs() {
        String caminho = "";
        switch (temaatual) {
            case LEI -> {
                caminho = "Lei > " + (tipoAvaliacao == TipoAvaliacao.DOCENTE ? "Docentes > " : "UC > ") + nome_uc_OR_docente + " > " + topicoAtual.getNome();
                Assunto.setText(nome_uc_OR_docente);
            }
            case IPC -> {
                caminho = "IPC > " + topicoAtual.getNome();
                Assunto.setText("IPC");
            }
            case ISEC -> {
                caminho = "ISEC >" + topicoAtual.getNome();
                Assunto.setText("ISEC");
            }
        }
        this.breadcrumbs.setText(caminho);
    }

    @FXML
    public void aplicarFiltro(ActionEvent event) {
        String filtro = filtroComboBox.getValue().toString();
        if ("Todas Avaliações".equals(filtro)) {
            System.out.println("Foi todas");
            avaliacoes = programManager.consultaAvaliacoes(topicoAtual, tipoAvaliacao, nome_uc_OR_docente);
        } else if ("Avaliações Reportadas".equals(filtro)) {
            System.out.println("Foi reportadas");
            avaliacoes = programManager.consultaAvaliacoesReportadas(topicoAtual, tipoAvaliacao, nome_uc_OR_docente);
        }

        atualizapublicacoes();
    }


    public void setTipoAvaliacao(TipoAvaliacao tipoAvaliacao) {
        this.tipoAvaliacao = tipoAvaliacao;
    }

    public void setAvaliacaoAtual(Avaliacao avaliacaoAtual) {
        this.avaliacaoAtual = avaliacaoAtual;
    }
    public Avaliacao[] getAvaliacoes() {
        return avaliacoes;
    }

    public double getValueRating() {
        return valueRating;
    }
    public void setValueRating(double valueRating) {
        this.valueRating = valueRating;
    }

    public Topicos getTopicoAtual() {
        return topicoAtual;
    }
    public void setTopicoAtual(Topicos topicoAtual) {
        this.topicoAtual = topicoAtual;
    }


    public void setNome_uc_OR_docente(String nome_uc_OR_docente) {
        this.nome_uc_OR_docente = nome_uc_OR_docente;
    }

    public TextArea getComentario() {
        return comentario;
    }

    public Label getErrorLabel() {
        return errorLabel;
    }
}