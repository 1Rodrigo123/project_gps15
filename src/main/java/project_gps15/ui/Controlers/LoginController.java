package project_gps15.ui.Controlers;

import project_gps15.Application;
import project_gps15.data.UserInfo;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.SceneManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import static project_gps15.Application.programManager;

public class LoginController  implements Initializable{
    @FXML TextField emailField;
    @FXML PasswordField passwordField;
    @FXML Label errorLabel;

    SceneManager sceneManager;
    private static LoginController login;

   public static LoginController getInstance() {
       if(login==null)
            new LoginController();
       return login;
    }

    public LoginController() {
       if(login == null)
            login=this;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        getInstance();
    }

    @FXML
    void onEmailFieldClicked(KeyEvent event) {
        if(emailField != null) {
            emailField.textProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue.isEmpty()) {
                    if (!isValidEmail(newValue)) {
                        //System.out.println("Formatacao de Email invalida");
                        emailField.setStyle("-fx-background-color: red;"); // Coloca o campo a vermelho se o email for inválido
                    } else {
                        emailField.setStyle(""); // Limpa o estilo se o email for válido
                        System.out.println("Formatacao de Email valida");
                    }
                }
            });
        }
    }

    @FXML
    void onLoginButtonClick(ActionEvent event) throws SQLException {
        String usermail = emailField.getText();
        String password = passwordField.getText();

        //Task5 da AC1 - Quando o utilizador não introduz dados
        if(usermail.isEmpty() && password.isEmpty()) {
            System.out.println("campos nulos");
            errorLabel.setText("Campos de preenchimento obrigatório. Por favor, preencha os campos.");
        }
        //Task2 - Quando o utilizador introduz um formato de email inválido
        else if (!isValidEmail(usermail)) {
            //System.out.println("Formato de email invalido");
            errorLabel.setText("Formato de email inválido. Por favor, verifique o seu email.");
        }
        //Passou pelas validações de input, então vai validar as credenciais
        else {
            UserInfo userInfo = programManager.isValidLogin(usermail, password);

            //Task3 e Task4 da AC1 - Quando o utilizador introduz dados inválidos
            if(userInfo == null){
                System.out.println("Invalid login");
                errorLabel.setText("Credenciais inválidas. Por favor, verifique o seu email e/ou palavra-passe.");
            }

            //Task1 - Login efetuado com sucesso
            else {
                System.out.println("Valid login");
                System.out.println(userInfo);
                errorLabel.setText("");

                if(programManager.registoJaExiste(usermail)){
                    System.out.println("Utilizador já registado");
                    Application.Scene.set(EnumFXML.HOME.toString());
                  //  PerfilDocenteControler perfilDocenteControler = PerfilDocenteControler.getPerfil();
                 //   perfilDocenteControler.initialize();
                  //  PerfilAlunoControler perfilAlunoControler = PerfilAlunoControler.getPerfil();
                    //perfilAlunoControler.initialize();
                }
                else {
                    System.out.println("Utilizador não registado");

                    if(programManager.fazRegistoUtilizador(userInfo)) { // Regista o utilizador na base de dados (se ainda não estiver registado)
                        System.out.println("Registo efetuado com sucesso");
                        programManager.setUserInfo(userInfo);
                        //Registou então vai para a homepage
                        Application.Scene.set(EnumFXML.HOME.getPath());
                    }
                    else{
                        System.out.println("Registo falhou");
                    }
                }
            }
        }
    }

    public void setSceneManager(SceneManager sceneManager){
        this.sceneManager=sceneManager;
    }

    //Função que verifica se o email introduzido pertence ao ISEC, ESAC, ESEC, ESTGOH, ESTeSC ou ISCAC (máscara)
    boolean isValidEmail(String email) {
        return email.matches("\\S+(@isec\\.pt|esac\\.pt|esec\\.pt|estgoh\\.pt|estesc\\.pt|iscac\\.pt)");
    }

    // Parece que o problema do TAB é da versão do JavaFX
    @FXML
    public void handleKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.TAB) {
            if (event.getSource() == emailField) {
                passwordField.requestFocus();
            } else if (event.getSource() == passwordField) {
                emailField.requestFocus();
            }
        }
    }
}