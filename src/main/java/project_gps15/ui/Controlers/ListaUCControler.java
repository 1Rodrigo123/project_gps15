package project_gps15.ui.Controlers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import project_gps15.Application;
import project_gps15.data.Topicos;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.ImageLoader;

import java.net.URL;
import java.util.*;

public class ListaUCControler extends BaseController {
    private static ListaUCControler instance;
    public VBox containerSemestres;

    private final Map<Button, String> ucsMap;


    public static ListaUCControler getInstance() {
        if (instance == null)
            new ListaUCControler();
        return instance;
    }

    public ListaUCControler() {
        ucsMap = new HashMap<>();
        if (instance == null)
            instance = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        getInstance();
    }

    @Override
    protected void atualiza() {
        _createListUCs();
       // super.atualiza();

        //_replaceLabels();
    }

    // Vou considerar que só existem cadeiras do 1º e 2º semestre
    private void _createListUCs() {
        containerSemestres.getChildren().clear();
        Label semestreLabel;
        List<String> ucs;

        // isto seria para correr os semestres todos mas vou só colocar 2
        for(int n_semestre=1; n_semestre<=2; ++n_semestre) {
            if(programManager.isModerador()){
                ucs = programManager.devolveTodasUCsPorSemestre(n_semestre);
            }
            else{
                ucs = programManager.devolveUCsPorSemestre(n_semestre);
            }

            if(!ucs.isEmpty()) {
                semestreLabel = _createLabelSemestre(n_semestre);
                containerSemestres.getChildren().add(semestreLabel);
                for (String uc : ucs)
                    containerSemestres.getChildren().add(_createBoxUC(uc));
            }
        }
    }

    private Label _createLabelSemestre(int semestre) {
        Label myLabel = new Label();
        myLabel.setText(semestre + "º Semestre");
        myLabel.setFont(new Font("System Bold", 17.0));
        myLabel.setTextFill(Color.STEELBLUE);
        myLabel.setPrefHeight(39.0);
        return myLabel;
    }

    private HBox _createBoxUC(String nomeUC) {
        // HBox
        HBox myHBox = new HBox();
        myHBox.setAlignment(Pos.CENTER_LEFT);
        myHBox.prefHeight(27.0);
        myHBox.prefWidth(588);
        myHBox.setStyle("-fx-background-color: gainsboro");

        // Separator
        Separator separator = new Separator();
        separator.setLayoutX(32.0);
        separator.setLayoutY(10.0);
        separator.setPrefHeight(13.0);
        separator.setPrefWidth(5.0);
        separator.setVisible(false);

        // Button
        Button button = new Button();
        button.setMnemonicParsing(false);
        button.setPrefHeight(20.0);
        button.setPrefWidth(25.0);
        button.setOnAction(this::gotoUC);   // evento

        // ImageView » button
        ImageView buttonImageView = new ImageView(ImageLoader.getImage("link_icon.png"));
        buttonImageView.setFitHeight(16.0);
        buttonImageView.setFitWidth(17.0);
        buttonImageView.setPickOnBounds(true);
        buttonImageView.setPreserveRatio(true);

        button.setGraphic(buttonImageView);

        // Label
        Label label = new Label(nomeUC);
        label.setPrefHeight(15.0);
        label.setPrefWidth(350.0);
        label.setText(nomeUC);
        label.setFont(new Font("Arial Bold", 14.0));

        // Margins...
        HBox.setMargin(separator, new Insets(0.0, 0.0, 0.0, 32.0));
        HBox.setMargin(button, new Insets(0.0, 0.0, 0.0, 0.0));
        HBox.setMargin(label, new Insets(0.0, 0.0, 0.0, 0.0));

        myHBox.getChildren().addAll(separator, button, label);

        ucsMap.put(button, nomeUC);

        return myHBox;
    }

    @FXML
    public void gotoUC(ActionEvent actionEvent) {
        String nomeUC = ucsMap.get((Button) actionEvent.getSource());
        if(programManager.isFlag()){
        PerfilUCControler ucControler = PerfilUCControler.getInstance();
        ucControler.setNomeUc(nomeUC);
        ucControler.setBreadCrumbUC(nomeUC);
        Application.Scene.set(EnumFXML.PERFIL_UC.toString());
        }else{
            ForumEstudantesControler forum=ForumEstudantesControler.getInstance();
            forum.setTopicoatual(Topicos.UC);
            forum.setUcatual(nomeUC);
            Application.Scene.set(EnumFXML.FORUM_ESTUDANTES.toString());


        }

    }
}