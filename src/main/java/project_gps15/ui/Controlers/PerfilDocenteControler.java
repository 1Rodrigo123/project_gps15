package project_gps15.ui.Controlers;

import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import project_gps15.data.Utilizadores;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import project_gps15.data.UserInfo;
import project_gps15.ui.ImageLoader;

import java.util.List;

import static project_gps15.Application.programManager;

public class PerfilDocenteControler extends BaseController {
    @FXML private Button EditarPerfilButton;
    @FXML private Pane MeuPerfilContent;
    @FXML private Pane NotificationContent;
    @FXML private Pane ProfileContent;
    @FXML private Pane SecurityContent;
    @FXML private Pane VisibilityContent;
    @FXML private VBox menuLateral;
    @FXML private Pane menuLateralEditar;
    @FXML private Pane menuPerfilAtividade;
    @FXML private Button notificacaoButton;
    @FXML private Label pathLabel;
    @FXML private Button perfilButton;
    @FXML private Button segurancaButton;
    @FXML private Button visibilidadeButton;

    @FXML private Label nomeInfo;
    @FXML private Label nomeLabel;

    @FXML public Label n_upvotes;
    @FXML public Label n_downvotes;
    @FXML public Label n_respostas;

    @FXML public VBox container_ucs;

    public Label nomeUtilizadorLabel;
    public Label nomeUtilizadorInfo;
    public Label emailInfo;



    private static PerfilDocenteControler perfil;

    public static PerfilDocenteControler getPerfil() {
        if (perfil == null)
            new PerfilDocenteControler();
        return perfil;
    }

    @FXML
    public void initialize() {
        UserInfo userInfo = programManager.getUserInfo();
        getPerfil();

        if (userInfo != null && userInfo.getTipo() == Utilizadores.DOCENTE) {
            setUserInfo(userInfo);
            System.out.println("ENTROU DOCENTE");
        }
    }

    public PerfilDocenteControler() {
        if (perfil == null)
            perfil = this;
    }

    @Override
    protected void atualiza() {
        n_respostas.setText(programManager.countPublicacoes('r') + " Respostas");
        n_upvotes.setText(programManager.countVotes('u') + " Upvotes");
        n_downvotes.setText(programManager.countVotes('d') + " Downvotes");
        _createListUCs();
    }
    void voltarinical() {
        menuLateralEditar.setVisible(false);

        menuLateral.setVisible(true);
        menuPerfilAtividade.setVisible(true);
        menuPerfilAtividade.toFront();
        menuPerfilAtividade.requestFocus();

        MeuPerfilContent.setVisible(true);
        MeuPerfilContent.toFront();

        ProfileContent.setVisible(false);
        VisibilityContent.setVisible(false);
        SecurityContent.setVisible(false);
        NotificationContent.setVisible(false);
        nomeLabel.setText(programManager.getUserInfo().nome());
        emailInfo.setText(programManager.getUserInfo().email());
        nomeInfo.setText(programManager.getUserInfo().nome());
        nomeUtilizadorLabel.setText(programManager.getUserInfo().nome());
        nomeUtilizadorInfo.setText(programManager.getUserInfo().nome());


        pathLabel.setText("Home > Meu Perfil");
    }


    // Método para mostrar o conteúdo do perfil inicialmente
    private void showMeuPerfilContent() {
        MeuPerfilContent.setVisible(true);
        ProfileContent.toBack();
        ProfileContent.setVisible(false); // Certifique-se de ocultar o ProfileContent se não for o conteúdo inicial

        // Adicione outras configurações necessárias para exibir o perfil inicial
        // Por exemplo, você pode configurar outras propriedades dos elementos dentro do MeuPerfilContent, se necessário
    }

    private void _createListUCs() {
        container_ucs.getChildren().remove(1, container_ucs.getChildren().size());

        List<String> ucs = programManager.devolveUCsPorSemestre(0);

        for(String uc : ucs) {
            container_ucs.getChildren().add(_create_HBox(uc));
        }
    }

    private HBox _create_HBox(String uc_name) {
        HBox hbox = new HBox();

        // Separator
        Separator separator1 = new Separator();
        separator1.setVisible(false);

        // ImageView
        ImageView imageView = new ImageView(ImageLoader.getImage("link_icon.png"));
        imageView.setFitHeight(19.0);
        imageView.setFitWidth(19.0);
        imageView.setPreserveRatio(true);

        // Separator
        Separator separator2 = new Separator();
        separator2.setVisible(false);

        // Label
        Label label = new Label(uc_name);
        label.setPrefHeight(15.0);
        label.setPrefWidth(227.0);
        label.setFont(new Font("Arial Bold", 11.0));

        // Add components to HBox
        hbox.getChildren().addAll(separator1, imageView, separator2, label);

        return hbox;
    }

    @FXML
    void onEditarPerfilClick(ActionEvent event) {
        menuLateral.setVisible(false);
        menuPerfilAtividade.setVisible(false);
        MeuPerfilContent.setVisible(false);
        menuLateralEditar.requestFocus();
        menuLateralEditar.toFront();
        menuLateralEditar.setVisible(true);
        menuLateralEditar.toFront();
        ProfileContent.toFront();
        ProfileContent.setVisible(true);
        EditarPerfilButton.setVisible(false);
        onPerfilDocenteClick(event);

        pathLabel.setText("Home > Meu Perfil > Editar Perfil > Perfil");
    }

    @FXML
    void onNotificacaoClick(ActionEvent event) {
        MeuPerfilContent.setVisible(false);
        ProfileContent.setVisible(false);
        VisibilityContent.setVisible(false);
        SecurityContent.setVisible(false);
        NotificationContent.toFront();
        NotificationContent.setVisible(true);
        perfilButton.setStyle("-fx-background-color:  #ff000");
        visibilidadeButton.setStyle("-fx-background-color:  #ff000");
        segurancaButton.setStyle("-fx-background-color:  #ff000");

        pathLabel.setText("Home > Meu Perfil > Editar Perfil > Notificações");
        notificacaoButton.setStyle("-fx-background-color:  #B0C4DE");
    }

    @FXML
    void onPerfilDocenteClick(ActionEvent event) {
        MeuPerfilContent.setVisible(false);
        NotificationContent.setVisible(false);
        VisibilityContent.setVisible(false);
        SecurityContent.setVisible(false);
        notificacaoButton.setStyle("-fx-background-color:  #ff000");
        visibilidadeButton.setStyle("-fx-background-color:  #ff000");
        segurancaButton.setStyle("-fx-background-color:  #ff000");

        ProfileContent.toFront();
        ProfileContent.setVisible(true);
        pathLabel.setText("Home > Meu Perfil > Editar Perfil > Perfil");
        perfilButton.setStyle("-fx-background-color:  #B0C4DE");
    }

    @FXML
    void onSegurancaClick(ActionEvent event) {
        MeuPerfilContent.setVisible(false);
        ProfileContent.setVisible(false);
        VisibilityContent.setVisible(false);
        NotificationContent.setVisible(false);
        perfilButton.setStyle("-fx-background-color:  #ff000");
        visibilidadeButton.setStyle("-fx-background-color:  #ff000");
        notificacaoButton.setStyle("-fx-background-color:  #ff000");

        SecurityContent.toFront();
        SecurityContent.setVisible(true);
        pathLabel.setText("Home > Meu Perfil > Editar Perfil > Segurança");
        segurancaButton.setStyle("-fx-background-color:  #B0C4DE");
    }

    @FXML
    void onVisibilidadeClick(ActionEvent event) {
        MeuPerfilContent.setVisible(false);
        ProfileContent.setVisible(false);
        NotificationContent.setVisible(false);
        SecurityContent.setVisible(false);
        perfilButton.setStyle("-fx-background-color:  #ff000");
        notificacaoButton.setStyle("-fx-background-color:  #ff000");
        segurancaButton.setStyle("-fx-background-color:  #ff000");

        VisibilityContent.toFront();
        VisibilityContent.setVisible(true);
        pathLabel.setText("Home > Meu Perfil > Editar Perfil > Visibilidade");
        visibilidadeButton.setStyle("-fx-background-color:  #B0C4DE");
    }

    @FXML
    public void setUserInfo(UserInfo userInfo) {
        if (userInfo != null) {
            //nomeInfo.setText(userInfo.getNome());
            System.out.println(userInfo.getNome());
            //mailInfo.setText(userInfo.getEmail());
            //telemovelInfo.setText(userInfo.getTelemovel());
            //estatutoInfo.setText("Docente");
        }
    }
}