package project_gps15.ui.Controlers;

import javafx.scene.control.TitledPane;
import project_gps15.data.TEMA;
import project_gps15.data.Topicos;
import project_gps15.data.Utilizadores;
import project_gps15.manager.ProgramManager;
import project_gps15.Application;

import project_gps15.ui.EnumFXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class BaseController implements Initializable {
    @FXML public TitledPane Forumop;
    @FXML public Button MeuPerfil;
    @FXML public TitledPane IpcPane;
    @FXML public TitledPane IsecPane;

    protected ProgramManager programManager;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.programManager= Application.programManager;
        Application.Scene.addListener(observable -> atualiza());
    }

    @FXML
    protected void atualiza() {
        if (Forumop != null) {
            if (programManager.isDocente())
                Forumop.setVisible(false);
            else if (programManager.isModerador())
                MeuPerfil.setVisible(false);
            else
                Forumop.setVisible(true);
                MeuPerfil.setVisible(true);
        }
    }

    @FXML
    private void onLogout(ActionEvent e){
     //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        Application.Scene.set(EnumFXML.LOGIN.toString());
    }

    @FXML
    private void OnLeiDocentes(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.LEI);
        if(programManager.isDocente()){
            PerfilAvaliacaoDocenteControler docenteControler = PerfilAvaliacaoDocenteControler.getInstance();
            String nomeDocente=programManager.getUserInfo().nome();
            docenteControler.setNomeDocente(nomeDocente);
            docenteControler.setBreadCrumbUC(nomeDocente);
            Application.Scene.set(EnumFXML.PERFIL_AVALIACAO_DOCENTES.toString());
        }else{
        Application.Scene.set(EnumFXML.LISTA_DOCENTES.toString());}
    }
    @FXML
    private void OnGeralIPC(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.IPC);
        sec.setTopicoAtual(Topicos.GERAL_IPC);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnGestaoIPC(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.IPC);
        sec.setTopicoAtual(Topicos.GESTAO);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnServicosIPC(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.IPC);
        sec.setTopicoAtual(Topicos.SERVICOS);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnServicosIsec(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.ISEC);
        sec.setTopicoAtual(Topicos.SERVICOS_ISEC);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnGeralIsec(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.ISEC);
        sec.setTopicoAtual(Topicos.GERAL_ISEC);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnGestaoIsec(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.ISEC);
        sec.setTopicoAtual(Topicos.GESTAO_ISEC);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }
    @FXML
    private void OnInstalacoesIsec(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.ISEC);
        sec.setTopicoAtual(Topicos.INSTALACOES);
        sec.atualiza();
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void onHome(ActionEvent e){
        //   System.out.println(Logout.getText());
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());

        Application.Scene.set(EnumFXML.HOME.toString());
    }

    @FXML
    private void onGeralForum(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        ForumEstudantesControler.getInstance().setTopicoatual(Topicos.GERAL);
        ForumEstudantesControler.getInstance().atualiza();
        Application.Scene.set(EnumFXML.FORUM_ESTUDANTES.toString());
    }
    @FXML
    private void onIpcForum(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        ForumEstudantesControler.getInstance().setTopicoatual(Topicos.IPC);
        ForumEstudantesControler.getInstance().atualiza();
        Application.Scene.set(EnumFXML.FORUM_ESTUDANTES.toString());
    }
    @FXML
    private void onIsecForum(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        ForumEstudantesControler.getInstance().setTopicoatual(Topicos.ISEC);
        ForumEstudantesControler.getInstance().atualiza();
        Application.Scene.set(EnumFXML.FORUM_ESTUDANTES.toString());

    }
    @FXML
    private void onCursosForum(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        ForumEstudantesControler.getInstance().setTopicoatual(Topicos.UC);
        ForumEstudantesControler.getInstance().atualiza();
        programManager.setFlag(false);
        Application.Scene.set(EnumFXML.LISTA_UCS.toString());

    }

    @FXML
    private void onLeiUCs(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        programManager.setFlag(true);
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.LEI);
        Application.Scene.set(EnumFXML.LISTA_UCS.toString());
    }

    @FXML
    private void onLeiCurso(ActionEvent e){
        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        SeccaoAvaliacaoUCControler sec=SeccaoAvaliacaoUCControler.getInstance();
        sec.setTemaatual(TEMA.LEI);
        Application.Scene.set(EnumFXML.PERFIL_UC.toString());
    }

    @FXML
    private void onMeuPerfil(ActionEvent e){

        Button aux= (Button) e.getSource();
        System.out.println(aux.getText());
        if(programManager.getUserInfo().getTipo()== Utilizadores.ESTUDANTE){
            Application.Scene.set(EnumFXML.PERFIL_ALUNO.toString());
            PerfilAlunoControler.getPerfil().voltarinical();}
        else {
            Application.Scene.set(EnumFXML.PERFIL_DOCENTE.toString());
            PerfilDocenteControler.getPerfil().voltarinical();
        }
    }
}