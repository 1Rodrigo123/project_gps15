package project_gps15.ui.Controlers;

import javafx.scene.control.Label;
import org.controlsfx.control.Rating;
import project_gps15.Application;
import project_gps15.data.TipoAvaliacao;
import project_gps15.data.Topicos;
import project_gps15.data.post.Avaliacao;
import project_gps15.ui.EnumFXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class PerfilAvaliacaoDocenteControler extends BaseController {
    @FXML public Label breadCrumbDocente;
    @FXML public Label nomeDocente;
    @FXML private Label nomepequeno;

    @FXML public Rating star_1_cap_cientifica;
    @FXML public Rating star_2_cap_pedagogica;
    @FXML public Rating star_3_cap_organizacional;
    @FXML public Label label_rating;
    @FXML public Label label_nAvaliacoes;

    private static PerfilAvaliacaoDocenteControler instance;

    public static PerfilAvaliacaoDocenteControler getInstance(){
        if(instance==null)
            new PerfilAvaliacaoDocenteControler();
        return instance;
    }

    public PerfilAvaliacaoDocenteControler() {
        if(instance==null)
            instance=this;
    }

    @FXML
    private void GeralClick(ActionEvent e) {
        SeccaoAvaliacaoUCControler sc = SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.GERAL_DOCENTE);
        sc.setTipoAvaliacao(TipoAvaliacao.DOCENTE);
        sc.setNome_uc_OR_docente(nomeDocente.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void PedagocicaClick(ActionEvent e) {
        SeccaoAvaliacaoUCControler sc = SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.CAPACIDADE_PEDAGOGICA);
        sc.setTipoAvaliacao(TipoAvaliacao.DOCENTE);
        sc.setNome_uc_OR_docente(nomeDocente.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void CientificaClick(ActionEvent e) {
        SeccaoAvaliacaoUCControler sc = SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.CAPACIDADE_CIENTIFICA);
        sc.setTipoAvaliacao(TipoAvaliacao.DOCENTE);
        sc.setNome_uc_OR_docente(nomeDocente.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void OrganizacionalClick(ActionEvent e) {
        SeccaoAvaliacaoUCControler sc = SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.CAPACIDADE_ORGANIZACIONAL);
        sc.setTipoAvaliacao(TipoAvaliacao.DOCENTE);
        sc.setNome_uc_OR_docente(nomeDocente.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    /* ---------- Classificações ------------ */
    @Override
    protected void atualiza() {
     //   super.atualiza();
        _atualizaEstrelas();
    }

    // Está extremamente redudante mas agora não dá para mais
    private void _atualizaEstrelas() {
        Avaliacao[] avaliacoes = null;
        int acc_notas, total_aval_por_topico, total_avaliacoes = 0;

        for (Topicos topico : Topicos.getTopicosDocentes()) {
            acc_notas = total_aval_por_topico = 0;
            avaliacoes = programManager.consultaAvaliacoes(topico, TipoAvaliacao.DOCENTE, nomeDocente.getText());
            total_avaliacoes += avaliacoes.length;

            for (Avaliacao av : avaliacoes) {
                if(av.getNota() > 0) {
                    acc_notas += av.getNota();
                    ++total_aval_por_topico;
                }
            }
            switch (topico) {
                case GERAL_DOCENTE -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0) {
                        double nota = (double) acc_notas / total_aval_por_topico;
                        label_rating.setText(String.format("%1.1f/5", nota));
                    }
                    else
                        label_rating.setText("-/5");
                }
                case CAPACIDADE_CIENTIFICA -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0) {
                        star_1_cap_cientifica.setRating((double) acc_notas / total_aval_por_topico);
                    }
                    else
                        star_1_cap_cientifica.setRating(0.0);
                }
                case CAPACIDADE_PEDAGOGICA -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0)
                        star_2_cap_pedagogica.setRating((double) acc_notas / total_aval_por_topico);
                    else
                        star_2_cap_pedagogica.setRating(0.0);
                }
                case CAPACIDADE_ORGANIZACIONAL -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0)
                        star_3_cap_organizacional.setRating((double) acc_notas / total_aval_por_topico);
                    else
                        star_3_cap_organizacional.setRating(0.0);
                }
            }
            label_nAvaliacoes.setText(total_avaliacoes + " avaliações");
        }
    }


    /* ---------- GETS & SETS ------------ */
    public String getNomeDocente() { return nomeDocente.getText(); }
    public void setNomeDocente(String nome) {
        nomeDocente.setText(nome);
        nomepequeno.setText("@" + nome);
    }

    public String getBreadCrumbDocente() {
        return breadCrumbDocente.getText();
    }
    public void setBreadCrumbUC(String caminho) {
        breadCrumbDocente.setText(" LEI > Docentes > " + caminho);
    }
}