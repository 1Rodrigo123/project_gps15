package project_gps15.ui.Controlers;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.Post;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.data.post.Resposta;
import project_gps15.ui.ImageLoader;
import project_gps15.ui.SceneManager;
import javafx.fxml.FXML;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class HomepageControler extends BaseController {
    @FXML private VBox espaco_publi;
    @FXML public Label labelFiltro;
    @FXML public ComboBox filtroComboBox;
    private Avaliacao[] avaliacoes;
    private  List<Resposta> respostas;
    private List<PublicacaoForum> posts;
    private UserInfo userInfo;
    private static HomepageControler sec;
    public static HomepageControler getSec() {
        if (sec == null)
            new HomepageControler();
        return sec;
    }

    public HomepageControler() {
        if (sec == null)
            sec = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        getSec();
    }

    @Override
    protected void atualiza(){
        super.atualiza();
        espaco_publi.getChildren().clear();
        userInfo = programManager.getUserInfo();

        if (userInfo.tipo() != Utilizadores.MODERADOR) {
            espaco_publi.setVisible(false);
            filtroComboBox.setVisible(false);
            labelFiltro.setVisible(false);
        }else {
            espaco_publi.setVisible(true);
        }
    }
    @FXML
    private void atualizapublicacoes() {
        espaco_publi.getChildren().clear();

        if (avaliacoes.length == 0)
            espaco_publi.getChildren().add(new Label("Ainda não há publicações reportadas."));
        else {
            for (Avaliacao a : avaliacoes) {
                VBox aux = new VBox();
                espaco_publi.getChildren().add(aux);
                aux.setAlignment(Pos.CENTER);
                VBox.setMargin(aux, new Insets(5, 0, 0, 3));

                aux.setPadding(new Insets(10, 10, 10, 10));
                aux.prefHeight(157);
                aux.prefWidth(300);
                aux.setStyle("-fx-border-color:  #191970; -fx-border-radius: 20;");

                HBox cima = new HBox();
                aux.getChildren().add(cima);
                cima.setAlignment(Pos.CENTER_LEFT);
                cima.setStyle("-fx-background-color: #ffffff; -fx-background-radius: 10;");
                cima.prefWidth(300);
                cima.prefHeight(30);

                String eval = a.getNota() != 0 ? a.getNota() + "/5" : "";
                Label title = new Label(a.getTopico() + " " + eval);

                cima.getChildren().add(title);
                cima.getChildren().add(new Separator(Orientation.HORIZONTAL));
                String email= a.getIsAnonima()?"Anonimo":a.getEmailAutor();
                cima.getChildren().add(new Hyperlink(email));
                cima.getChildren().add(new Text(a.getDataPublicacao()));

                title.setPadding(new Insets(0, 5, 0, 5));
                title.setStyle("-fx-background-color:  #F5DEB3; -fx-background-radius: 10; -fx-border-radius: 10; ");
                TextArea txt = new TextArea(a.getComentario());
                txt.setEditable(false);
                txt.setPrefWidth(200);
                aux.getChildren().add(txt);
                txt.prefHeight(100);

                HBox baixo = new HBox();

                baixo.setOpaqueInsets(new Insets(0, 0, 0, 20));
                baixo.setSpacing(10);
                baixo.setAlignment(Pos.BASELINE_LEFT);

                //Botão reportar
                Button Reportar = new Button("Reportar");
                Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
                Reportar.setOnAction(e -> mostraPopupConfirmacaoAvaliacao(a));


                //Botão apagar
                Button Apagar = new Button("Apagar");
                Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
                Apagar.setOnAction(e -> mostraPopupApagarAvaliacao(a));

                //Botão cumpre
                Button Cumpre = new Button("Cumpre");
                Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white; ");
                Cumpre.setOnAction(e -> {
                    programManager.atualizarReportAvaliacao(userInfo.email(), a.getIdPost(), false);
                    atualizapublicacoes();
                    mostraConfirmacao("Avaliação desmarcada", " Esta avaliação encontra-se não reportada com sucesso.");
                });


                Separator reportSeparator = new Separator(Orientation.VERTICAL);

                if(programManager.avaliacaofoiReportada(a.getIdPost())){
                    txt.setStyle("-fx-background-color: #f38888;");
                    Reportar.setVisible(false);
                    reportSeparator.setVisible(false);
                }
                else{
                    Cumpre.setDisable(true);
                }

                baixo.getChildren().addAll(Apagar,new Separator(Orientation.VERTICAL),Cumpre,reportSeparator,Reportar);
                aux.getChildren().add(baixo);
            }

        }
    }

    private void atualizapublicacoesForum() {

        espaco_publi.getChildren().clear();
        espaco_publi.setSpacing(10);

        for (PublicacaoForum pi:posts) {
            VBox aux = new VBox();
            HBox hBoxcima = new HBox();
            aux.setSpacing(5);
            HBox baixo = new HBox();
            TextArea txt = new TextArea();
            txt.setEditable(false);
            txt.setText(pi.getComentario());
            txt.setStyle("-fx-text-fill: black;");
            txt.setMaxHeight(60);
            Label title = new Label(pi.getTitulo());
            title.setStyle("-fx-background-color: steelblue;-fx-border-radius:10");
            title.setPadding(new Insets(5, 10, 5, 10));
            Label texto = new Label(pi.getEmailAutor());
            texto.setPadding(new Insets(5, 10, 5, 10));
            Label data = new Label(pi.getDataPublicacao());
            data.setPadding(new Insets(5, 10, 5, 10));
            hBoxcima.getChildren().addAll(title, new Separator(Orientation.VERTICAL), texto, new Separator(Orientation.VERTICAL), data);
            aux.getChildren().addAll(hBoxcima, txt, baixo);


            Button Reportar = new Button("Reportar");

            if (programManager.utilizadorJaReportou(userInfo.email(), pi.getIdPost(), "Post")) {
                Reportar.setStyle("-fx-background-color: #7e3a3a;");
                Reportar.setText("Publicação reportada");
                Reportar.setOnAction(null);
            } else {
                Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
                Reportar.setOnAction(e -> mostraPopupConfirmacaoPost(pi));
            }

            //Botão apagar apenas para o moderador
            Button Apagar = new Button("Apagar");
            Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
            Apagar.setOnAction(e -> mostraPopupApagarPost(pi));

            //Botão cumpre apenas para o moderador
            Button Cumpre = new Button("Cumpre");
            Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white; ");
            Cumpre.setOnAction(e -> {
                //Altera a flag de Reportada de true para false
                programManager.atualizarReportPost(userInfo.email(), pi.getIdPost(), false);
                atualizapublicacoes();
                mostraConfirmacao("Publicação desmarcada", " Esta publicação foi marcada como não reportada com sucesso.");
            });

            //Se for moderador
                Separator reportSeparator = new Separator(Orientation.VERTICAL);

                if (programManager.postfoiReportado(pi.getIdPost())) {
                    txt.setStyle("-fx-background-color: #f38888;");
                    Reportar.setVisible(false);
                    reportSeparator.setVisible(false);
                } else {
                    Cumpre.setDisable(true);
                }
                baixo.getChildren().addAll(
                        Apagar, new Separator(Orientation.VERTICAL),
                        Cumpre, reportSeparator, Reportar);
            //aux.set
            espaco_publi.getChildren().add(aux);
        }

    }
    //_________________________ Reportar Publicações _________________________
    private void mostraPopupConfirmacaoAvaliacao(Avaliacao avaliacao) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar Avaliação");
        alert.setHeaderText("Tem a certeza que deseja reportar esta avaliação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Altera a flag de reportada para true
                programManager.atualizarReportAvaliacao(userInfo.email(),avaliacao.getIdPost(), true);
                atualizapublicacoes();
                mostraConfirmacao("Avaliação reportada", " Esta avaliação foi reportada com sucesso.");
            }
        });
    }

    private void mostraPopupConfirmacaoResposta(Resposta resposta) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar Comentário");
        alert.setHeaderText("Tem a certeza que deseja reportar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Alterar a flag de reportada para true
                programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(),true);
                atualizapublicacoes();
                mostraConfirmacao("Comentário reportado", " Este comentário foi reportado com sucesso.");
            }
        });
    }

    private void mostraConfirmacao(String titulo, String mensagem) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titulo);
        alert.setHeaderText(null);
        alert.setContentText(mensagem);
        alert.showAndWait();
    }

    private void mostraPopupConfirmacaoPost(Post post) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reportar publicação");
        alert.setHeaderText("Tem a certeza que deseja reportar esta publicação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                //Altera a flag de reportada para true
                programManager.atualizarReportPost(userInfo.email(),post.getIdPost(), true);
                atualizapublicacoes();
                mostraConfirmacao("Publicação reportada", " Esta publicação foi reportada com sucesso.");
            }
        });
    }
    private void mostraPopupApagarPost(Post post){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar publicação");
        alert.setHeaderText("Tem a certeza que deseja apagar esta publicação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarPostEComentarios(post.getIdPost());
                mostraConfirmacao("Publicação apagada", " Esta publicação foi apagada com sucesso.");
            }
        });
        atualiza();
    }
    //------------------------------ Apagar Publicações --------------------
    private void mostraPopupApagarAvaliacao(Avaliacao avaliacao){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar Avaliação");
        alert.setHeaderText("Tem a certeza que deseja apagar esta avaliação?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarAvaliacaoEComentarios(avaliacao.getIdPost());
                mostraConfirmacao("Avaliação apagada", " Esta avaliação foi apagada com sucesso.");
            }
        });
        atualiza();
    }
    private void mostraPopupApagarResposta(Resposta resposta){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Apagar Comentário");
        alert.setHeaderText("Tem a certeza que deseja apagar este comentário?");
        alert.setContentText("Clique em 'OK' para confirmar ou 'Cancelar' para desistir.");

        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                programManager.apagarResposta(resposta.getIdComentario());
                atualizapublicacoes();
                mostraConfirmacao("Comentário apagado", " Este comentário foi apagado com sucesso.");
            }
        });
    }

    //------------------------------------------ RESPOSTAS ----------------------------------------

    private void mostrarRespostas() {
        espaco_publi.getChildren().clear();

        if (respostas.size() == 0)
            espaco_publi.getChildren().add(new Label("Ainda não há publicações reportadas."));
        else {
            for (Resposta r : respostas) {
                VBox respostaVBox = createRespostaVBox(r);
                espaco_publi.getChildren().add(respostaVBox);
            }
        }
    }

    private VBox createRespostaVBox(Resposta resposta){
        VBox respostaVBox = new VBox();

        respostaVBox.setAlignment(Pos.CENTER);
        respostaVBox.setPadding(new Insets(0, 10, 10, 0));
        respostaVBox.prefHeight(157);
        respostaVBox.prefWidth(300);
        respostaVBox.setStyle("-fx-border-color:  #ffffff; -fx-border-radius: 0;");
        respostaVBox.setStyle("-fx-background-color: #f1efef; -fx-background-radius: 10; -fx-border-radius: 10; ");
        String utilizador=resposta.isAnonimo()?"Anonimo":resposta.getUtilizador();

        String infoResposta = utilizador + " em " + resposta.getDataComentario();

        Label infoLabel = new Label(infoResposta);

        TextArea txtResposta = new TextArea(resposta.getConteudoComentario());
        txtResposta.setEditable(false);
        txtResposta.setPrefWidth(200);
        txtResposta.prefHeight(100);

        HBox botoesBox = new HBox(); // Crie uma nova HBox para os botões
        botoesBox.setAlignment(Pos.BASELINE_LEFT);
        botoesBox.setSpacing(10);

        //Botão reportar
        Button Reportar = new Button("Reportar");
        Reportar.setStyle("-fx-background-color: #a90909; -fx-text-fill: #000000; ");
        Reportar.setOnAction(e -> mostraPopupConfirmacaoResposta(resposta));

        //Botão apagar
        Button Apagar = new Button("Apagar");
        Apagar.setStyle("-fx-background-color: #d70303; -fx-text-fill: white; ");
        Apagar.setOnAction(e -> mostraPopupApagarResposta(resposta));

        //Botão cumpre
        Button Cumpre = new Button("Cumpre");
        Cumpre.setStyle("-fx-background-color: #078016; -fx-text-fill: white;");
        Cumpre.setOnAction(e -> {
            //Altera a flag de Reportada de true para false
            programManager.atualizarReportResposta(userInfo.email(),resposta.getIdComentario(), false);
            atualizapublicacoes();
            mostraConfirmacao("Comentário desmarcado", " Este comentário foi marcado como não reportado com sucesso.");
        });

        //Botões para o moderador

        Separator reportSeparator = new Separator(Orientation.VERTICAL);
        if(programManager.respostaFoiReportada(resposta.getIdComentario())){
            txtResposta.setStyle("-fx-background-color: #f38888;");
            Reportar.setVisible(false);
            reportSeparator.setVisible(false);
        }
        else{
            Cumpre.setDisable(true);
        }
        botoesBox.getChildren().addAll(Apagar , new Separator(Orientation.VERTICAL),Cumpre,reportSeparator,Reportar);

        respostaVBox.getChildren().addAll(infoLabel, txtResposta, botoesBox);

        return respostaVBox;
    }

    @FXML
    public void aplicarFiltro(ActionEvent event) {
        String filtro = filtroComboBox.getValue().toString();
        if ("Avaliações Reportadas".equals(filtro)) {
            avaliacoes = programManager.consultaTodasAvaliacoesReportadas();
            atualizapublicacoes();
        } else if ("Publicações Reportadas".equals(filtro)) {
            System.out.println("Publicações Reportadas");
             posts = programManager.ConsultaPublicacoesReportadasForum();
            atualizapublicacoesForum();
        }
        else{
            respostas = programManager.consultaTodasRespostasReportadas();
            mostrarRespostas();
        }

    }

}