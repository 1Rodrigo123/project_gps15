package project_gps15.ui.Controlers;

import javafx.scene.control.Label;
import org.controlsfx.control.Rating;
import project_gps15.Application;
import project_gps15.data.TipoAvaliacao;
import project_gps15.data.Topicos;
import project_gps15.data.post.Avaliacao;
import project_gps15.ui.EnumFXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.net.URL;
import java.util.ResourceBundle;

public class PerfilUCControler extends BaseController {
    private static PerfilUCControler instance;

    @FXML public Label breadCrumbUC;
    @FXML public Label nomeUC;
    @FXML public Rating star_1_organizacao;
    @FXML public Rating star_2_conteudos;
    @FXML public Rating star_3_metodoAvaliacao;
    @FXML public Label label_rating;
    @FXML public Label label_nAvaliacoes;

    public static PerfilUCControler getInstance() {
        if (instance == null)
            new PerfilUCControler();
        return instance;
    }

    public PerfilUCControler() {
        if (instance == null)
            instance = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        getInstance();
    }

    @FXML
    private void Geralclick(ActionEvent e){
        SeccaoAvaliacaoUCControler sc=SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.GERAL);
        sc.setTipoAvaliacao(TipoAvaliacao.UC);
        sc.setNome_uc_OR_docente(nomeUC.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void OrganizacaoClick(ActionEvent e){
        SeccaoAvaliacaoUCControler sc=SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.ORGANIZACAO);
        sc.setTipoAvaliacao(TipoAvaliacao.UC);
        sc.setNome_uc_OR_docente(nomeUC.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void Conteudosclick(ActionEvent e){
        SeccaoAvaliacaoUCControler sc=SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.CONTEUDOS);
        sc.setTipoAvaliacao(TipoAvaliacao.UC);
        sc.setNome_uc_OR_docente(nomeUC.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    @FXML
    private void Metodosclick(ActionEvent e){
        SeccaoAvaliacaoUCControler sc=SeccaoAvaliacaoUCControler.getInstance();
        sc.setTopicoAtual(Topicos.METODOS_AVALIACAO);
        sc.setTipoAvaliacao(TipoAvaliacao.UC);
        sc.setNome_uc_OR_docente(nomeUC.getText());
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
    }

    /* ---------- Classificações ------------ */

    @Override
    protected void atualiza() {
      //  super.atualiza();
        _atualizaEstrelas();
    }

    // Está extremamente redudante mas agora não dá para mais
    private void _atualizaEstrelas() {
        Avaliacao[] avaliacoes = null;
        int acc_notas, total_aval_por_topico, total_avaliacoes = 0;

        for (Topicos topico : Topicos.getTopicosUCs()) {
            acc_notas = total_aval_por_topico = 0;
            avaliacoes = programManager.consultaAvaliacoes(topico, TipoAvaliacao.UC, nomeUC.getText());
            total_avaliacoes += avaliacoes.length;  // avaliações com 0 estrelas não entram para as classifições

            for (Avaliacao av : avaliacoes) {
                if(av.getNota() > 0) {
                    acc_notas += av.getNota();
                    ++total_aval_por_topico;
                }
            }
            switch (topico) {
                case GERAL -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0) {
                        double nota = (double) acc_notas / total_aval_por_topico;
                        label_rating.setText(String.format("%1.1f/5", nota));
                    }
                    else
                        label_rating.setText("-/5");
                }
                case ORGANIZACAO -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0) {
                        star_1_organizacao.setRating((double) acc_notas / total_aval_por_topico);
                    }
                    else
                        star_1_organizacao.setRating(0.0);
                }
                case CONTEUDOS -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0)
                        star_2_conteudos.setRating((double) acc_notas / total_aval_por_topico);
                    else
                        star_2_conteudos.setRating(0.0);
                }
                case METODOS_AVALIACAO -> {
                    if(acc_notas > 0 && total_aval_por_topico > 0)
                        star_3_metodoAvaliacao.setRating((double) acc_notas / total_aval_por_topico);
                    else
                        star_3_metodoAvaliacao.setRating(0.0);
                }
            }
            label_nAvaliacoes.setText(total_avaliacoes + " avaliações");
        }
    }

    /* ---------- GETS & SETS ------------ */
    public String getNomeUc() { return nomeUC.getText(); }
    public void setNomeUc(String nome) { nomeUC.setText(nome); }

    public String getBreadCrumbUC() {
        return breadCrumbUC.getText();
    }
    public void setBreadCrumbUC(String caminho) {
        breadCrumbUC.setText(" LEI > UCS > " + caminho);
    }
}