package project_gps15.ui;

import javafx.scene.image.Image;

import java.io.InputStream;
import java.util.HashMap;

public class ImageLoader {
    private static HashMap<String, Image> images = new HashMap<>();

    private ImageLoader() {
    }

    public static Image getImage(String filename) {
        Image image = images.get(filename);

        if (image == null) {
            try (InputStream is = ImageLoader.class.getResourceAsStream("/images/" + filename))
            {
                image = new Image(is);
                images.put(filename, image);
            } catch (Exception e) {
                return null;
            }
        }
        return image;
    }
}