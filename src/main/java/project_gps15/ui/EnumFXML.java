package project_gps15.ui;

public enum EnumFXML {
    LOGIN("/fxml/login.fxml"),
    MENU_LATERAL("/fxml/menu-lateral.fxml"),

    HOME("/fxml/homepage.fxml"),
    PERFIL_DOCENTE("/fxml/perfil-docente.fxml"),
    SECCAO_AVALIACAO("/fxml/seccao-avaliacao.fxml"),
    FORUM_ESTUDANTES("/fxml/forum-estudantes.fxml"),
    LISTA_UCS("/fxml/lista-ucs.fxml"),
    LISTA_DOCENTES("/fxml/lista-docentes.fxml"),

    PERFIL_UC("/fxml/perfil-uc.fxml"),
    PERFIL_AVALIACAO_DOCENTES("/fxml/perfil-avaliacao-docentes.fxml"),
    PERFIL_CURSO("/fxml/perfil-curso.fxml"),
    PERFIL_ALUNO("/fxml/perfil-aluno.fxml");

    private final String path;

    EnumFXML(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public static EnumFXML getByName(String name) {
        for (EnumFXML value : EnumFXML.values()) {
            if (value.name().equals(name))
                return value;
        }
        throw new IllegalArgumentException("Valor inválido " + name);
    }
}