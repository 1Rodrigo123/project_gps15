package project_gps15.ui;

import project_gps15.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SceneManager {

    private SplitPane splitPane;
    private EnumFXML paneAtual;
    Map<EnumFXML, Pane> panes = new HashMap<>();

    public SceneManager(Stage stage) {
        paneAtual = EnumFXML.LOGIN;

        registerHandlers();
        Scene scene = createViews();
        //update();

        stage.setResizable(false);
        stage.setScene(scene);
    }

    private Scene createViews() {
        panes = new HashMap<>();
        StackPane stackPane = new StackPane();
        splitPane = new SplitPane();

        try {
            for(EnumFXML f : EnumFXML.values()) {
                //getClass().se
                FXMLLoader loader = new FXMLLoader(getClass().getResource(f.getPath()));
                Pane pane = loader.load();

                if (f != EnumFXML.LOGIN && f != EnumFXML.MENU_LATERAL) {
                    pane.setVisible(false);
                    stackPane.getChildren().add(pane);
                }
                panes.put(f, pane);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        splitPane.getItems().add(panes.get(EnumFXML.MENU_LATERAL));
        splitPane.getItems().add(stackPane);
        //split.getItems().addAll(myPanes.get(EnumFXML.MENU_LATERAL), stackPane);
        splitPane.setDividerPositions(0.265);

        StackPane principal = new StackPane(panes.get(EnumFXML.LOGIN), splitPane);
        Scene scene = new Scene(principal);

        switchScene(EnumFXML.LOGIN);
        return scene;
    }

    private void registerHandlers() {
        Application.Scene.addListener(observable -> update());
    }

    public void switchScene(EnumFXML e) {

        System.out.println("listener update");

        splitPane.setVisible(e != EnumFXML.LOGIN);

        panes.get(paneAtual).setVisible(false);


        Pane aux = panes.get(e);

        aux.requestFocus();
        aux.setVisible(true);

        paneAtual = e;
    }

    private void update() {
        EnumFXML e = EnumFXML.getByName(Application.Scene.get());
        switchScene(e);
    }
}