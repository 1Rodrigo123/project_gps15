package project_gps15;

import project_gps15.manager.ProgramManager;
import project_gps15.ui.Controlers.HomepageControler;

import project_gps15.ui.EnumFXML;
import project_gps15.ui.SceneManager;
import project_gps15.ui.Controlers.LoginController;
import javafx.beans.property.SimpleStringProperty;
import javafx.stage.Stage;


import java.io.IOException;


public class Application extends javafx.application.Application {
    public static ProgramManager programManager;
    public static SimpleStringProperty Scene = new SimpleStringProperty(EnumFXML.LOGIN.toString());

    @Override
    public void start(Stage stage) throws IOException {
        programManager = new ProgramManager();
        SceneManager sceneManager = new SceneManager(stage);
        LoginController loginController = LoginController.getInstance();
        loginController.setSceneManager(sceneManager);
        HomepageControler homePageControler = new HomepageControler();

        stage.setTitle("IPC4US");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}