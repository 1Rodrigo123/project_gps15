package project_gps15.data.user;

import javafx.scene.image.Image;

abstract public class User {
    protected String nomeCompleto;
    protected String username;
    protected String email;
    protected Image fotoPerfil;

    protected int numIdent; // nº de Estudante / Docente

    public User(String nomeCompleto, String username, String email, Image fotoPerfil, int numIdent) {
        this.nomeCompleto = nomeCompleto;
        this.username = username;
        this.email = email;
        this.fotoPerfil = fotoPerfil;
        this.numIdent = numIdent;
    }

    @Override
    public String toString() {
        return nomeCompleto + ";" + email;
    }

    // ----------- Gets e Sets

    public String getNomeCompleto() { return nomeCompleto; }

    public String getUsername() { return username; }

    public String getEmail() { return email; }

    public Image getFotoPerfil() { return fotoPerfil; }
    public void setFotoPerfil(Image fotoPerfil) { this.fotoPerfil = fotoPerfil; }

    public int getNumIdent() { return numIdent; }
}
