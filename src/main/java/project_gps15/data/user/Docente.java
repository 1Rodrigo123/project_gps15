package project_gps15.data.user;

// Faz sentido ter uma lista das publicações de cada user?

import javafx.scene.image.Image;

public class Docente extends User {
    public final static String tipoUser = "Docente";

    private int notaGeral;
    private int notaCapPedagogica;
    private int notaCapCientifica;
    private int notaCapOrganizacional;

    private boolean ocultarNotas;

    public Docente(String nomeCompleto, String username, String email, Image fotoPerfil, int numIdent) {
        super(nomeCompleto, username, email, fotoPerfil, numIdent);
        notaGeral = notaCapPedagogica = notaCapCientifica = notaCapOrganizacional = 0;
        ocultarNotas = false;
    }

    @Override
    public String toString() {
        return nomeCompleto + ";" + email + ";" + Estudante.tipoUser;
    }

    // ----------- Gets e Sets

    public int getNotaGeral() { return notaGeral; }
    public void setNotaGeral(int notaGeral) { this.notaGeral = notaGeral; }
    public int getNotaCapPedagogica() { return notaCapPedagogica; }
    public void setNotaCapPedagogica(int notaCapPedagogica) { this.notaCapPedagogica = notaCapPedagogica; }
    public int getNotaCapCientifica() { return notaCapCientifica; }
    public void setNotaCapCientifica(int notaCapCientifica) { this.notaCapCientifica = notaCapCientifica; }
    public int getNotaCapOrganizacional() { return notaCapOrganizacional; }
    public void setNotaCapOrganizacional(int notaCapOrganizacional) { this.notaCapOrganizacional = notaCapOrganizacional; }

    public boolean isOcultarNotas() { return ocultarNotas; }
    public void setOcultarNotas(boolean ocultarNotas) { this.ocultarNotas = ocultarNotas; }
}
