package project_gps15.data.user;

import javafx.scene.image.Image;

public class Estudante extends User {
    public final static String tipoUser = "Estudante";
    //private List<Badge> badges;

    public Estudante(String nomeCompleto, String username, String email, Image fotoPerfil, int numIdent) {
        super(nomeCompleto, username, email, fotoPerfil, numIdent);
    }

    @Override
    public String toString() {
        return nomeCompleto + ";" + email + ";" + Estudante.tipoUser;
    }
}