package project_gps15.data;

public enum TipoAvaliacao {
    UC("UCs"),
    DOCENTE("Docentes"),
    ISEC("ISEC"),
    IPC("IPC");

    private final String nome;
    TipoAvaliacao(String nome) {
        this.nome = nome;
    }

    public String getNome() { return nome; }
}
