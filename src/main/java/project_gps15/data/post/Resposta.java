package project_gps15.data.post;

import project_gps15.data.user.User;

public class Resposta {
    private int idComentario;
    private String tipoPublicacao;
    private int idPublicacao;
    private String utilizador;
    private String conteudoComentario;
    private String dataComentario;
    private int upvotes;
    private int downvotes;
    private boolean flagUpvoted;
    private boolean flagDownvoted;

    private boolean isAnonimo;


    // Construtor
    public Resposta(int idComentario, String tipoPublicacao,boolean isAnonimo, int idPublicacao, String utilizador,
                    String conteudoComentario, String dataComentario, int upvotes, int downvotes) {
        this.idComentario = idComentario;
        this.tipoPublicacao = tipoPublicacao;
        this.idPublicacao = idPublicacao;
        this.utilizador = utilizador;
        this.isAnonimo=isAnonimo;
        this.conteudoComentario = conteudoComentario;
        this.dataComentario = dataComentario;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
    }



    // Getters e Setters

    public boolean isAnonimo() {
        return isAnonimo;
    }
    public void setAnonimo(boolean anonimo) {
        isAnonimo = anonimo;
    }
    public int getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(int idComentario) {
        this.idComentario = idComentario;
    }

    public String getTipoPublicacao() {
        return tipoPublicacao;
    }

    public void setTipoPublicacao(String tipoPublicacao) {
        this.tipoPublicacao = tipoPublicacao;
    }

    public int getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(int idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    public String getConteudoComentario() {
        return conteudoComentario;
    }

    public void setConteudoComentario(String conteudoComentario) {
        this.conteudoComentario = conteudoComentario;
    }

    public String getDataComentario() {
        return dataComentario;
    }

    public void setDataComentario(String dataComentario) {
        this.dataComentario = dataComentario;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }
    public void setUpvoted(boolean upvoted) {
        this.flagUpvoted = upvoted;
    }
    public void setDownvoted(boolean downvoted) {
        this.flagDownvoted = downvoted;
    }

    public void incrementUpVotes() {
        this.upvotes++;
    }
    public void incrementDownVotes() {
        this.downvotes++;
    }


    public void decrementUpVotes() {
        if (this.upvotes > 0) {
            this.upvotes--;
        }
    }
    public void decrementDownVotes() {
        if (this.downvotes > 0) {
            this.downvotes--;
        }
    }
    public boolean isUpvoted() {
        return flagUpvoted;
    }

    public boolean isDownvoted() {
        return flagDownvoted;
    }
}