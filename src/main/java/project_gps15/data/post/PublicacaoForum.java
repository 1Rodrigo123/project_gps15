package project_gps15.data.post;

import project_gps15.data.Topicos;

public class PublicacaoForum extends Post {
    private String titulo;
    private Topicos topicos;
    private boolean flagUpvoted;
    private boolean flagDownvoted;

    public PublicacaoForum(int idPost, String idAutor, String dataPublicacao, String comentario, String titulo, Topicos topicos,int nUpVotes, int nDownVotes) {
        super(idPost, idAutor, dataPublicacao, comentario);
        this.titulo = titulo;
        this.topicos=topicos;
        this.nUpVotes = nUpVotes;
        this.nDownVotes = nDownVotes;
    }

    // ----------- Gets e Sets

    public String getTitulo() { return titulo; }

    public Topicos getTopicos() {
        return topicos;
    }

    public void setTitulo(String titulo) { this.titulo = titulo; }

    public int getnUpVotes() {
        return nUpVotes;
    }

    public int getnDownVotes() {
        return nDownVotes;
    }

    public void incrementUpVotes() {
        this.nUpVotes++;
    }
    public void incrementDownVotes() {
        this.nDownVotes++;
    }

    public void decrementUpVotes() {
        if (this.nUpVotes > 0) {
            this.nUpVotes--;
        }
    }
    public void decrementDownVotes() {
        if (this.nDownVotes > 0) {
            this.nDownVotes--;
        }
    }

    public void setUpvoted(boolean upvoted) {
        this.flagUpvoted = upvoted;
    }

    public void setDownvoted(boolean downvoted) {
        this.flagDownvoted = downvoted;
    }

}
