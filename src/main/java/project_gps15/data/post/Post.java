package project_gps15.data.post;

abstract public class Post {
    protected int idPost;

    protected String emailAutor;   // email do Estudante / Docente
    protected String dataPublicacao;
    protected String comentario;
    protected int nUpVotes;
    protected int nDownVotes;

    protected boolean reported;

    public Post(int idPost, String emailAutor, String dataPublicacao, String comentario) {
        this.idPost = idPost;
        this.emailAutor = emailAutor;
        this.dataPublicacao = dataPublicacao;
        this.comentario = comentario;
        this.nUpVotes = 0;
        this.nDownVotes = 0;
        reported = false;

        //idPost = idAutor * dataPublicacao.hashCode();
    }

    // ----------- equals + hashCode

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if( (obj == null) || (getClass() != obj.getClass()) )
            return false;
        Post other = (Post) obj;
        return idPost == other.idPost;
    }

    @Override
    public int hashCode() {
        return idPost;
    }

    // ----------- Gets e Sets

    public int getIdPost() { return idPost; }

    public String getDataPublicacao() { return dataPublicacao; }

    public String getEmailAutor() { return emailAutor; }

    public String getComentario() { return comentario; }
    public void setComentario(String comentario) { this.comentario = comentario; }

    public int getnUpVotes() { return nUpVotes; }
    public void setnUpVotes(int nUpVotes) { this.nUpVotes = nUpVotes; }
    public int getnDownVotes() { return nDownVotes; }
    public void setnDownVotes(int nDownVotes) { this.nDownVotes = nDownVotes; }

    public boolean isReported() { return reported; }
    public void setReported(boolean r) { reported = r; }
}
