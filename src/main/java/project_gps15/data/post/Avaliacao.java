package project_gps15.data.post;

public class Avaliacao extends Post {
    private boolean isAnonima;
    private int nota;
    String topico;
    private boolean flagUpvoted;
    private boolean flagDownvoted;
    private String nomeUC;
    private String emailDocente;

    public Avaliacao(int idPost, String emailAutor, String dataPublicacao, String comentario, boolean isAnonima, int nota,String topico,int nUpVotes, int nDownVotes, String nomeUC, String emailDocente) {
        super(idPost, emailAutor, dataPublicacao, comentario);
        this.isAnonima = isAnonima;
        this.nota = nota;
        this.topico=topico;
        this.nUpVotes = nUpVotes;
        this.nDownVotes = nDownVotes;
        // chaves estrangeiras para a UC / Docente
        this.nomeUC = nomeUC;
        this.emailDocente = emailDocente;

    }

    // ----------- Gets e Sets

    public String getTopico() {
        return topico;
    }

    public void setTopico(String topico) {
        this.topico = topico;
    }

    public boolean getIsAnonima() { return isAnonima; }
    public void setIsAnonima(boolean isAnonima) { this.isAnonima = isAnonima; }

    public int getNota() { return nota; }
    public void setNota(int nota) { this.nota = nota; }


    public int getnUpVotes() {
        return nUpVotes;
    }

    public int getnDownVotes() {
        return nDownVotes;
    }

    public void incrementUpVotes() {
        this.nUpVotes++;
    }
    public void incrementDownVotes() {
        this.nDownVotes++;
    }

    public void decrementUpVotes() {
        if (this.nUpVotes > 0) {
            this.nUpVotes--;
        }
    }
    public void decrementDownVotes() {
        if (this.nDownVotes > 0) {
            this.nDownVotes--;
        }
    }

    public boolean isUpvoted() {
        return flagUpvoted;
    }

    public boolean isDownvoted() {
        return flagDownvoted;
    }

    public void setUpvoted(boolean upvoted) {
        this.flagUpvoted = upvoted;
    }

    public void setDownvoted(boolean downvoted) {
        this.flagDownvoted = downvoted;
    }

    public String getNomeUC() { return nomeUC; }
    public String getEmailDocente() { return emailDocente; }
}