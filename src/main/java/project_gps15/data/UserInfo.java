package project_gps15.data;

public record UserInfo(String nome, String email, Utilizadores tipo) {
    @Override
    public String toString() {
        return nome + ";" + email + ";" + tipo;
    }

    // isto é um record ... estas 3 queries são desnecessárias

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public Utilizadores getTipo() {
        return tipo;
    }
}
