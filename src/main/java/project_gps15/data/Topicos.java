package project_gps15.data;

public enum Topicos {
    //UC
    GERAL("Geral"),
    ORGANIZACAO("Organização"),
    CONTEUDOS("Conteúdos"),
    METODOS_AVALIACAO("Métodos de Avaliação"),

    // Docentes
    GERAL_DOCENTE("Geral"),
    CAPACIDADE_CIENTIFICA("Capacidade Científica"),
    CAPACIDADE_PEDAGOGICA("Capacidade Pedagógica"),
    CAPACIDADE_ORGANIZACIONAL("Capacidade Organizacional"),

    //FORUM
    GERAL_FORUM("Geral"),
    IPC("IPC"),
    ISEC("ISEC"),
    UC("UC"),

    // IPC
    GERAL_IPC("Geral"),
    GESTAO("Gestao"),
    SERVICOS("Servicos"),

    //ISEC
    GERAL_ISEC("Geral"),
    GESTAO_ISEC("Gestao"),
    SERVICOS_ISEC("Servicos"),
    INSTALACOES("Instalacoes");

    private final String nome;
    Topicos(String nome) {
        this.nome = nome;
    }

    public String getNome() { return nome; }

    public static Topicos[] getTopicosUCs() {
        return new Topicos[] { Topicos.GERAL, Topicos.ORGANIZACAO, Topicos.CONTEUDOS, Topicos.METODOS_AVALIACAO };
    }

    public static Topicos[] getTopicosDocentes() {
        return new Topicos[] { Topicos.GERAL_DOCENTE, Topicos.CAPACIDADE_CIENTIFICA, Topicos.CAPACIDADE_PEDAGOGICA, Topicos.CAPACIDADE_ORGANIZACIONAL };
    }
}
