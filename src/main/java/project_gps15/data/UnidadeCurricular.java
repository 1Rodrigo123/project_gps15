package project_gps15.data;

public class UnidadeCurricular {
    private int idUC;
    private String nomeUC;

    public UnidadeCurricular(int idUC, String nomeUC) {
        this.idUC = idUC;
        this.nomeUC = nomeUC;
    }

    public int getIdUC() { return idUC; }
    public String getNomeUC() { return nomeUC; }
    public void setNomeUC(String nomeUC) { this.nomeUC = nomeUC; }
}