package project_gps15.manager;

import project_gps15.data.TipoAvaliacao;
import project_gps15.data.Topicos;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.Post;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.data.post.Resposta;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import java.util.Comparator;
import java.util.List;

public class ProgramManager {
    private UserInfo userInfo,useralvo;
    private String email_user;
    private final dbManager dbManager ;
    private boolean flag;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public ProgramManager() {
        dbManager =new dbManager();
    }
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public UserInfo getUseralvo(String nome,int tipo){
        useralvo=dbManager.getUserinfo(nome, tipo);
        return useralvo;
    }
    public boolean isDocente(){
        return dbManager.isDocente(userInfo.email());
    }

    public boolean isModerador(){
        return userInfo.getTipo() == Utilizadores.MODERADOR;
    }

    public List<PublicacaoForum> ConsultaPublicacaoForum(Topicos top,String uc){
        return dbManager.ConsultaPublicacoesForum(top,uc);

    }

    public boolean fazPublicacao(Topicos topico,String TituloPub,String ConteudoPub,String nomeUc){
        LocalDate date=LocalDate.now();
        PublicacaoForum aux=new PublicacaoForum(0,userInfo.email(),date.toString(),ConteudoPub,TituloPub,topico, 0,0);
       return dbManager.fazPublicacaoforum(aux,nomeUc);
    }

    public  boolean fazavaliacao(Topicos topico, double valorating, String comment, boolean isAnonim, TipoAvaliacao tipoAvaliacao, String nome_uc_OR_docente){
        LocalDate date=LocalDate.now();
        Avaliacao avaliacao;
        comment=comment.isEmpty()?"":comment;
        if(tipoAvaliacao == TipoAvaliacao.UC) {
            System.out.println("nova avaliação UC");
            avaliacao = new Avaliacao(0, userInfo.getEmail(), date.toString(), comment, isAnonim, (int) valorating, topico.toString(), 0, 0, nome_uc_OR_docente, null);

        }
        else if(tipoAvaliacao == TipoAvaliacao.DOCENTE) {
            System.out.println("nova avaliação docente");
            nome_uc_OR_docente = dbManager.devolveDocenteByName(nome_uc_OR_docente);
            avaliacao = new Avaliacao(0, userInfo.getEmail(), date.toString(), comment, isAnonim, (int) valorating, topico.toString(), 0, 0, null, nome_uc_OR_docente);
        }else{

            avaliacao = new Avaliacao(0, userInfo.getEmail(), date.toString(), comment, isAnonim, (int) valorating, topico.toString(), 0, 0, null, null);
        }

        return dbManager.fazavaliacao(avaliacao);
    }

    public UserInfo isValidLogin(String usermail, String password) {

        userInfo= dbManager.isValidLogin(usermail,password);
        return userInfo;
    }

    public boolean registoJaExiste(String usermail) throws SQLException {
        return dbManager.registoJaExiste(usermail);
    }
    public boolean fazRegistoUtilizador(UserInfo userInfo) {
        return dbManager.fazRegistoUtilizador(userInfo);
    }

    public Avaliacao[] consultaAvaliacoes(Topicos topico, TipoAvaliacao tipoAvaliacao, String nome_uc_OR_docente){
        Avaliacao[ ]res= dbManager.ConsultaAvaliacao(topico, tipoAvaliacao, nome_uc_OR_docente);
        Arrays.sort(res,
                Comparator.comparing((Post post) -> LocalDate.parse(post.getDataPublicacao(), DateTimeFormatter.ISO_LOCAL_DATE)));

        return res;
    }

    public void atualizaVotes( int evaluationId, int newUpvotes ,int newDownvotes){
        dbManager.updateUpvotesAndDownvotes(evaluationId, newUpvotes, newDownvotes);
    }

    public void atualizaVotesForum( int evaluationId, int newUpvotes ,int newDownvotes){
        dbManager.updateUpvotesAndDownvotesForum(evaluationId, newUpvotes, newDownvotes);
    }

    public void atualizaVotesRespostas( int RespostaId, int newUpvotes ,int newDownvotes){
        dbManager.updateUpvotesAndDownvotesRespostas(RespostaId, newUpvotes, newDownvotes);
    }

    public boolean utilizadorJaVotou(String idUtilizador, int idResposta, String tipoEntidade, String tipoVoto){
        return dbManager.utilizadorJaVotou(idUtilizador,idResposta, tipoEntidade,tipoVoto);
    }

    public void removeVoto(String idUtilizador, int idResposta, String tipoEntidade, String tipoVoto){
        dbManager.removeVoto(idUtilizador,idResposta,tipoEntidade,tipoVoto);
    }
    public void registarAcaoUtilizador(String idUtilizador,int idResposta, String tipoEntidade,String  tipoVoto){
        dbManager.registarAcaoUtilizador(idUtilizador,idResposta,tipoEntidade,tipoVoto);
    }
    public void inserirResposta(String tipoPublicacao, int idPublicacao, String emailUtilizador, String conteudoComentario,boolean isAnonimo){
        dbManager.inserirResposta( tipoPublicacao,idPublicacao, emailUtilizador,conteudoComentario,0,0,isAnonimo);
    }
    public int nRespostasPorAvaliacao(int idAvaliacao){
        return dbManager.nRespostasPorAvaliacao(idAvaliacao);
    }

    public int nRespostasPorPost(int idPost){
        return dbManager.nRespostasPorPost(idPost);
    }

    public List<Resposta> obterTodasRespostas(int idPost, String tipo){
        return dbManager.obterTodasRespostas(idPost,tipo);
    }

    public List<String> devolveUCsPorSemestre(int semestre) {
        /*
        if(userInfo.tipo() == Utilizadores.MODERADOR)
            return dbManager.devolveUCsEstudanteSemestre(userInfo.email(), semestre);
        if(userInfo.getTipo() == Utilizadores.DOCENTE)
            return dbManager.devolveUCsEstudanteSemestre(userInfo.email(), semestre);
        */
        return dbManager.devolveUCsEstudanteSemestre(userInfo.email(), semestre, (userInfo.getTipo() == Utilizadores.DOCENTE));
    }

    public String[] devolveDocentes(){
        return dbManager.devolveDocentes(userInfo.email()).toArray(new String[0]);
    }

       /* public void getVotesByID( int evaluationId){
        dbManage.getAvaliacaoById(evaluationId);
    }*/


    public String[] devolveTodosDocentes() {
        return dbManager.devolveTodosDocentes().toArray(new String[0]);
    }

    public List<String> devolveTodasUCsPorSemestre(int nSemestre) {
        return dbManager.devolveTodasUCsPorSemestre(nSemestre);
    }
//-------------------------- Reportar -------------------------

    public void atualizarReportPost(String idUtilizador, int idPost, boolean novoEstado){
        dbManager.atualizarReportPost(idUtilizador,idPost,novoEstado);
    }

    public void atualizarReportResposta(String idUtilizador,int idResposta, boolean novoEstado){
        dbManager.atualizarReportResposta(idUtilizador,idResposta, novoEstado);
    }

    public void atualizarReportAvaliacao(String idUtilizador,int idAvaliacao, boolean novoEstado){
         dbManager.atualizarReportAvaliacao(idUtilizador,idAvaliacao, novoEstado);
    }
    public boolean avaliacaofoiReportada(int idAvaliacao) {
        return dbManager.avaliacaofoiReportada(idAvaliacao);
    }

    public boolean postfoiReportado(int idPost) {
        return dbManager.postfoiReportado(idPost);
    }

    public boolean utilizadorJaReportou(String idUtilizador, int idEntidade,String tipoEntidade) {
        return dbManager.utilizadorJaReportou( idUtilizador, idEntidade,tipoEntidade);
    }

    public boolean respostaFoiReportada(int idComentario) {
        return dbManager.respostaFoiReportada(idComentario);
    }

    public Avaliacao[] consultaAvaliacoesReportadas(Topicos topicoAtual, TipoAvaliacao tipoAvaliacao, String nomeUcOrDocente) {
        return dbManager.consultaAvaliacoesReportadas(topicoAtual, tipoAvaliacao, nomeUcOrDocente);
    }

    //___________________________________Apagar_______________________________________________________
    public void apagarAvaliacaoEComentarios(int idAvaliacao) {
        dbManager.apagarAvaliacaoEComentarios(idAvaliacao);
    }

    public void apagarResposta(int idComentario) {
        dbManager.apagarResposta(idComentario);
    }

    public Avaliacao[] consultaTodasAvaliacoesReportadas() {
        return dbManager.consultaTodasAvaliacoesReportadas();
    }

    public List<PublicacaoForum> ConsultaPublicacoesReportadasForum() {
        return dbManager.ConsultaPublicacoesReportadasForum();
    }

    public  List<Resposta> consultaTodasRespostasReportadas() {
        return dbManager.consultaTodasRespostasReportadas();
    }

    public void apagarPostEComentarios(int idPost) {
        dbManager.apagarPostEComentarios(idPost);
    }

    /* -------------- Contadores --------------- */
    public int countPublicacoes(char tipo) {
        return dbManager.countPublicacoes(userInfo.getEmail(), tipo);
    }

    public int countVotes(char tipo) {
        return dbManager.countVotes(userInfo.getEmail(), tipo);
    }

}
