package project_gps15.manager;

import project_gps15.data.TipoAvaliacao;
import project_gps15.data.Topicos;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.data.post.Resposta;

import java.sql.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class dbManager {
    private static final String dbAdress = "database/IPC4US.db";    // base de dados vai ser usada fora do projeto
    private static final String dbUrl = "jdbc:sqlite:" + dbAdress;
    private static Connection connection;

    //----------------- Conexão à base de dados -----------------
    static {
        try {
            connection = DriverManager.getConnection(dbUrl);
        }catch (Exception e) {
            System.out.println("<Database|ERRO> Nao foi possivel estabelecer conexao com a DB.");
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    //----------------- Login -----------------
    public static UserInfo isValidLogin(String email, String passe) {
        try {
            // Verifica dados na tabela "Estudantes"
            UserInfo estudanteInfo = isValidStudentLogin(email, passe);
            if (estudanteInfo != null) {
                return estudanteInfo;
            }

            // Verifica dados na tabela "Docentes"
            UserInfo docenteInfo = isValidTeacherLogin(email, passe);
            if (docenteInfo != null) {
                return docenteInfo;
            }

            // Verifica dados na tabela "Moderadores"
            UserInfo moderadorInfo = isValidModeratorLogin(email, passe);
            if (moderadorInfo != null) {
                return moderadorInfo;
            }
        } catch (Exception ignored) { }
        return null;
    }

    private static UserInfo isValidStudentLogin(String email, String passe) throws SQLException {
        String query = "SELECT Nome_Estudante FROM Estudantes WHERE Email_Estudante = ? AND PalavraPasse_Estudante = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            statement.setString(2, passe);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    String nome = resultSet.getString("Nome_Estudante");
                    return new UserInfo(nome,email, Utilizadores.ESTUDANTE);
                }
            }
        }
        return null;
    }

    private static UserInfo isValidTeacherLogin(String email, String passe) throws SQLException {
        String query = "SELECT Nome_Docente FROM Docentes WHERE Email_Docente = ? AND PalavraPasse_Docente = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            statement.setString(2, passe);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    String nome = resultSet.getString("Nome_Docente");
                    return new UserInfo(nome,email, Utilizadores.DOCENTE);
                }
            }
        }
        return null;
    }

    private static UserInfo isValidModeratorLogin(String email, String passe) throws SQLException {
        String query = "SELECT Nome_Moderador FROM Moderadores WHERE Email_Moderador = ? AND PalavraPasse_Moderador = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            statement.setString(2, passe);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    String nome = resultSet.getString("Nome_Moderador");
                    return new UserInfo(nome,email, Utilizadores.MODERADOR);
                }
            }
        }
        return null;
    }

    //----------------- Registo de utilizador -----------------
    public boolean registoJaExiste(String email) {
        try {
            String checkUserQuery = "SELECT COUNT(*) FROM Utilizadores WHERE Email_Utilizador = ?";
            try (PreparedStatement pstmt = connection.prepareStatement(checkUserQuery)) {
                pstmt.setString(1, email);
                try (ResultSet resultSet = pstmt.executeQuery()) {
                    return resultSet.next() && resultSet.getInt(1) > 0;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean fazRegistoUtilizador(UserInfo userInfo) {
        String createEntryQuery = "INSERT INTO Utilizadores (Email_Utilizador, Username, Tipo_Utilizador) VALUES (?, ?, ?)";

        try (PreparedStatement pstmt = connection.prepareStatement(createEntryQuery)) {
            pstmt.setString(1, userInfo.email());
            pstmt.setString(2, userInfo.nome());
            pstmt.setString(3, userInfo.tipo().toString());

            return pstmt.executeUpdate() >= 1;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    //----------------- Devolve dados de UCs, Docentes e Utilizador ativo -----------------
    public List<String> devolveUCsEstudanteSemestre(String email, int semestre, boolean isDocente) {
        System.out.println("A devolver UCS estudantes");
        List<String> aux = new ArrayList<>();
        try (Statement statement = connection.createStatement())
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.append("SELECT DISTINCT UnidadesCurriculares.Nome_Unidade_Curricular FROM UnidadesCurriculares ");
            sbQuery.append("INNER JOIN RelacaoEstudanteDocente ON UnidadesCurriculares.Nome_Unidade_Curricular = RelacaoEstudanteDocente.Nome_Unidade_Curricular ");
            if(isDocente)
                sbQuery.append("WHERE RelacaoEstudanteDocente.Email_Docente ='" + email);
            else
                sbQuery.append("WHERE RelacaoEstudanteDocente.Email_Estudante ='" + email);
            if(semestre > 0)
                sbQuery.append("' AND UnidadesCurriculares.Semestre =" + semestre + ";");
            else
                sbQuery.append("';");

            ResultSet rs = statement.executeQuery(sbQuery.toString());
            while (rs.next()) {
                aux.add(rs.getString("Nome_Unidade_Curricular"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aux;
    }

    public List<String> devolveTodasUCsPorSemestre(int nSemestre) {
        List<String> aux = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement
                ("SELECT Nome_Unidade_Curricular FROM UnidadesCurriculares WHERE Semestre = ?")) {

            preparedStatement.setInt(1, nSemestre);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    aux.add(rs.getString("Nome_Unidade_Curricular"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aux;
    }
    public void removeutilizador(String email){
        String query="Delete  From Utilizadores WHERE Email_Utilizador = ?";
        try {
            PreparedStatement preparedStatement= connection.prepareStatement(query);
            preparedStatement.setString(1,email);
           if( preparedStatement.executeUpdate()<1){
               System.out.println("Failed");
           }else {
               System.out.println("Did it");
           }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean isDocente(String email) {
        try {
            String query = "Select Email_Utilizador From Utilizadores Where Email_Utilizador='" + email + "' and Tipo_Utilizador='" + Utilizadores.DOCENTE + "';";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if (rs.next()) {
                System.out.println(rs.getString("Email_Utilizador"));
                System.out.println("É um Docente");
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public String devolveDocenteByName(String name) {
        String email = null;
        try (Statement statement = connection.createStatement())
        {
            String query = "SELECT Email_Docente FROM Docentes where Nome_Docente='" + name + "';";
            ResultSet rs= statement.executeQuery(query);
            if(rs.next()) {
                email = rs.getString("Email_Docente");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return email;
    }

    public List<String> devolveDocentes(String email) {
        List<String> aux = new ArrayList<>();
        String GetQuery = "SELECT DISTINCT Nome_Docente FROM Docentes INNER JOIN RelacaoEstudanteDocente on Docentes.Email_Docente=RelacaoEstudanteDocente.Email_Docente WHERE RelacaoEstudanteDocente.Email_Estudante='" + email + "';";

        try (PreparedStatement getquery = connection.prepareStatement(GetQuery))
        {
            ResultSet rs = getquery.executeQuery();
            while (rs.next()) {
                aux.add(rs.getString("Nome_Docente"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aux;
    }

    public List<String> devolveTodosDocentes() {
        List<String> aux = new ArrayList<>();

        try (Statement statement = connection.createStatement())
        {
            String getQuery = "SELECT Nome_Docente FROM Docentes";
            try (ResultSet rs = statement.executeQuery(getQuery)) {
                while (rs.next()) {
                    aux.add(rs.getString("Nome_Docente"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aux;
    }

    public List<String> devolveEstudantes() {
        List<String> aux=new ArrayList<>();
        String GetQuery = "SELECT Nome_Estudante FROM Estudantes ";

        try (PreparedStatement getquery=connection.prepareStatement(GetQuery))
        {
            ResultSet rs=getquery.executeQuery();
            while (rs.next()){
                aux.add(rs.getString("Nome_Estudante"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aux;
    }

    public UserInfo getUserinfo(String nome, int tipo) {
        UserInfo aux = null;
        try {
            String table = tipo == 1 ? "DOCENTES WHERE Nome_Docente='" + nome + "';" : "ESTUDANTES WHERE Nome_Estudante='" + nome + "';";
            String query = "Select * FROM " + table;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            if (rs.next()) {
                aux = tipo == 1 ? new UserInfo(rs.getString(1), rs.getString(2), Utilizadores.DOCENTE) :
                        new UserInfo(rs.getString(1), rs.getString(2), Utilizadores.ESTUDANTE);
            }
            return aux;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    //---------------------------- Fórum -------------------------------
    public List<PublicacaoForum> ConsultaPublicacoesForum(Topicos topicos, String uc) {
        ArrayList<PublicacaoForum> lista = new ArrayList<>();
        try {
            String query = "Select * From Posts where Titulo_Topico= '" + topicos.toString() + "' AND NOME_UNIDADE_CURRICULAR='" + uc + "';";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                String titulo = rs.getString(2);
                Topicos top = Topicos.valueOf(rs.getString(3));
                String autor = rs.getString(4);
                String conteudo = rs.getString(5);
                String data = rs.getString(6);
                int upvote = rs.getInt(7);
                int downvotes = rs.getInt(8);
                PublicacaoForum aux = new PublicacaoForum(rs.getInt(1), autor, data, conteudo, titulo, top, upvote, downvotes);
                lista.add(aux);
            }
            return lista;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return lista;
        }
    }

    public List<PublicacaoForum> ConsultaPublicacoesReportadasForum() {
        ArrayList<PublicacaoForum> lista = new ArrayList<>();
        try {
            String query = "Select * From Posts where Reportado='true';";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                String titulo = rs.getString(2);
                Topicos top = Topicos.valueOf(rs.getString(3));
                String autor = rs.getString(4);
                String conteudo = rs.getString(5);
                String data = rs.getString(6);
                int upvote = rs.getInt(7);
                int downvotes = rs.getInt(8);
                PublicacaoForum aux = new PublicacaoForum(rs.getInt(1), autor, data, conteudo, titulo, top, upvote, downvotes);
                lista.add(aux);
            }
            return lista;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return lista;
        }
    }
    public boolean fazPublicacaoforum(PublicacaoForum pub, String nomeUC) {
        try {
            String verfica_user="Select * From Utilizadores Where Email_Utilizador='"+pub.getEmailAutor()+"';";
            Statement statement= connection.createStatement();
            ResultSet rs=statement.executeQuery(verfica_user);
            if(!rs.next())
                return false;



            String insertpub = "Insert Into Posts(Titulo_Post,Titulo_Topico,Email_Utilizador,Conteudo_Post,Data_Post,NOME_UNIDADE_CURRICULAR) Values(?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(insertpub);
            ps.setString(1, pub.getTitulo());
            ps.setString(2, pub.getTopicos().toString());
            ps.setString(3, pub.getEmailAutor());
            ps.setString(4, pub.getComentario());
            ps.setString(5, pub.getDataPublicacao());
            ps.setString(6, nomeUC);
            //System.out.println("Erro na insercao");
            //System.out.println("Insercao bem sucedida");
            return ps.executeUpdate() >= 1;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    //-------------------------- Avaliações ----------------------------
    public boolean fazavaliacao(Avaliacao avaliacao){
        try {
            String verfica_user="Select * From Utilizadores Where Email_Utilizador='"+avaliacao.getEmailAutor()+"';";
            Statement statement= connection.createStatement();
            ResultSet rs=statement.executeQuery(verfica_user);
            if(!rs.next()) {
                System.out.println("Nao ha user");
                return false;
            }

            String createEntryQuery = "INSERT INTO Avaliacoes (Conteudo_Avaliacao, Email_Utilizador, Titulo_Topico, Data_Avaliacao, Rating, Upvotes, Downvotes, Nome_Unidade_Curricular, Email_Docente,isAnonimo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

            try (PreparedStatement pstmt = connection.prepareStatement(createEntryQuery))
            {
                pstmt.setString(1, avaliacao.getComentario());
                pstmt.setString(2, avaliacao.getEmailAutor());
                pstmt.setString(3, avaliacao.getTopico());
                pstmt.setString(4, avaliacao.getDataPublicacao());
                pstmt.setInt(5, avaliacao.getNota());
                pstmt.setInt(6, avaliacao.getnUpVotes());
                pstmt.setInt(7, avaliacao.getnDownVotes());

                // chaves estrangeiras para a UC / Docente
                pstmt.setString(8, avaliacao.getNomeUC());
                pstmt.setString(9, avaliacao.getEmailDocente());
                int bool=avaliacao.getIsAnonima()?1:0;
                pstmt.setInt(10,bool);

                if(  pstmt.executeUpdate()<1){
                    System.out.println("Entry insertion or update failed");
                    return false;
                }
                else{
                    System.out.println("Entry insertion succeeded");
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public Avaliacao[] ConsultaAvaliacao(Topicos topico, TipoAvaliacao tipoAvaliacao, String nomeUcOrDocente) {
        List<Avaliacao> res = new ArrayList<>();
        try{
            String sqlQuery = "SELECT * FROM AVALIACOES WHERE Titulo_Topico=?";

            if (tipoAvaliacao == TipoAvaliacao.UC) {
                sqlQuery += " AND Nome_Unidade_Curricular=?";
            } else if (tipoAvaliacao == TipoAvaliacao.DOCENTE) {
                nomeUcOrDocente = devolveDocenteByName(nomeUcOrDocente);
                sqlQuery += " AND Email_Docente=?";
            }

            try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
                pstmt.setString(1, topico.name());

                if (tipoAvaliacao == TipoAvaliacao.UC || tipoAvaliacao == TipoAvaliacao.DOCENTE) {
                    pstmt.setString(2, nomeUcOrDocente);
                }

                ResultSet rs = pstmt.executeQuery();
                if (rs.isBeforeFirst()) {
                    while (rs.next()) {
                        Avaliacao aux = new Avaliacao(
                                rs.getInt(1),
                                rs.getString("Email_Utilizador"),
                                rs.getString("Data_Avaliacao"),
                                rs.getString("Conteudo_Avaliacao"),
                                rs.getInt("isAnonimo") == 1,
                                rs.getInt("Rating"),
                                rs.getString("Titulo_Topico"),
                                rs.getInt("Upvotes"),
                                rs.getInt("Downvotes"),
                                rs.getString("Nome_Unidade_Curricular"),
                                rs.getString("Email_Docente")

                        );
                        res.add(aux);
                        /*aux.setnUpVotes(rs.getInt("Upvotes"));
                        aux.setnDownVotes(rs.getInt("Downvotes"));*/
                    }
                    return res.toArray(new Avaliacao[0]);
                } else {
                    System.out.println("Não ha publicacoes");
                    return res.toArray(new Avaliacao[0]);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Avaliacao[] consultaAvaliacoesReportadas(Topicos topico, TipoAvaliacao tipoAvaliacao, String nomeUcOrDocente) {
        List<Avaliacao> res = new ArrayList<>();
        try {
            String sqlQuery = "SELECT * FROM AVALIACOES WHERE Titulo_Topico=? AND Reportada='true'";

            if (tipoAvaliacao == TipoAvaliacao.UC) {
                sqlQuery += " AND Nome_Unidade_Curricular=?";
            } else if (tipoAvaliacao == TipoAvaliacao.DOCENTE) {
                nomeUcOrDocente = devolveDocenteByName(nomeUcOrDocente);
                sqlQuery += " AND Email_Docente=?";
            }

            try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
                pstmt.setString(1, topico.name());

                if (tipoAvaliacao == TipoAvaliacao.UC || tipoAvaliacao == TipoAvaliacao.DOCENTE) {
                    pstmt.setString(2, nomeUcOrDocente);
                }

                ResultSet rs = pstmt.executeQuery();
                if (rs.isBeforeFirst()) {
                    while (rs.next()) {
                        Avaliacao aux = new Avaliacao(
                                rs.getInt(1),
                                rs.getString("Email_Utilizador"),
                                rs.getString("Data_Avaliacao"),
                                rs.getString("Conteudo_Avaliacao"),
                                false,
                                rs.getInt("Rating"),
                                rs.getString("Titulo_Topico"),
                                rs.getInt("Upvotes"),
                                rs.getInt("Downvotes"),
                                rs.getString("Nome_Unidade_Curricular"),
                                rs.getString("Email_Docente")
                        );
                        res.add(aux);
                    }
                    return res.toArray(new Avaliacao[0]);
                } else {
                    System.out.println("Não há publicações reportadas");
                    return res.toArray(new Avaliacao[0]);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Avaliacao[] consultaTodasAvaliacoesReportadas() {
        List<Avaliacao> res = new ArrayList<>();
        try {
            String sqlQuery = "SELECT * FROM AVALIACOES WHERE Reportada='true'";
            try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
                ResultSet rs = pstmt.executeQuery();
                if (rs.isBeforeFirst()) {
                    while (rs.next()) {
                        Avaliacao aux = new Avaliacao(
                                rs.getInt(1),
                                rs.getString("Email_Utilizador"),
                                rs.getString("Data_Avaliacao"),
                                rs.getString("Conteudo_Avaliacao"),
                                false,
                                rs.getInt("Rating"),
                                rs.getString("Titulo_Topico"),
                                rs.getInt("Upvotes"),
                                rs.getInt("Downvotes"),
                                rs.getString("Nome_Unidade_Curricular"),
                                rs.getString("Email_Docente")
                        );
                        res.add(aux);
                    }
                    return res.toArray(new Avaliacao[0]);
                } else {
                    System.out.println("Não há publicações reportadas");
                    return res.toArray(new Avaliacao[0]);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean updateUpvotesAndDownvotes(int evaluationId, int upvotes, int downvotes) {
        String updateQuery = "UPDATE Avaliacoes SET Upvotes = ?, Downvotes = ? WHERE ID_Avaliacao = ?";

        try (PreparedStatement pstmt = connection.prepareStatement(updateQuery))
        {
            pstmt.setInt(1, upvotes);
            pstmt.setInt(2, downvotes);
            pstmt.setInt(3, evaluationId);
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateUpvotesAndDownvotesForum(int evaluationId, int upvotes, int downvotes) {
        String updateQuery = "UPDATE Posts SET Upvotes = ?, Downvotes = ? WHERE ID_Post = ?";

        try (PreparedStatement pstmt = connection.prepareStatement(updateQuery))
        {
            pstmt.setInt(1, upvotes);
            pstmt.setInt(2, downvotes);
            pstmt.setInt(3, evaluationId);
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean utilizadorJaVotou(String idUtilizador, int idResposta, String tipoEntidade, String tipoVoto){
        String sql;

        if("Avaliacao".equals(tipoEntidade)) {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Avaliacao = ? AND TipoAcao = ?";
        }
        else if("Post".equals(tipoEntidade)) {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Post = ? AND TipoAcao = ?";
        }
        else {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Comentario = ? AND TipoAcao = ?";
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, idUtilizador);
            preparedStatement.setInt(2, idResposta);
            preparedStatement.setString(3, tipoVoto);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    //-------------------------- Respostas ----------------------------
    //Atualiza upvotes e downvotes de uma resposta
    public boolean updateUpvotesAndDownvotesRespostas(int idResposta, int upvotes, int downvotes) {
        try (PreparedStatement pstmt = connection.prepareStatement("UPDATE Comentarios SET Upvotes = ?, Downvotes = ? WHERE ID_Comentario = ?"))
        {
            pstmt.setInt(1, upvotes);
            pstmt.setInt(2, downvotes);
            pstmt.setInt(3, idResposta);
            int linhasAfetadas = pstmt.executeUpdate();

            return linhasAfetadas > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public void removeVoto(String idUtilizador, int idResposta, String tipoEntidade, String tipoVoto) {
        String sql;

        if("Avaliacao".equals(tipoEntidade)) {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Avaliacao = ? AND TipoAcao = ?";
        }
        else if("Post".equals(tipoEntidade)) {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Post = ? AND TipoAcao = ?";
        }
        else {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Comentario = ? AND TipoAcao = ?";
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, idUtilizador);
            preparedStatement.setInt(2, idResposta);
            preparedStatement.setString(3, tipoVoto);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    //Inserir um comentário (resposta)
    public void inserirResposta(String tipoPublicacao, int idPublicacao, String emailUtilizador, String conteudoComentario, int upvotes, int downvotes,boolean isAnonimo) {
        try{
            String sql = "INSERT INTO Comentarios (Tipo_Publicacao, ID_Publicacao, Email_Utilizador, Conteudo_Comentario, Data_Comentario, Upvotes, Downvotes,isAnonimo) VALUES (?, ?, ?, ?, ?, ?, ?,?)";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, tipoPublicacao);
                statement.setInt(2, idPublicacao);
                statement.setString(3, emailUtilizador);
                statement.setString(4, conteudoComentario);
                statement.setString(5, LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"))); // Data atual
                statement.setInt(6, upvotes);
                statement.setInt(7, downvotes);
                statement.setInt(8,isAnonimo?1:0);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Conta o número de respostas de uma avaliação
    public int nRespostasPorAvaliacao(int idAvaliacao) {
        try{
            String sql = "SELECT COUNT(*) FROM Comentarios WHERE Tipo_Publicacao = 'Avaliacao' AND ID_Publicacao = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idAvaliacao);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int nRespostasPorPost(int idPost) {
        try{
            String sql = "SELECT COUNT(*) FROM Comentarios WHERE Tipo_Publicacao = 'Post' AND ID_Publicacao = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, idPost);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Resposta> obterTodasRespostas(int idPost, String tipo) {
        List<Resposta> respostas = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Comentarios WHERE ID_Publicacao = ? AND Tipo_Publicacao = ? ")) {
            statement.setInt(1, idPost);
            statement.setString(2, tipo);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    int idComentario = resultSet.getInt("ID_Comentario");
                    String tipoPublicacao = resultSet.getString("Tipo_Publicacao");
                    int idPub = resultSet.getInt("ID_Publicacao");
                    String emailUtilizador = resultSet.getString("Email_Utilizador");
                    String conteudoComentario = resultSet.getString("Conteudo_Comentario");
                    String dataComentario = resultSet.getString("Data_Comentario");
                    boolean isAnonimo = resultSet.getInt("isAnonimo") == 1;
                    int upvotes = resultSet.getInt("Upvotes");
                    int downvotes = resultSet.getInt("Downvotes");

                    Resposta resposta = new Resposta(idComentario, tipoPublicacao, isAnonimo, idPub, emailUtilizador, conteudoComentario, dataComentario, upvotes, downvotes);

                    respostas.add(resposta);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return respostas;
    }

    //------------------------------- Reportar -----------------------------------
    public void atualizarReportPost(String idUtilizador,int idPost, boolean novoEstado){
        String sql = "UPDATE Posts SET Reportado = ? WHERE ID_Post = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setString(1, novoEstado ? "true" : "false");
            preparedStatement.setInt(2, idPost);
            preparedStatement.executeUpdate();
            System.out.println("Post reportado: " + idPost);

            // Regista a ação do utilizador
            if(novoEstado!= false)
                registarAcaoUtilizador(idUtilizador, idPost, "Post", "Reportar");
            else
                apagarRegistosReportar(idPost,"Post"); //Se o moderador retirar o report, os utilizadores podem voltar a reportar
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void atualizarReportAvaliacao(String idUtilizador,int idAvaliacao, boolean novoEstado) {
        String sql = "UPDATE Avaliacoes SET Reportada = ? WHERE ID_Avaliacao = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, novoEstado ? "true" : "false");
            preparedStatement.setInt(2, idAvaliacao);
            preparedStatement.executeUpdate();
            System.out.println("Fez report");
            // Regista a ação do utilizador
            if(novoEstado!= false)
                registarAcaoUtilizador(idUtilizador, idAvaliacao, "Avaliacao", "Reportar");
            else
                apagarRegistosReportar(idAvaliacao,"Avaliacao"); //Se o moderador retirar o report, os utilizadores podem voltar a reportar
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void atualizarReportResposta(String idUtilizador, int idResposta, boolean novoEstado) {
        String sql = "UPDATE Comentarios SET Reportado = ? WHERE ID_Comentario = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setString(1, novoEstado ? "true" : "false");
            preparedStatement.setInt(2, idResposta);
            preparedStatement.executeUpdate();
            System.out.println("Resposta reportada: " + idResposta);

            // Regista a ação do utilizador
            if(novoEstado!= false)
                registarAcaoUtilizador(idUtilizador, idResposta, "Comentario", "Reportar");
            else
                apagarRegistosReportar(idResposta,"Comentario");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void registarAcaoUtilizador(String idUtilizador, int idEntidade, String tipoEntidade, String tipoAcao) {
        String sql;

        if("Avaliacao".equals(tipoEntidade)) {
             sql = "INSERT INTO RegistoAcoesUtilizador (ID_Utilizador, ID_Avaliacao, TipoAcao) VALUES (?, ?, ?)";
        }
        else if("Post".equals(tipoEntidade)) {
            sql = "INSERT INTO RegistoAcoesUtilizador (ID_Utilizador, ID_Post, TipoAcao) VALUES (?, ?, ?)";
        }
        else {
            sql = "INSERT INTO RegistoAcoesUtilizador (ID_Utilizador, ID_Comentario, TipoAcao) VALUES (?, ?, ?)";
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, idUtilizador);
            preparedStatement.setInt(2, idEntidade);
            preparedStatement.setString(3, tipoAcao);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    private void apagarRegistosReportar(int idEntidade,String tipoEntidade) {
        String sql;

        if("Avaliacao".equals(tipoEntidade)) {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Avaliacao = ? AND TipoAcao = 'Reportar'";
        }
        else if("Post".equals(tipoEntidade)) {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Post = ? AND TipoAcao = 'Reportar'";
        }
        else {
            sql = "DELETE FROM RegistoAcoesUtilizador WHERE ID_Comentario = ? AND TipoAcao = 'Reportar'";
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, idEntidade);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    //_______________ Verificações de status (Reportada ou não) _______________
    public boolean utilizadorJaReportou(String idUtilizador, int idEntidade,String tipoEntidade) {
        String sql;

        if("Avaliacao".equals(tipoEntidade)) {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Avaliacao = ? AND TipoAcao = 'Reportar'";
        }
        else if("Post".equals(tipoEntidade)) {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Post = ? AND TipoAcao = 'Reportar'";
        }
        else {
            sql = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND ID_Comentario = ? AND TipoAcao = 'Reportar'";
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, idUtilizador);
            preparedStatement.setInt(2, idEntidade);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean avaliacaofoiReportada(int idAvaliacao) {
        String sql = "SELECT Reportada FROM Avaliacoes WHERE ID_Avaliacao = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setInt(1, idAvaliacao);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    String reportada = resultSet.getString("Reportada");
                    return "true".equalsIgnoreCase(reportada); // Retorna verdadeiro se o campo Reportada tiver o valor "true" (ignora maiúsculas/minúsculas)
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean postfoiReportado(int idPost) {
        String sql = "SELECT Reportado FROM Posts WHERE ID_Post = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setInt(1, idPost);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    String reportado = resultSet.getString("Reportado");
                    return "true".equalsIgnoreCase(reportado); // Retorna verdadeiro se o campo Reportada tiver o valor "true" (ignora maiúsculas/minúsculas)
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean respostaFoiReportada(int idComentario) {
        String sql = "SELECT Reportado FROM Comentarios WHERE ID_Comentario = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, idComentario);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    String reportada = resultSet.getString("Reportado");
                    return "true".equalsIgnoreCase(reportada); // Retorna verdadeiro se o campo Reportado tiver o valor "true" (ignora maiúsculas/minúsculas)
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }


    //-------------------------- Apagar (Moderador) ----------------------------
    //________________Avaliações________________
    public void apagarAvaliacaoEComentarios(int idAvaliacao) {
        // Estou a utilizar uma transação para garantir a consistência dos dados
        try {
            connection.setAutoCommit(false);

            // Apaga os comentários associados à avaliação
            apagarComentariosPorAvaliacao(idAvaliacao);

            // Apaga a avaliação
            apagarAvaliacao(idAvaliacao);

            connection.commit();
        } catch (SQLException e) {
            try {
                //Em caso de erro faz rollback
                connection.rollback();
            } catch (SQLException rollbackException) {
                rollbackException.printStackTrace();
            }

            e.printStackTrace();
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public void apagarAvaliacao(int idAvaliacao) {

        String sql = "DELETE FROM Avaliacoes WHERE ID_Avaliacao = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, idAvaliacao);
            statement.executeUpdate();
            System.out.println("Avaliação apagada: " + idAvaliacao);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void apagarComentariosPorAvaliacao(int idAvaliacao) {
        String sql = "DELETE FROM Comentarios WHERE Tipo_Publicacao = 'Avaliacao' AND ID_Publicacao = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, idAvaliacao);
            statement.executeUpdate();
            System.out.println("Comentários associados à avaliação apagados: " + idAvaliacao);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //________________Posts________________

    public void apagarPostEComentarios(int idPost) {
        try {
            connection.setAutoCommit(false);

            apagarComentariosPorPost(idPost);

            apagarPost(idPost);

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException rollbackException) {
                rollbackException.printStackTrace();
            }

            e.printStackTrace();
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public void apagarPost(int idPost) {
        String sql = "DELETE FROM Posts WHERE ID_Post = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, idPost);
            statement.executeUpdate();
            System.out.println("Post apagado: " + idPost);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void apagarComentariosPorPost(int idPost) {
        String sql = "DELETE FROM Comentarios WHERE Tipo_Publicacao = 'Post' AND ID_Publicacao = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, idPost);
            statement.executeUpdate();
            System.out.println("Comentários associados ao post apagados: " + idPost);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //________________Respostas________________
    public void apagarResposta(int idComentario) {
        String sql = "DELETE FROM Comentarios WHERE ID_Comentario = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, idComentario);
            statement.executeUpdate();
            System.out.println("Resposta apagada: " + idComentario);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public  List<Resposta> consultaTodasRespostasReportadas() {
        List<Resposta> respostas = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Comentarios WHERE Reportado='true'")) {

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    int idComentario = resultSet.getInt("ID_Comentario");
                    String tipoPublicacao = resultSet.getString("Tipo_Publicacao");
                    int idPub = resultSet.getInt("ID_Publicacao");
                    String emailUtilizador = resultSet.getString("Email_Utilizador");
                    String conteudoComentario = resultSet.getString("Conteudo_Comentario");
                    String dataComentario = resultSet.getString("Data_Comentario");
                    boolean isAnonimo = resultSet.getInt("isAnonimo") == 1;
                    int upvotes = resultSet.getInt("Upvotes");
                    int downvotes = resultSet.getInt("Downvotes");

                    Resposta resposta = new Resposta(idComentario, tipoPublicacao, isAnonimo, idPub, emailUtilizador, conteudoComentario, dataComentario, upvotes, downvotes);

                    respostas.add(resposta);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return respostas;
    }
    public  int getidAvaliacao(){
        try(Connection connection= DriverManager.getConnection(dbUrl)){
            String query="Select ID_Avaliacao from Avaliacoes where Email_Utilizador='Teste'";
            Statement statement= connection.createStatement();
            ResultSet rs= statement.executeQuery(query);
            if(rs.next()) {
                System.out.println("Encontrou");
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getidReposta(){
        try(Connection connection= DriverManager.getConnection(dbUrl)){
            String query="Select ID_Comentario from Comentarios where Email_Utilizador='Teste'";
            Statement statement= connection.createStatement();
            ResultSet rs= statement.executeQuery(query);
            if(rs.next()) {
                System.out.println("Encontrou resposta"+rs.getInt(1));
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;

    }
    public int getidPost(){
        try(Connection connection= DriverManager.getConnection(dbUrl)){
            String query="Select ID_Post from Posts where Email_Utilizador='Teste'";
            Statement statement= connection.createStatement();
            ResultSet rs= statement.executeQuery(query);
            if(rs.next())
                return rs.getInt(1);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    /* -------------- Contadores --------------- */
    public int countPublicacoes(String user_id, char tipo) {
        int count = 0;

        String query = switch (tipo) {
            case 'a' -> "SELECT COUNT(*) FROM Avaliacoes WHERE Email_Utilizador = ?";
            case 'p' -> "SELECT COUNT(*) FROM Posts WHERE Email_Utilizador = ?";
            case 'r' -> "SELECT COUNT(*) FROM Comentarios WHERE Email_Utilizador = ?";
            default -> "";
        };
        try (PreparedStatement statement = connection.prepareStatement(query))
        {
            statement.setString(1, user_id);
            ResultSet rs = statement.executeQuery();
            count = rs.getInt(1);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int countVotes(String user_id, char tipo) {
        int count = 0;

        String tipoAcao = switch (tipo) {
            case 'u' -> "Upvote";
            case 'd' -> "Downvote";
            default -> "";
        };

        String query = "SELECT COUNT(*) FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? AND TipoAcao = ?";
        try (PreparedStatement statement = connection.prepareStatement(query))
        {
            statement.setString(1, user_id);
            statement.setString(2, tipoAcao);
            ResultSet rs = statement.executeQuery();
            count = rs.getInt(1);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
    public void apagarRegistosUtilizadortestes(){
        String query="DELETE FROM RegistoAcoesUtilizador WHERE ID_Utilizador = ? or ID_Utilizador= ?";
        try {
            PreparedStatement preparedStatement=connection.prepareStatement(query);
            preparedStatement.setString(1,"Teste");
            preparedStatement.setString(2,"idUtilizadorExistente");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}