module project_gps15 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires com.almasb.fxgl.all;
    requires com.gluonhq.attach.lifecycle;
    //requires charm.glisten;
    requires java.sql;


    opens project_gps15 to javafx.fxml;
    exports project_gps15;
    opens project_gps15.ui to javafx.fxml;
    exports project_gps15.ui;
    exports project_gps15.ui.Controlers;
    opens project_gps15.ui.Controlers to javafx.fxml;
    exports project_gps15.manager;
    opens project_gps15.manager to javafx.fxml;
    exports project_gps15.data;
    opens project_gps15.data to javafx.fxml;


}