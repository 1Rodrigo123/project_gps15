package project_gps15.ui.Controlers;

import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project_gps15.Application;
import project_gps15.manager.ProgramManager;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.SceneManager;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class PerfilUCControlerTest {

    private static PerfilUCControler perfilUCControler;
    private static ProgramManager programManager;
    private static SceneManager sceneManager;

    @BeforeEach
    void setUp() {
        if(perfilUCControler == null) {
            CountDownLatch latch = new CountDownLatch(1);

            Platform.startup(() -> {
                Stage stage = new Stage();
                Application.programManager = new ProgramManager();
                programManager = new ProgramManager();
                sceneManager = new SceneManager(stage);
                perfilUCControler = PerfilUCControler.getInstance();
                sceneManager.switchScene(EnumFXML.PERFIL_UC);
                programManager.isValidLogin("a1@isec.pt","teste");

                latch.countDown();
            });

            try {
                latch.await(5, TimeUnit.SECONDS);  // Espera 5 segundos para o JavaFX ser iniciado
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    void atualiza() {
       // perfilUCControler.set
    }

    @Test
    void getInstance() {
        assertEquals(perfilUCControler, PerfilUCControler.getInstance());
    }

    @Test
    void initialize() {
        assertEquals(perfilUCControler, PerfilUCControler.getInstance());
    }

    @Test
    void getNomeUc() {
        perfilUCControler.setNomeUc("Análise Matemática I");
        assertNotEquals("Análise", perfilUCControler.getNomeUc());
        assertEquals("Análise Matemática I", perfilUCControler.getNomeUc());
    }

    @Test
    void setNomeUc() {
        perfilUCControler.setNomeUc("Análise Matemática I");
        assertNotEquals("Análise", perfilUCControler.getNomeUc());
        assertEquals("Análise Matemática I", perfilUCControler.getNomeUc());
    }

    @Test
    void getBreadCrumbUC() {
        String previousPath = perfilUCControler.getBreadCrumbUC();
        System.out.println(previousPath);
        perfilUCControler.setBreadCrumbUC("Análise Matemática I");
        assertNotEquals(previousPath + "Análise", perfilUCControler.getBreadCrumbUC());
        assertEquals(previousPath + "Análise Matemática I", perfilUCControler.getBreadCrumbUC());
        perfilUCControler.setBreadCrumbUC("");
    }

    @Test
    void setBreadCrumbUC() {
        String previousPath = perfilUCControler.getBreadCrumbUC();
        perfilUCControler.setBreadCrumbUC("Análise Matemática I");
        assertNotEquals(previousPath + "Análise", perfilUCControler.getBreadCrumbUC());
        assertEquals(previousPath + "Análise Matemática I", perfilUCControler.getBreadCrumbUC());
        perfilUCControler.setBreadCrumbUC("");
    }
}