package project_gps15.ui.Controlers;
import project_gps15.Application;
import project_gps15.manager.ProgramManager;
import project_gps15.ui.Controlers.LoginController;
import project_gps15.ui.SceneManager;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class LoginControllerTest {

    private static LoginController loginController;


    @AfterAll
    public static void shutdownJFX() {
        Platform.exit();
    }

    @BeforeAll
   static void setUp() {
        if (loginController == null) {
            CountDownLatch latch = new CountDownLatch(1);

            Platform.startup(() -> {
                Stage stage = new Stage();
                Application.programManager = new ProgramManager();
                SceneManager sceneManager = new SceneManager(stage);
                loginController = LoginController.getInstance();
                loginController.setSceneManager(sceneManager);
               // loginController=LoginController.getLogin();
                System.out.println(loginController);

                latch.countDown();

            });
            try {
                latch.await(5, TimeUnit.SECONDS);  // Espera 5 segundos para o JavaFX ser iniciado
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    //Verifica se o loginController foi inicializado
    @Test
    void testGetLogin() {
        assertEquals(loginController, LoginController.getInstance());
        //assertNotNull(loginController);
    }

    //Verifica se o email é válido (se tem o formato correto) - AC1 Task2
    @Test
    void testIsValidEmail() {
        assertTrue(loginController.isValidEmail("user@isec.pt"));
        assertTrue(loginController.isValidEmail("user@esac.pt"));
        assertTrue(loginController.isValidEmail("user@esec.pt"));
        assertTrue(loginController.isValidEmail("user@estgoh.pt"));
        assertTrue(loginController.isValidEmail("user@estesc.pt"));
        assertTrue(loginController.isValidEmail("user@iscac.pt"));
    }

    //Verifica se o email é inválido (se tem o formato incorreto) - AC1 Task2
    @Test
    void testIsInvalidEmail() {
        assertFalse(loginController.isValidEmail("user@gmail.com"));
        assertFalse(loginController.isValidEmail("user@nada.pt"));
        assertFalse(loginController.isValidEmail("  "));
        assertFalse(loginController.isValidEmail("gatos"));
        assertFalse(loginController.isValidEmail("manuel@"));
        assertFalse(loginController.isValidEmail("@isec.pt"));
    }

    //Verifica o funcionamento do campo de email quando o utilizador pressiona uma tecla
    @Test
    void testOnEmailFieldClicked() {

        TextField emailField = new TextField();
        emailField.setText("utilizador@isec.pt");

        KeyEvent event = new KeyEvent(KeyEvent.KEY_PRESSED, "", "", null, false, false, false, false);

        loginController.onEmailFieldClicked(event);
        assertEquals("", emailField.getStyle());
    }

    //Verifica se os campos estão vazios - AC1 Task5
    @Test
    void testOnLoginButtonClickNullFields() {

        TextField emailField = new TextField();
        PasswordField passwordField = new PasswordField();
        Label errorLabel = new Label();

        loginController.emailField = emailField;
        loginController.passwordField = passwordField;
        loginController.errorLabel = errorLabel;

        emailField.setText("");
        passwordField.setText("");

        assertDoesNotThrow(() -> loginController.onLoginButtonClick(new ActionEvent()));

        assertEquals("Campos de preenchimento obrigatório. Por favor, preencha os campos.", errorLabel.getText());
    }

    //Verifica se o login é válido (se o email e a password estão corretos) - AC1 Task1
    @Test
    void testOnLoginButtonClickValidLogin() {

        TextField emailField = new TextField();
        PasswordField passwordField = new PasswordField();
        Label errorLabel = new Label();

        loginController.emailField = emailField;
        loginController.passwordField = passwordField;
        loginController.errorLabel = errorLabel;

        emailField.setText("a1@isec.pt");
        passwordField.setText("teste");

        assertDoesNotThrow(() -> loginController.onLoginButtonClick(new ActionEvent()));

        assertEquals("", errorLabel.getText());

        //Significa que já mudou de cena
        assertEquals(false, loginController.sceneManager.equals("Login"));

    }

    //Verifica login inválido US1 - AC1 Task3 e Task4 (email ou password incorretos)
    @Test
    void testOnLoginButtonClickInvalidLogin() {

        TextField emailField = new TextField();
        PasswordField passwordField = new PasswordField();
        Label errorLabel = new Label();

        loginController.emailField = emailField;
        loginController.passwordField = passwordField;
        loginController.errorLabel = errorLabel;

        emailField.setText("mariano@iscac.pt");
        passwordField.setText("banana");

        assertDoesNotThrow(() -> loginController.onLoginButtonClick(new ActionEvent()));

        assertNotEquals("", errorLabel.getText());
    }
}
