package project_gps15.ui.Controlers;

import javafx.scene.control.Label;
import project_gps15.Application;
import project_gps15.data.TEMA;
import project_gps15.data.Topicos;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.Resposta;
import project_gps15.manager.ProgramManager;
import project_gps15.manager.dbManager;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.SceneManager;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class SeccaoAvaliacaoUCControlerTest {
    private static SeccaoAvaliacaoUCControler seccaoAvaliacaoUCControler;
    private static ProgramManager programManager;
    private static SceneManager sceneManager;
    @AfterAll
    public static void shutdownJFX() {
        dbManager dbManager=new dbManager();
        dbManager.removeutilizador("Teste");
        Platform.exit();
    }


    @BeforeAll
    static void setUp() {
        if (seccaoAvaliacaoUCControler == null) {
            CountDownLatch latch = new CountDownLatch(1);

            Platform.startup(() -> {
                Stage stage = new Stage();
                Application.programManager = new ProgramManager();
                dbManager dbManager=new dbManager();
                dbManager.fazRegistoUtilizador(new UserInfo("Teste","Teste", Utilizadores.ESTUDANTE));
                programManager= Application.programManager;
                sceneManager = new SceneManager(stage);
                programManager.isValidLogin("a1@isec.pt","teste");
                seccaoAvaliacaoUCControler = SeccaoAvaliacaoUCControler.getInstance();
                sceneManager.switchScene(EnumFXML.SECCAO_AVALIACAO);
                programManager.isValidLogin("a1@isec.pt","teste");


                latch.countDown();

            });
            try {
                latch.await(5, TimeUnit.SECONDS);  // Espera 5 segundos para o JavaFX ser iniciado
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    @Test
    void initialize() {
        assertEquals(seccaoAvaliacaoUCControler,SeccaoAvaliacaoUCControler.getInstance());
    }

    @Test
    void atualiza() {
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.GERAL);
        Application.Scene.set(EnumFXML.SECCAO_AVALIACAO.toString());
      assertArrayEquals(programManager.consultaAvaliacoes(seccaoAvaliacaoUCControler.topicoAtual, null, null),seccaoAvaliacaoUCControler.getAvaliacoes());

        //assertEquals(programManager.consultaAvaliacoes(seccaoAvaliacaoUCControler.topico_atual),seccaoAvaliacaoUCControler.getAvaliacoes());
       assertNotEquals(programManager.consultaAvaliacoes(Topicos.CONTEUDOS, null, null),seccaoAvaliacaoUCControler.getAvaliacoes());
    }

    @Test
    void getSec() {
        assertEquals(seccaoAvaliacaoUCControler,SeccaoAvaliacaoUCControler.getInstance());
    }

    @Test
    void getValueRating() {
        seccaoAvaliacaoUCControler.setValueRating(4);
        assertEquals(4, seccaoAvaliacaoUCControler.getValueRating());

    }
    @Test
    void getTopico_atual() {
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.GERAL);
        assertEquals(Topicos.GERAL, seccaoAvaliacaoUCControler.getTopicoAtual());

    }
    @Test
    void testOnPublicarWithoutOffensiveWords() {


        // Teste quando não há palavras ofensivas no comentário
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.GERAL); // Defina o tópico atual se necessário
        seccaoAvaliacaoUCControler.setValueRating(4); // Defina a avaliação se necessário
        seccaoAvaliacaoUCControler.getComentario().setText("Um comentário sem palavras ofensivas");

        // Verifica se o método onPublicar não define o errorLabel
        seccaoAvaliacaoUCControler.onPublicar();
        assertEquals("", seccaoAvaliacaoUCControler.getErrorLabel().getText());
    }

    @Test
    void testOnPublicarWithOffensiveWords() {
        // Teste quando há palavras ofensivas no comentário
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.GERAL); // Defina o tópico atual se necessário
        seccaoAvaliacaoUCControler.setValueRating(4); // Defina a avaliação se necessário
        seccaoAvaliacaoUCControler.getComentario().setText("Babaca");

        // Verifica se o método onPublicar define corretamente o errorLabel
        seccaoAvaliacaoUCControler.onPublicar();
        assertEquals("O comentário contém palavras ofensivas. Por favor, reveja o seu comentário.", seccaoAvaliacaoUCControler.getErrorLabel().getText());
    }



    @Test
    void testOnUpvoteButtonClick() {
        Avaliacao avaliacao = new Avaliacao(1,"teste@isec.pt",null,"Gosto dessa, nota 100",false,5,"Geral",0,0, null, null);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");
        seccaoAvaliacaoUCControler.setTemaatual(TEMA.ISEC);
        seccaoAvaliacaoUCControler.onUpvoteButtonClick(avaliacao, upLabel, downLabel);


        assertEquals(1, avaliacao.getnUpVotes());
        assertEquals("1", upLabel.getText());
        assertEquals("0", downLabel.getText());
    }

    @Test
    void testOnDownvoteButtonClick() {

        Avaliacao avaliacao = new Avaliacao(1,"teste@isec.pt",null," nota 100",false,5,"Geral",0,0, null, null);

        Label upLabel = new Label("0");
        Label downLabel = new Label("0");
        seccaoAvaliacaoUCControler.setUserInfo(programManager.getUserInfo());
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.GERAL_DOCENTE);
        seccaoAvaliacaoUCControler.onDownvoteButtonClick(avaliacao, upLabel, downLabel);

        assertEquals(1, avaliacao.getnDownVotes());
        assertEquals("0", upLabel.getText());
        assertEquals("1", downLabel.getText());
    }

    @Test
    void testOnUpvoteButtonClickResposta() {
        seccaoAvaliacaoUCControler.setUserInfo(programManager.getUserInfo());
        Resposta resposta = new Resposta(1,"Avaliacao",false,1,"teste@isec.pt",null,"resposta sim",0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");

        seccaoAvaliacaoUCControler.onUpvoteButtonClickResposta(resposta, upLabel, downLabel);

        assertEquals(1, resposta.getUpvotes());
        assertEquals("1", upLabel.getText());
        assertEquals("0", downLabel.getText());
    }

    @Test
    void testOnDownvoteButtonClickResposta() {

        Resposta resposta = new Resposta(1,"Avaliacao",false,1,"teste@isec.pt",null,"resposta sim",0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");
        seccaoAvaliacaoUCControler.setUserInfo(programManager.getUserInfo());
        seccaoAvaliacaoUCControler.setTopicoAtual(Topicos.SERVICOS);

        seccaoAvaliacaoUCControler.onDownvoteButtonClickResposta(resposta, upLabel, downLabel);

        assertEquals(1, resposta.getDownvotes());
        assertEquals("0", upLabel.getText());
        assertEquals("1", downLabel.getText());
    }
}