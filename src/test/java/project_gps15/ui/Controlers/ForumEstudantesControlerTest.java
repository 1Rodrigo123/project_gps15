package project_gps15.ui.Controlers;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.jupiter.api.*;
import project_gps15.Application;
import project_gps15.data.Topicos;
import project_gps15.data.post.Post;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.data.post.Resposta;
import project_gps15.manager.ProgramManager;
import project_gps15.ui.EnumFXML;
import project_gps15.ui.SceneManager;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


class ForumEstudantesControlerTest {

    private static ForumEstudantesControler forumEstudantesController;
    private static ProgramManager programManager;
    private static SceneManager sceneManager;
    @AfterAll
    public static void shutdownJFX() {
        Platform.exit();
    }


    @BeforeAll
    static void setUp() {
        if (forumEstudantesController == null) {
            CountDownLatch latch = new CountDownLatch(1);

            Platform.startup(() -> {
                Stage stage = new Stage();
                Application.programManager = new ProgramManager();
                programManager= Application.programManager;
                sceneManager = new SceneManager(stage);
                forumEstudantesController = ForumEstudantesControler.getInstance();
                sceneManager.switchScene(EnumFXML.SECCAO_AVALIACAO);
                programManager.isValidLogin("a1@isec.pt","teste");


                latch.countDown();

            });
            try {
                latch.await(5, TimeUnit.SECONDS);  // Espera 5 segundos para o JavaFX ser iniciado
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    @Test
    void initialize() {
        assertEquals(forumEstudantesController,ForumEstudantesControler.getInstance());
    }


    @Test
    void setTopicoatual() {
        forumEstudantesController.setTopicoatual(Topicos.GERAL);
        assertEquals(Topicos.GERAL, forumEstudantesController.getTopicoatual());

    }

    @Test
    void setUcatual() {
        forumEstudantesController.setUcatual("Programação");
        assertEquals("Programação", forumEstudantesController.getUcatual());
    }


    @Test
    void testOnPublicarWithoutOffensiveWords() {


        // Teste quando não há palavras ofensivas no comentário
        forumEstudantesController.setTopicoatual(Topicos.GERAL); // Defina o tópico atual se necessário
        forumEstudantesController.TituloPub.setText("Titulo Tetste");
        forumEstudantesController.getComentario().setText("Um comentário sem palavras ofensivas");

        // Verifica se o método onPublicar não define o errorLabel
        forumEstudantesController.onPublicar();
        assertEquals("Um comentário sem palavras ofensivas", forumEstudantesController.ConteudoPub.getText());
    }

    @Test
    void testOnPublicarWithOffensiveWords() {
        // Teste quando há palavras ofensivas no comentário
        forumEstudantesController.TituloPub.setText("Titulo Tetste");
        forumEstudantesController.setTopicoatual(Topicos.GERAL); // Defina o tópico atual se necessário
        forumEstudantesController.getComentario().setText("Babaca");

        forumEstudantesController.onPublicar();
        assertEquals("Não podes publicar conteúdo ofensivo", forumEstudantesController.ConteudoPub.getText());
    }

      /*  @Test
    void atualiza() {
        forumEstudantesController.setTopicoatual(Topicos.GERAL);
        Application.Scene.set(EnumFXML.FORUM_ESTUDANTES.toString());
        assertEquals(programManager.ConsultaPublicacaoForum(forumEstudantesController.topicoatual, null),forumEstudantesController.getPublicacaoForum());
    }



    @Test
    void onUpvoteButtonClick() {
        PublicacaoForum publicacaoForum = new PublicacaoForum(8, "teste@isec.pt", "2023-12-12", "123", "tituolo", Topicos.UC,0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");

        forumEstudantesController.onUpvoteButtonClick(publicacaoForum, upLabel, downLabel);

        assertEquals(1, publicacaoForum.getnUpVotes());
        assertEquals("1", upLabel.getText());
        assertEquals("0", downLabel.getText());
    }

    @Test
    void onDownvoteButtonClick() {
        PublicacaoForum publicacaoForum = new PublicacaoForum(8, "teste@isec.pt", "2023-12-12", "123", "tituolo", Topicos.UC,0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");

        forumEstudantesController.onDownvoteButtonClick(publicacaoForum, upLabel, downLabel);

        assertEquals(1, publicacaoForum.getnDownVotes());
        assertEquals("0", upLabel.getText());
        assertEquals("1", downLabel.getText());
    }

    @Test
    void onUpvoteButtonClickResposta() {
        Resposta resposta = new Resposta(1,"Post",false,1,"teste@isec.pt",null,"resposta sim",0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");

        forumEstudantesController.onUpvoteButtonClickResposta(resposta, upLabel, downLabel);

        assertEquals(1, resposta.getUpvotes());
        assertEquals("1", upLabel.getText());
        assertEquals("0", downLabel.getText());

    }

    @Test
    void onDownvoteButtonClickResposta() {
        Resposta resposta = new Resposta(1,"Post",false,1,"teste@isec.pt",null,"resposta sim",0,0);
        Label upLabel = new Label("0");
        Label downLabel = new Label("0");

        forumEstudantesController.onDownvoteButtonClickResposta(resposta, upLabel, downLabel);

        assertEquals(1, resposta.getDownvotes());
        assertEquals("0", upLabel.getText());
        assertEquals("1", downLabel.getText());
    }*/
}