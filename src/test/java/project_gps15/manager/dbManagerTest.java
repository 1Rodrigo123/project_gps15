package project_gps15.manager;

import javafx.application.Platform;
import javafx.stage.Stage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import project_gps15.Application;
import project_gps15.data.TipoAvaliacao;
import project_gps15.data.Topicos;
import project_gps15.data.UserInfo;
import project_gps15.data.Utilizadores;
import project_gps15.data.post.Avaliacao;
import project_gps15.data.post.PublicacaoForum;
import project_gps15.ui.Controlers.LoginController;
import project_gps15.ui.SceneManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class dbManagerTest {
    @BeforeAll
    static void setUp() {
       dbManager dbManager=new dbManager();
       dbManager.fazRegistoUtilizador(new UserInfo("Teste","Teste",Utilizadores.ESTUDANTE));
    }
    @AfterAll
    public static void shutdownJFX() {
      dbManager dbManager=new dbManager();
      dbManager.removeutilizador("Teste");




    }

    @Test
    void isValidLogin() {


        UserInfo expectedUserInfo = new UserInfo("Gabriel Almeida", "a1@isec.pt", Utilizadores.ESTUDANTE);

        // Teste com login valido
        UserInfo actualUserInfo = dbManager.isValidLogin("a1@isec.pt", "teste");
        assertEquals(expectedUserInfo, actualUserInfo);

        // Teste com login invalido
        actualUserInfo = dbManager.isValidLogin("invalidEmail", "invalidPassword");
        assertNull(actualUserInfo);
    }

    @Test
    void registoJaExiste() {

        dbManager dbManager=new dbManager();
        UserInfo testUserInfo = new UserInfo("teste", "teste@isec.pt", Utilizadores.ESTUDANTE);

        // Test with new user
        boolean isRegistered = dbManager.fazRegistoUtilizador(testUserInfo);
        assertTrue(isRegistered);
        dbManager.removeutilizador("teste@isec.pt");

        isRegistered = dbManager.registoJaExiste("teste@isec.pt");
        assertFalse(isRegistered);
    }



    @Test
    void fazRegistoUtilizador() {

        dbManager dbManagerInstance = new dbManager();
        dbManagerInstance.removeutilizador("teste@isec.pt");

        UserInfo testUserInfo = new UserInfo("teste", "teste@isec.pt", Utilizadores.ESTUDANTE);

        // Test with new user
        boolean isRegistered = dbManagerInstance.fazRegistoUtilizador(testUserInfo);
        assertTrue(isRegistered);

        // Test with existing user
        isRegistered = dbManagerInstance.fazRegistoUtilizador(testUserInfo);
        assertFalse(isRegistered);
        dbManagerInstance.removeutilizador("teste@isec.pt");
    }


    @Test
    void devolveUCsEstudanteSemestre() {


        dbManager dbManagerInstance = new dbManager();
        List<String> expectedUCs = new ArrayList<>();
        expectedUCs.add("Tecnologias Web");
        expectedUCs.add("Sistemas Digitais");
        expectedUCs.add("Eletrónica");


        List<String> UcsDevolvidas = dbManagerInstance.devolveUCsEstudanteSemestre("a1@isec.pt", 1, false);
        assertEquals(expectedUCs, UcsDevolvidas);

        expectedUCs = Arrays.asList("Sistemas Digitais");
        UcsDevolvidas = dbManagerInstance.devolveUCsEstudanteSemestre("cPaiva@isec.pt", 1, true);
        assertEquals(expectedUCs, UcsDevolvidas);


        UcsDevolvidas = dbManagerInstance.devolveUCsEstudanteSemestre("nonExistingEmail", 1, false);
        assertTrue(UcsDevolvidas.isEmpty());
    }

    @Test
    void devolveTodasUCsPorSemestre() {
        // Create an instance of dbManager
        dbManager dbManagerInstance = new dbManager();


        List<String> expectedUCs = Arrays.asList("Álgebra Linear", "Análise Matemática I","Eletrónica","Introdução à Programação","Sistemas Digitais","Tecnologias Web");
        List<String> UcsDevolvidas = dbManagerInstance.devolveTodasUCsPorSemestre(1);
        assertEquals(expectedUCs, UcsDevolvidas);


        UcsDevolvidas = dbManagerInstance.devolveTodasUCsPorSemestre(100);
        assertTrue(UcsDevolvidas.isEmpty());
    }

    @Test
    void isDocente() {
        dbManager dbManagerInstance = new dbManager();

        boolean isTeacher = dbManagerInstance.isDocente("cPaiva@isec.pt");
        assertTrue(isTeacher);

        isTeacher = dbManagerInstance.isDocente("a1@isec.pt");
        assertFalse(isTeacher);

        isTeacher = dbManagerInstance.isDocente("cPaiva");
        assertFalse(isTeacher);
    }

    @Test
    void devolveDocenteByName() {

        dbManager dbManagerInstance = new dbManager();

        String expectedEmail = "cPaiva@isec.pt";
        String actualEmail = dbManagerInstance.devolveDocenteByName("Carlos Paiva");
        assertEquals(expectedEmail, actualEmail);
        actualEmail = dbManagerInstance.devolveDocenteByName("Manuel@isec.pt");
        assertNull(actualEmail);
    }

    @Test
    void devolveDocentes() {
        dbManager dbManagerInstance = new dbManager();

        List<String> ProfessoresEsperados = Arrays.asList( "Carlos Moita","Carlos Paiva");
        List<String> Professores_Devolvidos = dbManagerInstance.devolveDocentes("a202212345@isec.pt");
        assertEquals(ProfessoresEsperados, Professores_Devolvidos);

        Professores_Devolvidos = dbManagerInstance.devolveDocentes("a2222@isec.pt");
        assertTrue(Professores_Devolvidos.isEmpty());
    }

    @Test
    void devolveTodosDocentes() {
        dbManager dbManagerInstance = new dbManager();

        List<String> ProfessoresEsperados = Arrays.asList("João Macedo", "Carlos Paiva", "Carlos Moita","Elisa Sousa");
        List<String> Professores_Devolvidos = dbManagerInstance.devolveTodosDocentes();
        assertEquals(ProfessoresEsperados, Professores_Devolvidos);
    }

    @Test
    void devolveEstudantes() {
        dbManager instanciaDbManager = new dbManager();


        List<String> estudantesEsperados = Arrays.asList("Antonio Pires", "Maria Bragra", "Pedro Gomes","Aline Silva","Orlando Gaspar","Gabriel Almeida");
        List<String> estudantesAtuais = instanciaDbManager.devolveEstudantes();
        assertEquals(estudantesEsperados, estudantesAtuais);
    }

    @Test
    void getUserinfo() {
        dbManager instanciaDbManager = new dbManager();

        UserInfo userInfoEsperado = new UserInfo("Carlos Paiva", "cPaiva@isec.pt", Utilizadores.DOCENTE);
        UserInfo userInfoAtual = instanciaDbManager.getUserinfo("Carlos Paiva", 1);
        assertEquals(userInfoEsperado, userInfoAtual);

        userInfoEsperado = new UserInfo("Gabriel Almeida", "a1@isec.pt", Utilizadores.ESTUDANTE);
        userInfoAtual = instanciaDbManager.getUserinfo("Gabriel Almeida", 0);
        assertEquals(userInfoEsperado, userInfoAtual);

        userInfoAtual = instanciaDbManager.getUserinfo("Manuel", 1);
        assertNull(userInfoAtual);
    }

    @Test
    void consultaPublicacoesForum() {
        dbManager instanciaDbManager = new dbManager();

        instanciaDbManager.fazPublicacaoforum(new PublicacaoForum(0,"Teste","data","comentaro","teste",Topicos.UC,0,0),"Teste");
        List<PublicacaoForum> publicacoesEsperadas = Arrays.asList(new PublicacaoForum(instanciaDbManager.getidPost(),"Teste","data","comentaro","teste",Topicos.UC,0,0));
        List<PublicacaoForum> publicacoesAtuais = instanciaDbManager.ConsultaPublicacoesForum(Topicos.UC, "Teste");
        assertEquals(publicacoesEsperadas, publicacoesAtuais);

        publicacoesAtuais = instanciaDbManager.ConsultaPublicacoesForum(Topicos.CAPACIDADE_CIENTIFICA, "Métodos Estatisticos");
        assertTrue(publicacoesAtuais.isEmpty());
        instanciaDbManager.apagarPost(instanciaDbManager.getidPost());
    }

    @Test
    void fazPublicacaoforum() {
        dbManager instanciaDbManager = new dbManager();


       PublicacaoForum novaPublicacao = new PublicacaoForum(1, "Teste", "data1", "conteudo1", "titulo1", Topicos.UC,0,0);
        boolean publicacaoFeita = instanciaDbManager.fazPublicacaoforum(novaPublicacao, "AlgebraLinear");
        assertTrue(publicacaoFeita);
        instanciaDbManager.apagarPost(instanciaDbManager.getidPost());

    }

    @Test
    void fazavaliacao() {
        dbManager instanciaDbManager = new dbManager();


        Avaliacao novaAvaliacao = new Avaliacao(1, "Teste", "data1", "conteudo1", true, 5, "topico1", 10, 2, "UC1", "docente1");
        boolean avaliacaoFeita = instanciaDbManager.fazavaliacao(novaAvaliacao);
        assertTrue(avaliacaoFeita);

        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());
    }



    @Test
    void testConsultaAvaliacao() {
       // Aqui vou verificar se o array vai ser diferente pq é enormeisto
        dbManager instanciaDbManager = new dbManager();

        Avaliacao[] avaliacoesEsperadas = new Avaliacao[]{new Avaliacao(1, "Teste", "data1", "conteudo1", true, 5, "topico1", 10, 2, "UC1", "docente1")};
        Avaliacao[] avaliacoesAtuais = instanciaDbManager.ConsultaAvaliacao(Topicos.GERAL_FORUM, TipoAvaliacao.UC, "UC1");
        assertNotEquals(avaliacoesEsperadas, avaliacoesAtuais);
        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());

    }

    @Test
    void consultaAvaliacoesReportadas() {
        dbManager instanciaDbManager = new dbManager();

        Avaliacao[] avaliacoesEsperadas = new Avaliacao[]{new Avaliacao(20, "Teste", "2023-12-07", "bada 2", false, 5, "Geral", 0, 0, "Tecnologias Web", null)};
        Avaliacao[] avaliacoesAtuais = instanciaDbManager.consultaAvaliacoesReportadas(Topicos.GERAL, TipoAvaliacao.UC, "Tecnologias Web");
        assertArrayEquals(avaliacoesEsperadas, avaliacoesAtuais);
        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());

        avaliacoesAtuais = instanciaDbManager.consultaAvaliacoesReportadas(Topicos.GERAL, TipoAvaliacao.UC, "UC_NAO_EXISTENTE");
        assertArrayEquals(new Avaliacao[0], avaliacoesAtuais);
    }

    @Test
    void consultaTodasAvaliacoesReportadas() {
        dbManager instanciaDbManager = new dbManager();

        Avaliacao[] avaliacoesEsperadas = new Avaliacao[]{new Avaliacao(1, "autor1", "data1", "conteudo1", false, 5, "topico1", 10, 2, "UC1", "docente1")};
        Avaliacao[] avaliacoesAtuais = instanciaDbManager.consultaTodasAvaliacoesReportadas();
        assertNotEquals(avaliacoesEsperadas, avaliacoesAtuais);
    }


    @Test
    void updateUpvotesAndDownvotes() {
        dbManager instanciaDbManager = new dbManager();

        Avaliacao novaAvaliacao = new Avaliacao(1, "Teste", "data1", "conteudo1", true, 5, "topico1", 10, 2, "UC1", "docente1");
        instanciaDbManager.fazavaliacao(novaAvaliacao);
        boolean upvotesAndDownvotesAtualizados = instanciaDbManager.updateUpvotesAndDownvotes(instanciaDbManager.getidAvaliacao(), 11, 3);
        assertTrue(upvotesAndDownvotesAtualizados);

        upvotesAndDownvotesAtualizados = instanciaDbManager.updateUpvotesAndDownvotes(1000, 11, 3);
        assertFalse(upvotesAndDownvotesAtualizados);
        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());
        instanciaDbManager.apagarRegistosUtilizadortestes();
    }

    @Test
    void utilizadorJaVotou() {
        dbManager instanciaDbManager = new dbManager();
        Avaliacao novaAvaliacao = new Avaliacao(1, "Teste", "data1", "conteudo1", true, 5, "topico1", 10, 2, "UC1", "docente1");
        instanciaDbManager.fazavaliacao(novaAvaliacao);

        instanciaDbManager.inserirResposta("Avaliacao",instanciaDbManager.getidAvaliacao(),"Teste","Teste",0,0,false);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.updateUpvotesAndDownvotesRespostas(idresposta,0,1);
        instanciaDbManager.registarAcaoUtilizador("Teste",idresposta,"comment","Downvote");
        boolean utilizadorJaVotou = instanciaDbManager.utilizadorJaVotou("Teste", idresposta, "comment", "Downvote");
        assertTrue(utilizadorJaVotou);
        utilizadorJaVotou = instanciaDbManager.utilizadorJaVotou("a@isec.pt", 1000, "avaliacao", "upvote");
        assertFalse(utilizadorJaVotou);
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());
        instanciaDbManager.apagarRegistosUtilizadortestes();
    }

    @Test
    void updateUpvotesAndDownvotesRespostas() {

        dbManager instanciaDbManager = new dbManager();
        Avaliacao novaAvaliacao = new Avaliacao(1, "Teste", "data1", "conteudo1", true, 5, "topico1", 10, 2, "UC1", "docente1");
        instanciaDbManager.fazavaliacao(novaAvaliacao);
        instanciaDbManager.inserirResposta("Avaliacao",instanciaDbManager.getidAvaliacao(),"Teste","Teste",0,0,false);
        int idresposta=instanciaDbManager.getidReposta();
        boolean upvotesAndDownvotesAtualizados = instanciaDbManager.updateUpvotesAndDownvotesRespostas(idresposta, 11, 3);
        assertTrue(upvotesAndDownvotesAtualizados);

        upvotesAndDownvotesAtualizados = instanciaDbManager.updateUpvotesAndDownvotesRespostas(1000, 11, 3);
        assertFalse(upvotesAndDownvotesAtualizados);
        instanciaDbManager.apagarAvaliacao(instanciaDbManager.getidAvaliacao());
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarRegistosUtilizadortestes();
    }

    @Test
    void removeVoto() {
        dbManager instanciaDbManager = new dbManager();

        instanciaDbManager.removeVoto("a1@isec.pt", 17, "Avaliacao", "Downvote");

        boolean utilizadorJaVotou = instanciaDbManager.utilizadorJaVotou("a1@isec.pt", 17, "Avaliacao", "Downvote");
        assertFalse(utilizadorJaVotou);
    }

    @Test
    void inserirResposta() {
        dbManager instanciaDbManager = new dbManager();
       boolean teste= instanciaDbManager.fazavaliacao(new Avaliacao(0,"Teste","data","Teste",false,3,Topicos.GERAL.toString(),0,0,"Algebra",null));
        System.out.println(teste);

       int getidAvaliacao= instanciaDbManager.getidAvaliacao();
        instanciaDbManager.inserirResposta("Avaliacao",getidAvaliacao,"Teste","Teste",0,0,false);
        int nRespostas = instanciaDbManager.nRespostasPorAvaliacao(getidAvaliacao);
        assertEquals(1, nRespostas);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarAvaliacao(getidAvaliacao);
    }

    @Test
    void nRespostasPorAvaliacao() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(0,"Teste","data","Teste",false,3,Topicos.GERAL.toString(),0,0,"Algebra",null));
        int getidAvaliacao= instanciaDbManager.getidAvaliacao();
        instanciaDbManager.inserirResposta("Avaliacao",getidAvaliacao,"Teste","Teste",0,0,false);
        int nRespostas = instanciaDbManager.nRespostasPorAvaliacao(getidAvaliacao);
        assertEquals(1, nRespostas);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarAvaliacao(getidAvaliacao);
    }



    @Test// to do
    void atualizarReportPost() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazPublicacaoforum(new PublicacaoForum(0,"Teste","data","Teste","Teste",Topicos.CAPACIDADE_PEDAGOGICA,0,0),"Algebra");
        int idpost=instanciaDbManager.getidPost();

        instanciaDbManager.atualizarReportPost("Teste", idpost, true);
        boolean reportAtualizado= instanciaDbManager.postfoiReportado(idpost);
        assertTrue(reportAtualizado);
        instanciaDbManager.atualizarReportPost("idUtilizadorExistente", 1000, true);

        reportAtualizado = instanciaDbManager.postfoiReportado(1000);
        assertFalse(reportAtualizado);
        instanciaDbManager.apagarPost(idpost);
        instanciaDbManager.apagarRegistosUtilizadortestes();
    }

    @Test
    void atualizarReportAvaliacao() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(0,"Teste","data","Teste",false,3,Topicos.GERAL.toString(),0,0,"Algebra",null));
        int idavaliacao=instanciaDbManager.getidAvaliacao();

        instanciaDbManager.atualizarReportAvaliacao("Teste", idavaliacao, true);
        boolean reportAtualizado= instanciaDbManager.avaliacaofoiReportada(idavaliacao);
        assertTrue(reportAtualizado);
        instanciaDbManager.atualizarReportAvaliacao("idUtilizadorExistente", 1000, true);

        reportAtualizado = instanciaDbManager.avaliacaofoiReportada(1000);
        assertFalse(reportAtualizado);
        instanciaDbManager.apagarAvaliacao(idavaliacao);
    }

    @Test
    void atualizarReportResposta() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(0,"Teste","data","Teste",false,3,Topicos.GERAL.toString(),0,0,"Algebra",null));
        int idavaliacao=instanciaDbManager.getidAvaliacao();
        instanciaDbManager.inserirResposta("Avaliacao",idavaliacao,"Teste","Teste",0,0,false);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.atualizarReportResposta("Teste", idresposta, true);
        boolean reportAtualizado= instanciaDbManager.respostaFoiReportada(idresposta);
        assertTrue(reportAtualizado);
        instanciaDbManager.atualizarReportAvaliacao("idUtilizadorExistente", 1000, true);

        reportAtualizado = instanciaDbManager.avaliacaofoiReportada(1000);
        assertFalse(reportAtualizado);
        instanciaDbManager.apagarAvaliacao(idavaliacao);
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarRegistosUtilizadortestes();

    }

    @Test
    void registarAcaoUtilizador() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(0,"Teste","data","Teste",false,3,Topicos.GERAL.toString(),0,0,"Algebra",null));
        int idavaliacao=instanciaDbManager.getidAvaliacao();
        instanciaDbManager.inserirResposta("Avaliacao",idavaliacao,"Teste","Teste",0,0,false);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.atualizarReportResposta("Teste", idresposta, true);

        instanciaDbManager.registarAcaoUtilizador("Teste", idresposta, "Comentario", "Reportar");

        boolean acaoRegistrada = instanciaDbManager.utilizadorJaReportou("Teste", idresposta, "Comentario");
        assertTrue(acaoRegistrada);
        instanciaDbManager.apagarAvaliacao(idavaliacao);
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarRegistosUtilizadortestes();

    }




    @Test
    void apagarAvaliacao() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(50,"Teste","12-12-12","as",false,3,Topicos.GERAL.toString(),0,0,"Eletrónica",null));
        int id=instanciaDbManager.getidAvaliacao();
        instanciaDbManager.apagarAvaliacao(id);
         id=instanciaDbManager.getidAvaliacao();
        assertEquals(id, 0);
    }
    @Test
    void apagarResposta() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazavaliacao(new Avaliacao(50,"Teste","12-12-12","as",false,3,Topicos.GERAL.toString(),0,0,"Eletrónica",null));
        int id=instanciaDbManager.getidAvaliacao();
        instanciaDbManager.inserirResposta("Avaliacao",id,"Teste","Teste",0,0,false);
        int idresposta=instanciaDbManager.getidReposta();
        instanciaDbManager.apagarResposta(idresposta);
        instanciaDbManager.apagarAvaliacao(id);
        id=instanciaDbManager.getidReposta();
        assertEquals(id, 0);
    }
    @Test
    void apagarPost() {
        dbManager instanciaDbManager = new dbManager();
        instanciaDbManager.fazPublicacaoforum(new PublicacaoForum(0,"Teste","data","comentaro","teste",Topicos.UC,0,0),"Teste");
        int idpost=instanciaDbManager.getidPost();
        instanciaDbManager.apagarPost(idpost);
        idpost=instanciaDbManager.getidPost();
        assertEquals(idpost, 0);
    }






}